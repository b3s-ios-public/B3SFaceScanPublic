## How to Build this App

### 1. prepare cocoapods
- you need a MacBook running the latest MacOS with the latest verison of Xcode installed on it.
- then [install cocoapods](https://cocoapods.org/), i.e. a dependency manager for Swift and Objective-C Cocoa projects.

### 2. Add our B3S Repo
we made our cocoapods repo publicly accessible. we will release our public pods to this repo.
Now use the following command to add our repo to your cocoapods.
``` shell
#open the Terminal.app on your MacOS, execute the following command
pod repo add mypod-spec https://gitlab.com/b3s-ios-public/pods/specs.git
#if you can't access this repo, please contact us.
```

### 3. Clone this Project
```shell
#create a working directory, e.g. ~/workspace or whatever
cd ~/workspace
#clone this project
git clone https://gitlab.com/b3s-ios-public/B3SFaceScanPublic.git
```

### 4. Install dependency pods
```shell
#install the pods required by this project
cd ~/workspace/B3SFaceScanPublic/B3SFaceScan
pod install
#if you can't access our B3S repo, please contact us.
```

### 5. Build it using Xcode

Open ~/workspace/B3SFaceScanPublic/B3SFaceScan/B3SFaceScan.xcworkspace using Xcode.

Now you can work with this project in Xcode as usual. 

####Contact
techsupport@body3dscale.com
