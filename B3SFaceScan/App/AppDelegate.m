//
//  AppDelegate.m
//  B3SFaceScan
//
//  Created on 2020/7/24.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "AppDelegate.h"
//#import <Bugly/Bugly.h>
#import <UMCommon/UMCommon.h>
#import <UMCommonLog/UMCommonLogHeaders.h>
#import "StyleManager.h"
#import "B3SNavigationController.h"
#import "B3SHomeViewController.h"
#import <B3SScanSDK/B3SScanSDK.h>
#import "B3SUserManager.h"
//#import <SDKIdentity/SDKIdentity.h>

#import "B3SLocalizeService.h"
@interface AppDelegate ()

@end

@implementation AppDelegate

+ (instancetype)sharedInstance
{
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions 
{
    [self handlerMainLogic];
    
    [self setLanguageSetting];
     
    // initialize UI text localization util
    [B3SLocalizeService sharedInstance];

//    if (@available(iOS 13.0, *)) {
//        // SceneDelegate中创建window
//    } else {
//        self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
//    }
    
    return YES;
}

- (void)handlerMainLogic
{
    [self initCrashReport];
    [self sdkAuthorization];
    [StyleManager sharedInstance];
    
}

/**
  Set language category for UI text and voice.
  Currently only chinese and english are supported.
 */
-(void) setLanguageSetting
{
    NSArray *languages = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
    NSString *firstLang = languages.firstObject;
    
    if ([firstLang hasPrefix:@"zh-"]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"zh" forKey:@"appLanguage"];
    } else {//if([language hasPrefix:@"en"]){
        [[NSUserDefaults standardUserDefaults] setObject:@"en" forKey:@"appLanguage"];
    }
    [[NSUserDefaults standardUserDefaults]synchronize];
}

- (void)sdkAuthorization
{
    // get the singleton
    B3SSdkManager *sdkMgr = [B3SSdkManager sharedInstance];
    
    NSDate *start=[NSDate date];
    [sdkMgr verifySDKIntegrity:^(NSError *error){
        NSLog(@"verificate time used:%.3lf seconds",[[NSDate date] timeIntervalSinceDate:start]);
        if(error){
          NSLog(@"SDK Authorization failed, %@", error);
        }else{
          NSLog(@"SDK Authorization succ");
        }
    }];
}

-(void) initCrashReport
{
    //开发者需要显式的调用此函数，日志系统才能工作
    [UMCommonLogManager setUpUMCommonLogManager];
    [UMConfigure setLogEnabled:YES];//设置打开日志
    //需要在隐私政策中声明使用了友盟SDK
    [UMConfigure initWithAppkey:@"60f30c57a6f90557b7bcbaef" channel:@"Testflight"];

}

#pragma mark - UISceneSession lifecycle


- (UISceneConfiguration *)application:(UIApplication *)application configurationForConnectingSceneSession:(UISceneSession *)connectingSceneSession options:(UISceneConnectionOptions *)options  API_AVAILABLE(ios(13.0)){
    // Called when a new scene session is being created.
    // Use this method to select a configuration to create the new scene with.
  
    return [[UISceneConfiguration alloc] initWithName:@"Default Configuration" sessionRole:connectingSceneSession.role];
 
}


- (void)application:(UIApplication *)application didDiscardSceneSessions:(NSSet<UISceneSession *> *)sceneSessions  API_AVAILABLE(ios(13.0)){
    // Called when the user discards a scene session.
    // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
}


@end
