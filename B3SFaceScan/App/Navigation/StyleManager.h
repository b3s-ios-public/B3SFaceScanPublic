//
//  StyleManager.h
//
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface StyleManager : NSObject

+ (instancetype)sharedInstance;

@end

NS_ASSUME_NONNULL_END
