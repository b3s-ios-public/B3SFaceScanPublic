//
//  StyleManager.m
//
//  Created on 2020/5/27.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "StyleManager.h"
//#import "PopupDialogOverlayView.h"
//#import <IQKeyboardManager/IQKeyboardManager.h>
//#import <GKNavigationBarViewController/GKNavigationBarConfigure.h>
@implementation StyleManager

+ (instancetype)sharedInstance
{
    static StyleManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[StyleManager alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self gk_navigationBarStyle];
        [self configTabBar];
        [self configDialogStyle];
        [self configToastStyle];
        [self configKeyboard];
    }
    return self;
}

- (void)configTabBar{
    [[UITabBar appearance] setBarTintColor:[UIColor whiteColor]];
}

- (void)configNavStyle{
 
    // UINavigationBar
    UINavigationBar *navBar = [UINavigationBar appearance];
    navBar.tintColor = [UIColor whiteColor]; // 文字颜色
    navBar.barTintColor = [UIColor whiteColor]; // 背景色
    navBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]}; // 标题文字颜色
    [navBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault]; // 透明导航栏
    [navBar setShadowImage:[UIImage new]]; // 去掉导航栏底部黑线
    
    // UIBarButtonItem
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:16], NSForegroundColorAttributeName: [UIColor whiteColor]};
    [[UIBarButtonItem appearance] setTitleTextAttributes:attributes forState:UIControlStateNormal];
    [[UIBarButtonItem appearance] setTitleTextAttributes:attributes forState:UIControlStateHighlighted];
}


- (void)gk_navigationBarStyle{
    
    [[GKNavigationBarConfigure sharedInstance] setupCustomConfigure:^(GKNavigationBarConfigure *configure) {
        configure.gk_navItemRightSpace = 18;
        configure.gk_navItemLeftSpace = 18;
        configure.backgroundColor = HexColor(0x35363D);
        configure.titleColor = [UIColor whiteColor];
    }];
}

- (void)configDialogStyle{
    
//    // 全局弹框遮罩层
//    PopupDialogOverlayView *dialogOverlay = [PopupDialogOverlayView appearance];
//    dialogOverlay.blurRadius = 3; // 模糊范围
//    dialogOverlay.blurEnabled = NO; // 毛玻璃效果
//    dialogOverlay.liveBlur = NO; // 动态渲染背景
//    //dialogOverlay.opacity = 0.6;
//    dialogOverlay.opacity = 1.0;
//    dialogOverlay.color = [[UIColor blackColor] colorWithAlphaComponent:0.3];
}

- (void)configToastStyle{
    
    // 提示层
//    [CSToastManager setDefaultDuration:2.0];
//    [CSToastManager setDefaultPosition:CSToastPositionCenter];
//    [CSToastManager setTapToDismissEnabled:YES];
}

- (void)configKeyboard{
    
    // 启用IQKeyboardManager
//    [IQKeyboardManager sharedManager].enable = YES;
//    // 隐藏输入框顶部工具条
//    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
//    // 点击空白地方收起键盘
//    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
//    // 输入框与键盘最小距离
//    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField = 50;
}

@end
