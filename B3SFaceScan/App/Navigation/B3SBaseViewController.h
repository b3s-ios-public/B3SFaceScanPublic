//
//  B3SBaseViewController.h
//  body3dscale
//
//

#import <UIKit/UIKit.h>
#import "ViewControllerProtocol.h"
#import <GKNavigationBarViewController/GKNavigationBarViewController.h>

@interface B3SBaseViewController : GKNavigationBarViewController

- (void)showBackgroundView;

- (BOOL)navigationShouldPopOnBackButton; //子类可以重新这个方法做拦截


- (void)setupNavItems; //子类调用这个方法
@end

