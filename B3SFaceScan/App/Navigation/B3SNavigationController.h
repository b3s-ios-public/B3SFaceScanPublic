//
//  B3SNavigationController.h
//  body3dscale
//
//  Created on 2018/6/25.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <UIKit/UIKit.h>

@interface B3SNavigationController : UINavigationController

@property (nonatomic, strong) UIColor *tintColor;


@end
