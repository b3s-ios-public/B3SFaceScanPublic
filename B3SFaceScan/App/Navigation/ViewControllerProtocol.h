//
//  body3dscale
//
//  Created on 2019/4/10.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#ifndef ViewControllerProtocol_h
#define ViewControllerProtocol_h
#import <Foundation/Foundation.h>
//#import "B3SBaseViewController.h"

@protocol ViewControllerProtocol <NSObject>

/**
 设置NavBarItems
 */
@optional -(void)setupNavItmes;

/**
 初始化通知
 */
@optional - (void)setupNotify;

/**
 初始化场景以及节点
 */
- (void)setupViews;

/**
 配置layouts
 */
-(void)setupLayouts;

/**
 配置RAC
 */
-(void)setupRAC;

/**
 初始化节点
 */
@optional -(void)setupNodes;

/**
 初始化灯光效果
 */
@optional - (void)setupLight;

/**
 初始化手势
 */
@optional - (void)setupGestures;

/**
 加载数据
 */
@optional -(void)loadData;

/**
 调试参数
 */
@optional -(void)debugOptions;

@end


@protocol ViewProtocol <NSObject>

/**
 初始化子视图
 */
- (void)setupViews;

/**
 配置layouts
 */
-(void)setupLayouts;


@end

#endif /* TeethMacro_h */
