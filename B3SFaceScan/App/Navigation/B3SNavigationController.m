//
//  B3SNavigationController.m
//  body3dscale
//
//  Created on 2018/6/25.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SNavigationController.h"

@interface B3SNavigationController ()<UIGestureRecognizerDelegate>

@end

@implementation B3SNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [super pushViewController:viewController animated:animated];
    
    self.interactivePopGestureRecognizer.delegate = self;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    UIViewController *topVC = self.topViewController;
    
    if ([topVC respondsToSelector:@selector(statusBarStyle)]) {
        return [topVC performSelector:@selector(statusBarStyle)];
    }
    return UIStatusBarStyleLightContent;
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return self.childViewControllers.count > 1;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return self.viewControllers.count > 1;
}

/// 是否支持旋转
-(BOOL)shouldAutorotate
{
    return [[self.viewControllers lastObject] shouldAutorotate];
}

/// 适配旋转的类型
#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    return [[self.viewControllers lastObject] supportedInterfaceOrientations];
}

/// 优先屏幕旋转方向
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {

    return [[self.viewControllers lastObject] preferredInterfaceOrientationForPresentation];
}

@end
