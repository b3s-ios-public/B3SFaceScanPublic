//
//  B3SBaseViewController.m
//  body3dscale
//
//  Created on 2019/4/19.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SBaseViewController.h"

@interface B3SBaseViewController ()
@property (nonatomic, strong) UIImageView * imageView;
@end

@implementation B3SBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    

    
}

- (void)showBackgroundView{
    
    UIImage * bgImage = [UIImage imageNamed:@"background"];
    self.imageView = [[UIImageView alloc] initWithImage:bgImage];
    [self.view addSubview:self.imageView];
    [self.view sendSubviewToBack:self.imageView];
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
}

- (void)setupNavItems{
    
     [self.gk_navigationBar setHidden:NO]; // 默认不隐藏导航栏
    // 自定义返回键
    UIButton *leftBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 32)];
    leftBtn.contentMode = UIViewContentModeLeft;
    [leftBtn setImage:[[UIImage imageNamed:@"nav_back_white"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [leftBtn setImage:[[UIImage imageNamed:@"nav_back_white"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateSelected];
    [leftBtn addTarget:self action:@selector(leftBarItemClick:) forControlEvents:UIControlEventTouchUpInside];
    self.gk_navLeftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
  
    self.gk_navBarTintColor = HexColor(0x1E1E1E);
    self.gk_navTintColor = HexColor(0x1E1E1E);
    
    // 设置导航栏按钮文案
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:16], NSForegroundColorAttributeName: HexColor(0x1E1E1E)};
    [[UIBarButtonItem appearance] setTitleTextAttributes:attributes forState:UIControlStateNormal];
    [[UIBarButtonItem appearance] setTitleTextAttributes:attributes forState:UIControlStateHighlighted];
}


/// 是否支持旋转
- (BOOL)shouldAutorotate {
    return NO;
}

/// 适配旋转的类型
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskPortrait;
}

// 屏幕优先方向
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    
    return UIInterfaceOrientationPortrait;
}

- (void)leftBarItemClick:(id)sender{
    
    if([self respondsToSelector:@selector(navigationShouldPopOnBackButton)]) {
        if ([self navigationShouldPopOnBackButton]) {
            if (self.navigationController) {
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                [self dismissViewControllerAnimated:YES completion:NULL];
            }
        }
    }else{

        [self.navigationController popViewControllerAnimated:YES];
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
}

-(BOOL)navigationShouldPopOnBackButton{
    return YES;
}

@end
