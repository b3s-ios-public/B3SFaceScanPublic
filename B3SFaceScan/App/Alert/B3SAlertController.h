//
//  B3SAlertController.h
//  body3dscale
//
//  Created on 21/03/2018.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^B3SAlertControllerBlock)(id obj);

@interface B3SAlertController : B3SBaseViewController

@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIView *alertView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineViewCenterX;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alterWidth;


@property (nonatomic, copy) dispatch_block_t cancelBlock;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alertViewCenterYConstraint;

- (void)dismiss;
- (void)dismissWithComplete:(void(^)(void))complete;

@end
