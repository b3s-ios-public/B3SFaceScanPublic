//
//  B3SAlertFeedbackViewController.m
//  body3dscale
//
//  Created on 21/03/2018.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SAlertFeedbackViewController.h"
#import <ReactiveCocoa.h>
#import <UITextView+Placeholder.h>
#import <YYKit.h>

@interface B3SAlertFeedbackViewController ()

@property (nonatomic, assign) BOOL isAvailable;

@end

@implementation B3SAlertFeedbackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    __weak typeof(self) wself = self;
    [self.feedbackTextView.rac_textSignal subscribeNext:^(NSString *text) {
        wself.isAvailable = (text.length > 0);

    }];
    
    self.feedbackTextView.placeholder = @"请填写内容";
    self.feedbackTextView.placeholderColor = [UIColor colorWithHexString:@"cccccc"];
    
    self.rightButton.rac_command = [[RACCommand alloc] initWithEnabled:RACObserve(self, isAvailable) signalBlock:^RACSignal *(id input) {
        if (wself.rightBlock) {
            wself.rightBlock(wself.feedbackTextView.text);
        }
        
        [wself dismiss];
        return [RACSignal empty];
    }];

}

@end
