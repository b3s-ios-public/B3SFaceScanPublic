//
//  B3SAlertFeedbackViewController.h
//  body3dscale
//
//  Created on 21/03/2018.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <UIKit/UIKit.h>
#import "B3SAlertController.h"

@interface B3SAlertFeedbackViewController : B3SAlertController
@property (weak, nonatomic) IBOutlet UITextView *feedbackTextView;

@property (nonatomic, strong) B3SAlertControllerBlock rightBlock;

@end
