//
//  B3SAlertController.m
//  body3dscale
//
//  Created on 21/03/2018.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SAlertController.h"
#import <ReactiveCocoa.h>
#import <Masonry.h>

@interface B3SAlertController ()
@end

@implementation B3SAlertController

+ (instancetype)new
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Alert" bundle:nil];
    return [sb instantiateViewControllerWithIdentifier:NSStringFromClass(self)];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.lineViewHeight.constant = 1.0/[UIScreen mainScreen].scale;
    self.lineViewWidth.constant = 1.0/[UIScreen mainScreen].scale;
    
    if(!self.cancelBlock){
        self.lineViewCenterX.constant = -self.alterWidth.constant/2;
    }
    
    self.alertView.layer.cornerRadius = 5.0f;
    
    __weak typeof(self) wself = self;
    self.cancelButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        
        if (wself.cancelBlock) {
            wself.cancelBlock();
        }
        
        [wself dismiss];
        return [RACSignal empty];
    }];
    
    @weakify(self);
    [[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardDidShowNotification object:nil] subscribeNext:^(NSNotification *aNotification) {
        @strongify(self);
        NSDictionary* info = [aNotification userInfo];
        //kbSize即為鍵盤尺寸 (有width, height)
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;//得到鍵盤的高度
        
        [UIView animateWithDuration:0.4 animations:^{
            wself.alertViewCenterYConstraint.constant = - (kbSize.height + CGRectGetHeight(self.alertView.frame)/2 - CGRectGetHeight(self.view.frame)/2);
            [wself.view layoutIfNeeded];
        }];
    }];
    
    [[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillHideNotification object:nil] subscribeNext:^(id x) {
        
        [UIView animateWithDuration:0.4 animations:^{
            wself.alertViewCenterYConstraint.constant = 0;
            [wself.view layoutIfNeeded];
        }];
    }];
}

- (void)dismiss
{
    [self dismissWithComplete:NULL];
}

- (void)dismissWithComplete:(void(^)(void))complete
{
    [UIView animateWithDuration:0.2 animations:^{
        self.alertViewCenterYConstraint.constant = CGRectGetMaxY(self.view.frame) + CGRectGetHeight(self.alertView.frame)/2.f + 10;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self dismissViewControllerAnimated:NO completion:^{
            if (complete) {
                complete();
            }
        }];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
