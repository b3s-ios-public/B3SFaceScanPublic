//
//  B3SAlertNormalViewController.m
//  body3dscale
//
//  Created on 06/03/2018.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SAlertNormalViewController.h"
#import <ReactiveCocoa.h>

@interface B3SAlertNormalViewController ()

@property (weak, nonatomic) IBOutlet UIButton *okButton;

@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@end

@implementation B3SAlertNormalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    __weak typeof(self) wself = self;
    self.cancelButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        
        if (wself.cancelBlock) {
            wself.cancelBlock();
            wself.cancelBlock = nil;
        }
        
        [wself dismiss];
        return [RACSignal empty];
    }];
    
    self.okButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        if (wself.complete) {
            wself.complete();
            wself.complete = nil;
        }
        
        [wself dismiss];
        return [RACSignal empty];
    }];
    
    [RACObserve(self, text) subscribeNext:^(NSString *x) {
        wself.textLabel.text = x;
    }];
    
    [RACObserve(self, ok_text) subscribeNext:^(NSString *x) {
        if (x) {
            [wself.okButton setTitle:x forState:UIControlStateNormal];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
