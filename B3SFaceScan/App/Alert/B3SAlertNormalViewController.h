//
//  B3SAlertNormalViewController.h
//  body3dscale
//
//  Created on 06/03/2018.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <UIKit/UIKit.h>
#import "B3SAlertController.h"

@interface B3SAlertNormalViewController : B3SAlertController

@property (nonatomic, copy) dispatch_block_t complete;

@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *ok_text;

@end
