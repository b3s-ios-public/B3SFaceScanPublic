//
//  B3SDate.m
//  B3SCommon
//
//  Created on 2019/2/27.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SDateHelper.h"

#import <Foundation/Foundation.h>
#import "B3SLocalizedHelper.h"

@implementation B3SDateHelper

NSString *DATE_FORMAT = @"yyyy-MM-dd HH:mm:ss";
NSString *YEAR_MOUTH_FORMAT = @"yy年-MM月";
NSString *DAY_TIME_FORMAT = @"dd日 HH:mm";

NSString *DATE_FORMAT_EN = @"dd-MM-yyyy HH:mm:ss";
NSString *YEAR_MOUTH_FORMAT_EN = @"MM-yyyy";
NSString *DAY_TIME_FORMAT_EN = @"dd-MM HH:mm";

+ (NSString *)getFormatTime:(NSDate *)date format:(NSString *)format
{
    NSDateFormatter *formart = [[NSDateFormatter alloc]init];
    [formart setDateFormat:format];
    return [formart stringFromDate:date];
}

+ (NSString *)getLocalizedDate:(NSDate *)date
{
    if([B3SLocalizedHelper isChinese]) {
        return [self getFormatTime:date format:DATE_FORMAT];
    } else {
        return [self getFormatTime:date format:DATE_FORMAT_EN];
    }
}


+ (NSString *)getLocalizedYearMouth:(NSDate *)date
{
    if([B3SLocalizedHelper isChinese]) {
        return [self getFormatTime:date format:YEAR_MOUTH_FORMAT];
    } else {
        return [self getFormatTime:date format:YEAR_MOUTH_FORMAT_EN];
    }
}

+ (NSString *)getLocalizedDayTime:(NSDate *)date
{
    if([B3SLocalizedHelper isChinese]) {
        return [self getFormatTime:date format:DAY_TIME_FORMAT];
    } else {
        return [self getFormatTime:date format:DAY_TIME_FORMAT_EN];
    }
}

@end
