//
//  B3SSceneHelper.m
//  B3SCommon
//
//  Created on 08/03/2018.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SSceneHelper.h"
#import <ModelIO/ModelIO.h>
#import <SceneKit/ModelIO.h>

@implementation B3SSceneHelper

+ (SCNScene *)getSceneWithHeadByNode:(SCNNode *)node modelDirUrl:(NSURL *)url
{
    SCNScene *scene = [SCNScene scene];
    
    // 创建模型, 务必先放大再旋转
    MDLAsset *asset = [[MDLAsset alloc] initWithURL:[url URLByAppendingPathComponent:@"mesh.obj"]];
    node = [SCNNode nodeWithMDLObject:[asset objectAtIndex:0]];
    
    node.position = SCNVector3Make(0, 0, 0);
    // 放大2倍
    node.transform = SCNMatrix4MakeScale(2.2, 2.2, 2.2);
    // 随着x轴自旋转
    //    _node.rotation = SCNVector4Make(4, 0, 0, M_PI);
        
    SCNMaterial *mat = [SCNMaterial material];
    NSData *data = [NSData dataWithContentsOfURL:[url URLByAppendingPathComponent:@"mesh.png"]];
    UIImage *image = [UIImage imageWithData:data];;
    mat.diffuse.contents = image;
    if ([[UIDevice currentDevice].systemVersion floatValue] < 12.0) {
        mat.diffuse.contentsTransform = SCNMatrix4Translate(SCNMatrix4MakeScale(1, -1, 1), 0, 1, 0);
    }
    node.geometry.materials = @[mat];
    

    [scene.rootNode addChildNode:node];
    
    // 创建相机
    SCNNode *cameraNode = [SCNNode node];
    cameraNode.camera = [SCNCamera camera];
    cameraNode.camera.xFov = 20;
    cameraNode.camera.yFov = 20;
    cameraNode.position = SCNVector3Make(0, 0, 1.8);
    [scene.rootNode addChildNode:cameraNode];
    
    return scene;
}

+ (SCNVector3)getNewVector3WithTransform:(SCNMatrix4)transform oldVector3:(SCNVector3)vec
{
    simd_float4 float4 = simd_mul(SCNVector4ToFloat4(SCNVector4Make(vec.x, vec.y, vec.z, 1)), SCNMatrix4ToMat4(transform));
    SCNVector4 v4 = SCNVector4FromFloat4(float4);
    return SCNVector3Make(v4.x, v4.y, v4.z);
}

@end
