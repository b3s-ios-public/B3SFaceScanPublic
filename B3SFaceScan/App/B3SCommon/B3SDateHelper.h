//
//  B3SDate.h
//  B3SCommon
//
//  Created on 2019/2/27.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <Foundation/Foundation.h>

@interface B3SDateHelper : NSObject

/**
 * 获取时间格式化
 *
 * @param date          日期
 * @return 本地化日期
 */
+ (NSString *)getFormatTime:(NSDate *)date format:(NSString *)format;

/**
 * 获取本地化日期（当前只区分英文和中文日期格式）
 *
 * @param date          日期
 * @return 本地化日期
 */
+ (NSString *)getLocalizedDate:(NSDate *)date;

/**
 * 获取本地化年月（当前只区分英文和中文日期格式）
 *
 * @param date          日期
 * @return 本地化日期
 */
+ (NSString *)getLocalizedYearMouth:(NSDate *)date;

/**
 * 获取本地化日时分（当前只区分英文和中文日期格式）
 *
 * @param date          日期
 * @return 本地化日期
 */
+ (NSString *)getLocalizedDayTime:(NSDate *)date;

@end
