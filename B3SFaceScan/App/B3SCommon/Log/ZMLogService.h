//
//  ZMLogService.h
//  zm-doctor
//
//  Created by yh on 2018/8/30.
//  Copyright © 2018年 . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZMLogService : NSObject

+ (instancetype)sharedInstance;

- (void)printSyetemInfo;

- (void)uploadCurrentLog;

- (BOOL)isUploading;

- (void)setDeviceModelName:(NSString *)name;

@end
