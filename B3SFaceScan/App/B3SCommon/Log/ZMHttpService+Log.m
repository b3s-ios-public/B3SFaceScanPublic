//
//  ZMHttpService+Log.m
//  zm-doctor
//
//  Created by yh on 2018/8/31.
//  Copyright © 2018年 . All rights reserved.
//

#import "ZMHttpService+Log.h"
#import "ZMLogUploadDetail.h"
#import <AFNetworking.h>

@implementation ZMHttpService (Log)

- (void)addLogWithComplete:(CompleteBlock)complete
{
    NSDictionary *parameters = @{};
    
    NSString *url = [NSString stringWithFormat:@"%@/api/scan/log/add",defaultModelURL];
    
    [self.manager 
      POST:url 
      parameters:parameters 
      headers:nil
      progress:^(NSProgress * _Nonnull uploadProgress) {} 
      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
          
          NSNumber *code = [responseObject objectForKey:@"code"];
          if (code.integerValue == 20000) {
              NSError *error = nil;
              ZMLogUploadDetail *model = [[ZMLogUploadDetail alloc] initWithDictionary:[responseObject objectForKey:@"content"]  error:&error];
              if (complete) {
                  complete(model);
              }
          } else {
              if (complete) {
                  complete(nil);
              }
          }
      } 
      failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
          if (complete) {
              complete(nil);
          }
      }];
}

- (void)logCallBackWithObject:(NSString *)object bucket:(NSString *)bucket complete:(CompleteBlock)complete
{
    NSDictionary *parameters = @{
                                 @"object":object,
                                 @"bucket":bucket
                                 };
    NSString *url = [NSString stringWithFormat:@"%@/api/scan/log/callback",defaultModelURL];
    
    [self.manager 
      POST:url 
      parameters:parameters
      headers:nil 
      progress:^(NSProgress * _Nonnull uploadProgress) {} 
      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                NSNumber *code = [responseObject objectForKey:@"code"];
                if (code.integerValue == 20000) {
                    complete(@YES);
                } else {
                    if (complete) {
                        complete(nil);
                    }
                }
            } 
      failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
          if (complete) {
              complete(nil);
          }
      }];
}

@end
