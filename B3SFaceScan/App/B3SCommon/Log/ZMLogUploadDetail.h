//
//  ZMLogUploadDetail.h
//  zm-doctor
//
//  Created by yh on 2018/9/3.
//  Copyright © 2018年 . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZMJSONModel.h"

@interface ZMLogUploadDetail : ZMJSONModel

@property (nonatomic, copy) NSString *logid;
@property (nonatomic, copy) NSString *uploadPath;
@property (nonatomic, copy) NSString *accessKeyId;
@property (nonatomic, copy) NSString *accessKeySecret;
@property (nonatomic, copy) NSString *securityToken;
@property (nonatomic, copy) NSString *expiration;
@property (nonatomic, copy) NSString *bucketName;
@property (nonatomic, copy) NSString *endPoint;
@property (nonatomic, copy) NSString *zipcode;

@end
