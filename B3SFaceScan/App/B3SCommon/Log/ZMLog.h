//
//  ZMLog.h
//  zm-doctor
//
//  Created by  on 2018/5/4.
//  Copyright © 2018 . All rights reserved.
//

#ifndef ZMLog_h
#define ZMLog_h

#import <CocoaLumberjack/CocoaLumberjack.h>

// 设置日志级别
#ifdef DEBUG
static const DDLogLevel ddLogLevel = DDLogLevelVerbose;
#else
static const DDLogLevel ddLogLevel = DDLogLevelInfo;
#endif

#ifndef DEBUG
//#define NSLog(fmt, ...) external_NSLog(fmt,  ## __VA_ARGS__ )
#define NSLog(fmt, ...) DDLogInfo(fmt,  ## __VA_ARGS__);
#endif

#endif /* ZMLog_h */
