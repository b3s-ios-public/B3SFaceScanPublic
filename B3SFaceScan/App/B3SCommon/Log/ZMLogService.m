//
//  ZMLogService.m
//  zm-doctor
//
//  Created by yh on 2018/8/30.
//  Copyright © 2018年 . All rights reserved.
//

#import "ZMLogService.h"
#import <Aspects.h>
#import <YYKit.h>
#import "ZMLog.h"
#import <BlocksKit.h>
#import <ReactiveCocoa.h>
#import <Aspects.h>
#import <SSZipArchive.h>
#import "ZMHttpService+Log.h"
#import <OSSService.h>
//#import "fishhook.h"
#import "ZMLogUploadDetail.h"

static void (*orig_NSLog)(NSString *format, ...);
void(new_NSLog)(NSString *format, ...) {
    va_list args;
    if(format) {
        va_start(args, format);
        NSString *message = [[NSString alloc] initWithFormat:format arguments:args];
        orig_NSLog(@"%@", message);
       // DDLogInfo(message);
        va_end(args);
    }
}

void(main_self)(void) {
 //   rebind_symbols((struct rebinding[1]){{"NSLog", new_NSLog, (void *)&orig_NSLog}}, 1);
}

@interface ZMLogService ()

@property (nonatomic, strong) DDFileLogger *fileLogger;

@property (nonatomic, copy) NSString *logsDirectory;

@property (nonatomic, strong) NSMutableSet *clients;

@property (nonatomic, copy) NSString *deviceModelName;

@end

@implementation ZMLogService

+ (instancetype)sharedInstance
{
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        main_self();
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.clients = [NSMutableSet set];
        
        NSError *error = nil;
        @weakify(self);
        [DDLogFileManagerDefault aspect_hookSelector:@selector(logFileDateFormatter) withOptions:AspectPositionAfter usingBlock:^(id<AspectInfo> info) {
            __autoreleasing NSDateFormatter *formatter = nil;
            [info.originalInvocation getReturnValue:&formatter];
            [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Shanghai"]];
        } error:&error];
        
        [DDLogFileManagerDefault aspect_hookSelector:@selector(defaultLogsDirectory) withOptions:AspectPositionInstead usingBlock:^(id<AspectInfo> info) {
            @strongify(self);
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *baseDir = paths.firstObject;
            id logsDirectory = [baseDir stringByAppendingPathComponent:@"Logs"];
            [info.originalInvocation setReturnValue:&logsDirectory];
            self.logsDirectory = logsDirectory;
        } error:&error];
        
//        //NSAssert(!error, @"hook error");
        
        // 初始化日志
#ifdef DEBUG
        [DDLog addLogger:[DDTTYLogger sharedInstance]]; // TTY = Xcode console
#endif
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *baseDir = paths.firstObject;
        id logsDirectory = [baseDir stringByAppendingPathComponent:@"Logs"];
        self.logsDirectory = logsDirectory;
        
        DDLogFileManagerDefault *myLogFileManager = [[DDLogFileManagerDefault alloc] initWithLogsDirectory:logsDirectory];
        DDFileLogger *fileLogger = [[DDFileLogger alloc] initWithLogFileManager:myLogFileManager]; // File Logger
        fileLogger.rollingFrequency = 60 * 60 * 24; // 24 hour rolling
        fileLogger.logFileManager.maximumNumberOfLogFiles = 8;
        fileLogger.maximumFileSize = 1024 * 1024 * 5; // 5M
        fileLogger.doNotReuseLogFiles = YES;
        [DDLog addLogger:fileLogger];
        self.fileLogger = fileLogger;
        _deviceModelName = nil;
    }
    return self;
}

- (BOOL)isUploading
{
    if (self.clients.count > 0) {
        return YES;
    }
    
    return NO;
}

- (void)printSyetemInfo
{
    NSLog(@"NSBundle Info : %@",[[NSBundle mainBundle].infoDictionary jsonPrettyStringEncoded]);
    NSLog(@"NSUserDefaults StandardUserDefaults : %@",[NSUserDefaults standardUserDefaults].dictionaryRepresentation);
    NSLog(@"systemVersion: %@",[UIDevice currentDevice].systemVersion);
    if(self.deviceModelName) {
        NSLog(@"machineModelName: %@",self.deviceModelName);
    } else {
        NSLog(@"machineModelName: %@",[UIDevice currentDevice].machineModelName);   //YYKit的这个可能不是很全！！
    }
    NSLog(@"NSLocaleCountryCode: %@",[[NSLocale currentLocale] objectForKey:NSLocaleCountryCode]);
    NSLog(@"NSLocaleLanguageCode: %@",[[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode]);
    NSLog(@"NSLocaleIdentifier: %@",[[NSLocale currentLocale] objectForKey:NSLocaleIdentifier]);
    NSLog(@"当前时区: %@",[[NSTimeZone localTimeZone] name]);
    NSLog(@"preferredLanguages: %@",[NSLocale preferredLanguages]);
}

- (void)uploadCurrentLog
{
    @weakify(self);
    [self.fileLogger rollLogFileWithCompletionBlock:^{
        @strongify(self);
        [self uploadLastLog];
    }];
}

- (void)uploadLastLog
{
    @weakify(self);
    [[ZMHttpService sharedInstance] addLogWithComplete:^(ZMLogUploadDetail *detail) {
        @strongify(self);
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *currentFileLog = self.fileLogger.currentLogFileInfo.filePath;
        NSArray *subLogPaths = [self.fileLogger.logFileManager.unsortedLogFilePaths bk_select:^BOOL(NSString *file) {
            if ([file isEqualToString:currentFileLog]) {
                return NO;
            }
            return YES;
        }];
        
        // 如果网络有异常, 立即清空本地日志
        if (!detail) {
            [self.fileLogger.logFileManager.unsortedLogFilePaths bk_each:^(NSString *filePath) {
                if ([fileManager fileExistsAtPath:filePath]) {
                    [fileManager removeItemAtPath:filePath error:nil];
                }
            }];
            return ;
        }
        
        NSString *zipFilePath = [self.logsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.zip",detail.logid]];
        
        NSString *currentLogDir = [self.logsDirectory stringByAppendingPathComponent:@"ddd"];
        NSError *error = nil;
        
        if (![fileManager fileExistsAtPath:currentLogDir]) {
            [fileManager createDirectoryAtPath:currentLogDir withIntermediateDirectories:YES attributes:@{} error:&error];
        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            @strongify(self);
            
            [SSZipArchive createZipFileAtPath:zipFilePath withFilesAtPaths:subLogPaths withPassword:detail.zipcode];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([fileManager fileExistsAtPath:zipFilePath]) {
                    // 删除已经打包的日志
                    for (NSString *logFilePath in subLogPaths) {
                        if ([fileManager fileExistsAtPath:logFilePath]) {
                            [fileManager removeItemAtPath:logFilePath error:nil];
                        }
                    }
                    
                    CGFloat fileSize = 0.f;
                    NSDictionary *fileDic = [fileManager attributesOfItemAtPath:zipFilePath error:nil];//获取文件的属性
                    unsigned long long size = [[fileDic objectForKey:NSFileSize] longLongValue];
                    fileSize = 1.0*size/1024.0;
                    
                    [[self ossUploadbyDetail:detail zipFilePath:zipFilePath] subscribeNext:^(OSSTask *task) {
                        if ([[NSFileManager defaultManager] fileExistsAtPath:zipFilePath]) {
                            NSError *error = nil;
                            [[NSFileManager defaultManager] removeItemAtPath:zipFilePath error:&error];
                            if (error) {
                                NSLog(@"delZipFile Fail : %@",error);
                            }
                        }
                    } error:^(NSError *error) {
                        NSLog(@"upload object failed, error: %@", error);
                    } completed:^{
                    }];
                    
                } else {
                    NSLog(@"生成zip文件失败.");
                }
            });
        });
    }];
}

- (RACSignal *)ossUploadbyDetail:(ZMLogUploadDetail *)detail zipFilePath:(NSString *)zipFilePath
{
    if (!zipFilePath || !detail) {
        return nil;
    }
    
    @weakify(self);
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self);
        OSSStsTokenCredentialProvider *provider = [[OSSStsTokenCredentialProvider alloc] initWithAccessKeyId:detail.accessKeyId secretKeyId:detail.accessKeySecret securityToken:detail.securityToken];
        OSSClient *client = [[OSSClient alloc] initWithEndpoint:detail.endPoint credentialProvider:provider];
        
        [self.clients addObject:client];
        OSSPutObjectRequest * put = [OSSPutObjectRequest new];
        put.bucketName = detail.bucketName;
        put.objectKey = [detail.uploadPath stringByAppendingPathComponent:zipFilePath.lastPathComponent];
        put.uploadingData = [NSData dataWithContentsOfFile:zipFilePath];
        
        // 设置回调参数
        put.callbackParam = @{
                              @"callbackUrl": [NSString stringWithFormat:@"%@/api/scan/log/callback",OSS_CallBack_URL],
                              @"callbackBody": [NSString stringWithFormat:@"bucket=%@&object=%@",put.bucketName,put.objectKey]
                              };
        
        OSSTask * putTask = [client putObject:put];
        [putTask continueWithBlock:^id(OSSTask *task) {
            if (!task.error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [subscriber sendNext:task];
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [subscriber sendError:task.error];
                });
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                @strongify(self);
                [subscriber sendCompleted];
                [self.clients removeObject:client];
            });
            return nil;
        }];
        return nil;
    }];
}

- (void)setDeviceModelName:(NSString *)name
{
    _deviceModelName = name;
}

@end

