//
//  ZMHttpService+Log.h
//  zm-doctor
//
//  Created by yh on 2018/8/31.
//  Copyright © 2018年 . All rights reserved.
//

#import "ZMHttpService.h"
//#import "ZMLogUploadDetail.h"

@interface ZMHttpService (Log)

- (void)addLogWithComplete:(CompleteBlock)complete;

- (void)logCallBackWithObject:(NSString *)object bucket:(NSString *)bucket complete:(CompleteBlock)complete;

@end
