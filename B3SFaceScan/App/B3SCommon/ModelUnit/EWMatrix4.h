//
//  EWMatrix4.h
//  eyewear-iOS
//
//  Created on 2019/2/27.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SceneKit/SceneKit.h>
#import "EWVector3.h"
#import "EWQuaternion.h"

NS_ASSUME_NONNULL_BEGIN
// 采用跟 SCNMatrix4 一样的元素标号

@interface EWMatrix4 : NSObject

@property (assign, nonatomic) double m11;
@property (assign, nonatomic) double m12;
@property (assign, nonatomic) double m13;
@property (assign, nonatomic) double m14;

@property (assign, nonatomic) double m21;
@property (assign, nonatomic) double m22;
@property (assign, nonatomic) double m23;
@property (assign, nonatomic) double m24;

@property (assign, nonatomic) double m31;
@property (assign, nonatomic) double m32;
@property (assign, nonatomic) double m33;
@property (assign, nonatomic) double m34;

@property (assign, nonatomic) double m41;
@property (assign, nonatomic) double m42;
@property (assign, nonatomic) double m43;
@property (assign, nonatomic) double m44;

- (instancetype)makeWithSCNMatrix4:(SCNMatrix4)m;

- (instancetype)setPosition:(EWVector3 *)position;

- (instancetype)makeBasis:(EWVector3 *)xAxis
            yAxis:(EWVector3 *)yAxis
            zAxis:(EWVector3 *)zAxis;

- (instancetype)multiply:(EWMatrix4 *)m n:(nullable EWMatrix4 *)n;

- (instancetype)multiplyMatrices:(EWMatrix4 *)m n:(EWMatrix4 *)n;

- (instancetype)makeRotationX:(double)theta;

- (instancetype)makeRotationZ:(double)theta;

- (instancetype)copy:(EWMatrix4 *)m;

- (instancetype)decompose:(EWVector3 *)position
               quaternion:(EWQuaternion *)quaternion
                    scale:(EWVector3 *)scale;

- (instancetype)premultiply:(EWMatrix4 *)m;

- (instancetype)compose:(EWVector3 *)position
             quaternion:(EWQuaternion *)quaternion
                  scale:(EWVector3 *)scale;

- (instancetype)getInverse:(EWMatrix4 *)m;

- (NSArray *)toArray;

- (instancetype)clone;

- (SCNMatrix4)toSCNMatrix4;
@end

NS_ASSUME_NONNULL_END
