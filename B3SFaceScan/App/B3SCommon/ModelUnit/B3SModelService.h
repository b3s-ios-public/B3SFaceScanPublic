//
//  WavefrontOBJ.h
//  OpenGLESLearn
//
//  Created on 2017/6/20.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <Foundation/Foundation.h>

@class B3SModelVector3;

typedef enum : NSInteger {
    B3SModelServicePrecisionUnitMeter ,
    B3SModelServicePrecisionUnitDecimeter ,
    B3SModelServicePrecisionUnitCentimeter ,
    B3SModelServicePrecisionUnitMillimeter,
    B3SModelServicePrecisionUnitUnknown,
} B3SModelServiceModelUnit;

@interface B3SModelService : NSObject

@property (assign, nonatomic) B3SModelServiceModelUnit modelUnitForExporting;
/**保存模型的顶点坐标数组, 单位总是为米*/
@property (strong, nonatomic) NSMutableArray *vertexArray;
/**保存模型的顶点法向量坐标数组, 没有单位*/
@property (strong, nonatomic) NSMutableArray *normalArray;
/**保存模型的顶点纹理坐标数组, 没有单位*/
@property (strong, nonatomic) NSMutableArray *uvArray;
/**保存模型的三角面数组, 没有单位*/
@property (strong, nonatomic) NSMutableArray *triangleFaceArray;
/**保存模型的注释行*/
@property (strong, nonatomic) NSMutableArray *commentArray;
/**保存模型的3d特征顶点编号*/
@property (strong, nonatomic) NSMutableArray *ldmk3dYMLArray;

/**
 从本地加载obj文件内容到内存中, 构造内存中的顶点数组/纹理坐标数据/面数组等. (内存中顶点数组总是以米为单位)
 
 @param url 本地obj文件路径, file:// 格式的全路径
 @param knownUnit 若您已知该文件中的顶点坐标是什么量纲, 请指定该参数为对应的量纲枚举,有助于加速加载obj文件.
                若您无法确定该文件中顶点坐标是什么量纲,请传B3SModelServicePrecisionUnitUnknown.
 */
- (void)loadDataFromObjUrl:(NSURL *)url
                  withUnit:(B3SModelServiceModelUnit) knownUnit;

// 模型编号从1开始计数
- (B3SModelVector3 *)getVector3WithIndex:(NSInteger)index;

/**
  将内存的模型数据(含顶点数组/面数组等), 序列化成obj格式的字符串. (使用当前用户设置项中的导出量纲)
@return 返回的字符串可以直接写入到.obj文件中.
 */
- (NSString *)exportObj;

/**
  将内存的模型数据(含顶点数组/面数组等), 序列化成obj格式的字符串.
   @param forceUnit 指定导出时量纲. 若为B3SModelServicePrecisionUnitUnknown, 则会使用用户设置项中设定的导出量纲.
 @return 返回的字符串可以直接写入到.obj文件中.
 */
- (NSString *)exportObjWithUnit:(B3SModelServiceModelUnit) forceUnit;


//判断obj文件是什么量纲单位.(默认是米)
-(B3SModelServiceModelUnit) determineModelUnitOfObjFile:(NSURL *)url;


@end
