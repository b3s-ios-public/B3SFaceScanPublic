//
//  EWPoint.m
//  eyewear-iOS
//
//  Created on 2019/1/22.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SModelPoint.h"

@implementation B3SModelPoint

+ (instancetype)makeX:(float)x y:(float)y
{
    B3SModelPoint *point = [B3SModelPoint new];
    point.x = x;
    point.y = y;
    return point;
}

- (CGPoint)toCGPoint
{
    CGPoint point = CGPointMake(self.x, self.y);
    return point;
}

@end
