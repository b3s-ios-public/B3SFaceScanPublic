//
//  EWQuaternion.h
//  eyewear-iOS
//
//  Created on 2019/2/27.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SceneKit/SceneKit.h>

@class EWMatrix4;
@class EWVector3;

NS_ASSUME_NONNULL_BEGIN

@interface EWQuaternion : NSObject

@property (assign, nonatomic) double x;
@property (assign, nonatomic) double y;
@property (assign, nonatomic) double z;
@property (assign, nonatomic) double w;

+ (instancetype)makeX:(double)x
                    y:(double)y
                    z:(double)z
                    w:(double)w;

- (instancetype)setFromRotationMatrix:(EWMatrix4 *)m;

- (instancetype)setFromAxisAngle:(EWVector3 *)axis
                           angle:(double)angle;

- (instancetype)setFromUnitVectors:(EWVector3 *)vFrom vTo:(EWVector3 *)vTo;

- (NSArray *)toArray;

- (SCNVector4)toSCNVector4;

@end

NS_ASSUME_NONNULL_END
