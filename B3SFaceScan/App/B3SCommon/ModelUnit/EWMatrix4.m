//
//  EWMatrix4.m
//  eyewear-iOS
//
//  Created on 2019/2/27.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "EWMatrix4.h"
#include <math.h>

@implementation EWMatrix4


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.m11 = 1.f;
        self.m22 = 1.f;
        self.m33 = 1.f;
        self.m44 = 1.f;
    }
    return self;
}

- (instancetype)setPosition:(EWVector3 *)position
{
    self.m14 = position.x;
    self.m24 = position.y;
    self.m34 = position.z;
    
    return self;
}

- (instancetype)makeWithSCNMatrix4:(SCNMatrix4)m
{
    EWMatrix4 *mat = self;
    mat.m11 = m.m11;
    mat.m12 = m.m12;
    mat.m13 = m.m13;
    mat.m14 = m.m14;
    
    mat.m21 = m.m21;
    mat.m22 = m.m22;
    mat.m23 = m.m23;
    mat.m24 = m.m24;
    
    mat.m31 = m.m31;
    mat.m32 = m.m32;
    mat.m33 = m.m33;
    mat.m34 = m.m34;
    
    mat.m41 = m.m41;
    mat.m42 = m.m42;
    mat.m43 = m.m43;
    mat.m44 = m.m44;
    
    return mat;
}

- (instancetype)makeBasis:(EWVector3 *)xAxis
            yAxis:(EWVector3 *)yAxis
            zAxis:(EWVector3 *)zAxis
{
    self.m11 = xAxis.x;
    self.m12 = yAxis.x;
    self.m13 = zAxis.x;
    self.m14 = 0;
    
    self.m21 = xAxis.y;
    self.m22 = yAxis.y;
    self.m23 = zAxis.y;
    self.m24 = 0;
    
    self.m31 = xAxis.z;
    self.m32 = yAxis.z;
    self.m33 = zAxis.z;
    self.m34 = 0;
    
    self.m41 = 0;
    self.m42 = 0;
    self.m43 = 0;
    self.m44 = 1;
    
    return self;
}

- (instancetype)multiply:(EWMatrix4 *)m n:(nullable EWMatrix4 *)n
{
    if (n) {
        return [self multiplyMatrices:m n:n];
    }
    return [self multiplyMatrices:[self clone] n:m];
}

//- (instancetype)multiplyMatrices:(EWMatrix4 *)m n:(EWMatrix4 *)n
//{
//    self.m11 = m.m11 * n.m11 + m.m12 * n.m21 + m.m13 * n.m31 + m.m14 * n.m41;
//    self.m12 = m.m11 * n.m12 + m.m12 * n.m22 + m.m13 * n.m32 + m.m14 * n.m42;
//    self.m13 = m.m11 * n.m13 + m.m12 * n.m23 + m.m13 * n.m33 + m.m14 * n.m43;
//    self.m14 = m.m11 * n.m14 + m.m12 * n.m24 + m.m13 * n.m34 + m.m14 * n.m44;
//
//    self.m21 = m.m21 * n.m11 + m.m22 * n.m21 + m.m23 * n.m31 + m.m24 * n.m41;
//    self.m22 = m.m21 * n.m12 + m.m22 * n.m22 + m.m23 * n.m32 + m.m24 * n.m42;
//    self.m23 = m.m21 * n.m13 + m.m22 * n.m23 + m.m23 * n.m33 + m.m24 * n.m43;
//    self.m24 = m.m21 * n.m14 + m.m22 * n.m24 + m.m23 * n.m34 + m.m24 * n.m44;
//
//    self.m31 = m.m31 * n.m11 + m.m32 * n.m21 + m.m33 * n.m31 + m.m34 * n.m41;
//    self.m32 = m.m31 * n.m12 + m.m32 * n.m22 + m.m33 * n.m32 + m.m34 * n.m42;
//    self.m33 = m.m31 * n.m13 + m.m32 * n.m23 + m.m33 * n.m33 + m.m34 * n.m43;
//    self.m34 = m.m31 * n.m14 + m.m32 * n.m24 + m.m33 * n.m34 + m.m34 * n.m44;
//
//    self.m41 = m.m41 * n.m11 + m.m42 * n.m21 + m.m43 * n.m31 + m.m44 * n.m41;
//    self.m42 = m.m41 * n.m12 + m.m42 * n.m22 + m.m43 * n.m32 + m.m44 * n.m42;
//    self.m43 = m.m41 * n.m13 + m.m42 * n.m23 + m.m43 * n.m33 + m.m44 * n.m43;
//    self.m44 = m.m41 * n.m14 + m.m42 * n.m24 + m.m43 * n.m34 + m.m44 * n.m44;
//
//    return self;
//}

- (instancetype)multiplyMatrices:(EWMatrix4 *)m n:(EWMatrix4 *)n
{
//    var ae = a.elements;
//    var be = b.elements;
//    var te = this.elements;
    
    double a11 = m.m11, a12 = m.m12, a13 = m.m13, a14 = m.m14;
    double a21 = m.m21, a22 = m.m22, a23 = m.m23, a24 = m.m24;
    double a31 = m.m31, a32 = m.m32, a33 = m.m33, a34 = m.m34;
    double a41 = m.m41, a42 = m.m42, a43 = m.m43, a44 = m.m44;
    
    double b11 = n.m11, b12 = n.m12, b13 = n.m13, b14 = n.m14;
    double b21 = n.m21, b22 = n.m22, b23 = n.m23, b24 = n.m24;
    double b31 = n.m31, b32 = n.m32, b33 = n.m33, b34 = n.m34;
    double b41 = n.m41, b42 = n.m42, b43 = n.m43, b44 = n.m44;
    
    self.m11 = a11 * b11 + a12 * b21 + a13 * b31 + a14 * b41;
    self.m12 = a11 * b12 + a12 * b22 + a13 * b32 + a14 * b42;
    self.m13 = a11 * b13 + a12 * b23 + a13 * b33 + a14 * b43;
    self.m14 = a11 * b14 + a12 * b24 + a13 * b34 + a14 * b44;
    
    self.m21 = a21 * b11 + a22 * b21 + a23 * b31 + a24 * b41;
    self.m22 = a21 * b12 + a22 * b22 + a23 * b32 + a24 * b42;
    self.m23 = a21 * b13 + a22 * b23 + a23 * b33 + a24 * b43;
    self.m24 = a21 * b14 + a22 * b24 + a23 * b34 + a24 * b44;
    
    self.m31 = a31 * b11 + a32 * b21 + a33 * b31 + a34 * b41;
    self.m32 = a31 * b12 + a32 * b22 + a33 * b32 + a34 * b42;
    self.m33 = a31 * b13 + a32 * b23 + a33 * b33 + a34 * b43;
    self.m34 = a31 * b14 + a32 * b24 + a33 * b34 + a34 * b44;
    
    self.m41 = a41 * b11 + a42 * b21 + a43 * b31 + a44 * b41;
    self.m42 = a41 * b12 + a42 * b22 + a43 * b32 + a44 * b42;
    self.m43 = a41 * b13 + a42 * b23 + a43 * b33 + a44 * b43;
    self.m44 = a41 * b14 + a42 * b24 + a43 * b34 + a44 * b44;
    
    return self;
}

- (instancetype)makeRotationZ:(double)theta
{
    double c = cos(theta);
    double s = sin(theta);
    
    self.m11 = c;
    self.m12 = -s;
    self.m21 = s;
    self.m22 = c;
    self.m33 = 1;
    self.m44 = 1;
    
    return self;
}

- (instancetype)makeRotationX:(double)theta
{
    double c = cos(theta);
    double s = sin(theta);
    
    self.m11 = 1.f;
    self.m22 = c;
    self.m23 = -s;
    self.m32 = s;
    self.m33 = c;
    self.m44 = 1.f;
    
    return self;
}


- (instancetype)decompose:(EWVector3 *)position
               quaternion:(EWQuaternion *)quaternion
                    scale:(EWVector3 *)scale
{
        
    EWMatrix4 *matrix = [EWMatrix4 new];
    EWVector3 *vector = [EWVector3 new];
    vector.x = self.m11;
    
    double sx = [[EWVector3 makeX:self.m11 y:self.m21 z:self.m31] length];
    double sy = [[EWVector3 makeX:self.m12 y:self.m22 z:self.m32] length];
    double sz = [[EWVector3 makeX:self.m13 y:self.m23 z:self.m33] length];
    
    // if determine is negative, we need to invert one scale
    double det = [self determinant];
    if ( det < 0 ) {
        sx = - sx;
    }
        
    position.x = self.m14;
    position.y = self.m24;
    position.z = self.m34;
        
    // scale the rotation part
    [matrix copy:self];
        
    double invSX = 1.f / sx;
    double invSY = 1.f / sy;
    double invSZ = 1.f / sz;
    
    matrix.m11 *= invSX;
    matrix.m21 *= invSX;
    matrix.m31 *= invSX;
    
    matrix.m12 *= invSY;
    matrix.m22 *= invSY;
    matrix.m32 *= invSY;
    
    matrix.m13 *= invSZ;
    matrix.m23 *= invSZ;
    matrix.m33 *= invSZ;
        
    [quaternion setFromRotationMatrix:matrix];
        
    scale.x = sx;
    scale.y = sy;
    scale.z = sz;
    
    return self;
    
}

- (double)determinant
{
    
    double n11 = self.m11, n12 = self.m12, n13 = self.m13, n14 = self.m14;
    double n21 = self.m21, n22 = self.m22, n23 = self.m23, n24 = self.m24;
    double n31 = self.m31, n32 = self.m32, n33 = self.m33, n34 = self.m34;
    double n41 = self.m41, n42 = self.m42, n43 = self.m43, n44 = self.m44;
    
    //TODO: make this more efficient
    //( based on http://www.euclideanspace.com/maths/algebra/matrix/functions/inverse/fourD/index.htm )
    
    return (
            n41 * (
                   + n14 * n23 * n32
                   - n13 * n24 * n32
                   - n14 * n22 * n33
                   + n12 * n24 * n33
                   + n13 * n22 * n34
                   - n12 * n23 * n34
                   ) +
            n42 * (
                   + n11 * n23 * n34
                   - n11 * n24 * n33
                   + n14 * n21 * n33
                   - n13 * n21 * n34
                   + n13 * n24 * n31
                   - n14 * n23 * n31
                   ) +
            n43 * (
                   + n11 * n24 * n32
                   - n11 * n22 * n34
                   - n14 * n21 * n32
                   + n12 * n21 * n34
                   + n14 * n22 * n31
                   - n12 * n24 * n31
                   ) +
            n44 * (
                   - n13 * n22 * n31
                   - n11 * n23 * n32
                   + n11 * n22 * n33
                   + n13 * n21 * n32
                   - n12 * n21 * n33
                   + n12 * n23 * n31
                   )
            
            );
}

- (instancetype)copy:(EWMatrix4 *)m
{
    EWMatrix4 *mat = self;
    mat.m11 = m.m11;
    mat.m12 = m.m12;
    mat.m13 = m.m13;
    mat.m14 = m.m14;
    
    mat.m21 = m.m21;
    mat.m22 = m.m22;
    mat.m23 = m.m23;
    mat.m24 = m.m24;
    
    mat.m31 = m.m31;
    mat.m32 = m.m32;
    mat.m33 = m.m33;
    mat.m34 = m.m34;
    
    mat.m41 = m.m41;
    mat.m42 = m.m42;
    mat.m43 = m.m43;
    mat.m44 = m.m44;
    
    return mat;
}

- (instancetype)clone
{
    EWMatrix4 *mat = [EWMatrix4 new];
    mat.m11 = self.m11;
    mat.m12 = self.m12;
    mat.m13 = self.m13;
    mat.m14 = self.m14;
    
    mat.m21 = self.m21;
    mat.m22 = self.m22;
    mat.m23 = self.m23;
    mat.m24 = self.m24;
    
    mat.m31 = self.m31;
    mat.m32 = self.m32;
    mat.m33 = self.m33;
    mat.m34 = self.m34;
    
    mat.m41 = self.m41;
    mat.m42 = self.m42;
    mat.m43 = self.m43;
    mat.m44 = self.m44;
    
    return mat;
}

- (instancetype)premultiply:(EWMatrix4 *)m
{
    return [self multiplyMatrices:m b:[self clone]];
}

- (instancetype)multiplyMatrices:(EWMatrix4 *)a b:(EWMatrix4 *)b
{
    
    double a11 = a.m11, a12 = a.m12, a13 = a.m13, a14 = a.m14;
    double a21 = a.m21, a22 = a.m22, a23 = a.m23, a24 = a.m24;
    double a31 = a.m31, a32 = a.m32, a33 = a.m33, a34 = a.m34;
    double a41 = a.m41, a42 = a.m42, a43 = a.m43, a44 = a.m44;
    
    double b11 = b.m11, b12 = b.m12, b13 = b.m13, b14 = b.m14;
    double b21 = b.m21, b22 = b.m22, b23 = b.m23, b24 = b.m24;
    double b31 = b.m31, b32 = b.m32, b33 = b.m33, b34 = b.m34;
    double b41 = b.m41, b42 = b.m42, b43 = b.m43, b44 = b.m44;
    
    self.m11 = a11 * b11 + a12 * b21 + a13 * b31 + a14 * b41;
    self.m12 = a11 * b12 + a12 * b22 + a13 * b32 + a14 * b42;
    self.m13 = a11 * b13 + a12 * b23 + a13 * b33 + a14 * b43;
    self.m14 = a11 * b14 + a12 * b24 + a13 * b34 + a14 * b44;
    
    self.m21 = a21 * b11 + a22 * b21 + a23 * b31 + a24 * b41;
    self.m22 = a21 * b12 + a22 * b22 + a23 * b32 + a24 * b42;
    self.m23 = a21 * b13 + a22 * b23 + a23 * b33 + a24 * b43;
    self.m24 = a21 * b14 + a22 * b24 + a23 * b34 + a24 * b44;
    
    self.m31 = a31 * b11 + a32 * b21 + a33 * b31 + a34 * b41;
    self.m32 = a31 * b12 + a32 * b22 + a33 * b32 + a34 * b42;
    self.m33 = a31 * b13 + a32 * b23 + a33 * b33 + a34 * b43;
    self.m34 = a31 * b14 + a32 * b24 + a33 * b34 + a34 * b44;
    
    self.m41 = a41 * b11 + a42 * b21 + a43 * b31 + a44 * b41;
    self.m42 = a41 * b12 + a42 * b22 + a43 * b32 + a44 * b42;
    self.m43 = a41 * b13 + a42 * b23 + a43 * b33 + a44 * b43;
    self.m44 = a41 * b14 + a42 * b24 + a43 * b34 + a44 * b44;
    
    return self;
}

- (instancetype)identity
{
    return [EWMatrix4 new];
    
}

- (instancetype)compose:(EWVector3 *)position
             quaternion:(EWQuaternion *)quaternion
                  scale:(EWVector3 *)scale
{
    double x = quaternion.x, y = quaternion.y, z = quaternion.z, w = quaternion.w;
    double x2 = x + x,  y2 = y + y, z2 = z + z;
    double xx = x * x2, xy = x * y2, xz = x * z2;
    double yy = y * y2, yz = y * z2, zz = z * z2;
    double wx = w * x2, wy = w * y2, wz = w * z2;
    
    double sx = scale.x, sy = scale.y, sz = scale.z;
    
    self.m11 = ( 1 - ( yy + zz ) ) * sx;
    self.m21 = ( xy + wz ) * sx;
    self.m31 = ( xz - wy ) * sx;
    self.m41 = 0;
    
    self.m12 = ( xy - wz ) * sy;
    self.m22 = ( 1 - ( xx + zz ) ) * sy;
    self.m32 = ( yz + wx ) * sy;
    self.m42 = 0;
    
    self.m13 = ( xz + wy ) * sz;
    self.m23 = ( yz - wx ) * sz;
    self.m33 = ( 1 - ( xx + yy ) ) * sz;
    self.m43 = 0;
    
    self.m14 = position.x;
    self.m24 = position.y;
    self.m34 = position.z;
    self.m44 = 1;
    
    return self;
}

- (instancetype)getInverse:(EWMatrix4 *)m
{
    
    double n11 = m.m11, n21 = m.m21, n31 = m.m31, n41 = m.m41;
    double n12 = m.m12, n22 = m.m22, n32 = m.m32, n42 = m.m42;
    double n13 = m.m13, n23 = m.m23, n33 = m.m33, n43 = m.m43;
    double n14 = m.m14, n24 = m.m24, n34 = m.m34, n44 = m.m44;
    
    double t11 = n23 * n34 * n42 - n24 * n33 * n42 + n24 * n32 * n43 - n22 * n34 * n43 - n23 * n32 * n44 + n22 * n33 * n44;
    double t12 = n14 * n33 * n42 - n13 * n34 * n42 - n14 * n32 * n43 + n12 * n34 * n43 + n13 * n32 * n44 - n12 * n33 * n44;
    double t13 = n13 * n24 * n42 - n14 * n23 * n42 + n14 * n22 * n43 - n12 * n24 * n43 - n13 * n22 * n44 + n12 * n23 * n44;
    double t14 = n14 * n23 * n32 - n13 * n24 * n32 - n14 * n22 * n33 + n12 * n24 * n33 + n13 * n22 * n34 - n12 * n23 * n34;
    
    double det = n11 * t11 + n21 * t12 + n31 * t13 + n41 * t14;
    
    if ( det == 0 ) {
        
        return [self identity];
        
    }
    
    double detInv = 1.f / det;
    
    self.m11 = t11 * detInv;
    self.m21 = ( n24 * n33 * n41 - n23 * n34 * n41 - n24 * n31 * n43 + n21 * n34 * n43 + n23 * n31 * n44 - n21 * n33 * n44 ) * detInv;
    self.m31 = ( n22 * n34 * n41 - n24 * n32 * n41 + n24 * n31 * n42 - n21 * n34 * n42 - n22 * n31 * n44 + n21 * n32 * n44 ) * detInv;
    self.m41 = ( n23 * n32 * n41 - n22 * n33 * n41 - n23 * n31 * n42 + n21 * n33 * n42 + n22 * n31 * n43 - n21 * n32 * n43 ) * detInv;
    
    self.m12 = t12 * detInv;
    self.m22 = ( n13 * n34 * n41 - n14 * n33 * n41 + n14 * n31 * n43 - n11 * n34 * n43 - n13 * n31 * n44 + n11 * n33 * n44 ) * detInv;
    self.m32 = ( n14 * n32 * n41 - n12 * n34 * n41 - n14 * n31 * n42 + n11 * n34 * n42 + n12 * n31 * n44 - n11 * n32 * n44 ) * detInv;
    self.m42 = ( n12 * n33 * n41 - n13 * n32 * n41 + n13 * n31 * n42 - n11 * n33 * n42 - n12 * n31 * n43 + n11 * n32 * n43 ) * detInv;
    
    self.m13 = t13 * detInv;
    self.m23 = ( n14 * n23 * n41 - n13 * n24 * n41 - n14 * n21 * n43 + n11 * n24 * n43 + n13 * n21 * n44 - n11 * n23 * n44 ) * detInv;
    self.m33 = ( n12 * n24 * n41 - n14 * n22 * n41 + n14 * n21 * n42 - n11 * n24 * n42 - n12 * n21 * n44 + n11 * n22 * n44 ) * detInv;
    self.m43 = ( n13 * n22 * n41 - n12 * n23 * n41 - n13 * n21 * n42 + n11 * n23 * n42 + n12 * n21 * n43 - n11 * n22 * n43 ) * detInv;
    
    self.m14 = t14 * detInv;
    self.m24 = ( n13 * n24 * n31 - n14 * n23 * n31 + n14 * n21 * n33 - n11 * n24 * n33 - n13 * n21 * n34 + n11 * n23 * n34 ) * detInv;
    self.m34 = ( n14 * n22 * n31 - n12 * n24 * n31 - n14 * n21 * n32 + n11 * n24 * n32 + n12 * n21 * n34 - n11 * n22 * n34 ) * detInv;
    self.m44 = ( n12 * n23 * n31 - n13 * n22 * n31 + n13 * n21 * n32 - n11 * n23 * n32 - n12 * n21 * n33 + n11 * n22 * n33 ) * detInv;
    
    return self;
    
}

- (NSArray *)toArray
{
    return @[@(self.m11),@(self.m21),@(self.m31),@(self.m41),
             @(self.m12),@(self.m22),@(self.m32),@(self.m42),
             @(self.m13),@(self.m23),@(self.m33),@(self.m43),
             @(self.m14),@(self.m24),@(self.m34),@(self.m44)];
}

- (SCNMatrix4)toSCNMatrix4
{
    EWMatrix4 *m = self;
    SCNMatrix4 mat;
    mat.m11 = m.m11;
    mat.m12 = m.m12;
    mat.m13 = m.m13;
    mat.m14 = m.m14;
    
    mat.m21 = m.m21;
    mat.m22 = m.m22;
    mat.m23 = m.m23;
    mat.m24 = m.m24;
    
    mat.m31 = m.m31;
    mat.m32 = m.m32;
    mat.m33 = m.m33;
    mat.m34 = m.m34;
    
    mat.m41 = m.m41;
    mat.m42 = m.m42;
    mat.m43 = m.m43;
    mat.m44 = m.m44;
    
    return mat;
}
@end
