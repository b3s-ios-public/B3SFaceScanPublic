//
//  WavefrontOBJ.m
//  OpenGLESLearn
//
//  Created on 2017/6/20.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SModelService.h"
#import "B3SModelVector3.h"
#import "B3SModelPoint.h"
#import "B3SModelTriangleFace.h"
#import "EWMatrix4.h"

/*
 ** obj 文件格式简单介绍
 
 #开头的是注释
 
 mtllib smoothCube.mtl 相当于导入了一个材质文件，本文将不做详细介绍，会在后面的文章再做介绍。
 
 o Cube 说明下面的数据都属于这个Cube Object。
 
 v 1.000000 -1.000000 -1.000000 表示顶点位置，正方体一共8个顶点，所以有8行这样的数据。
 
 vt 0.0000 -1.0000 表示顶点的UV。
 
 vn -0.5773 0.5773 0.5773  表示顶点的法线。
 
 usemtl Material  表示使用名为Material的材质，本文将不做介绍。
 
 s 1 表明开启平滑渲染。
 
 f 2/1/1 4/2/2 1/3/3 表示一个三角面， f 顶点索引/UV索引/法线索引 顶点索引/UV索引/法线索引 顶点索引/UV索引/法线索引，
                    我们可以根据各个索引去取实际的值。这里的索引是从1开始的，在代码中，要记得减去1才能使用。
 
 */

@interface B3SModelService() {

}


@end

@implementation B3SModelService

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.vertexArray = [NSMutableArray new];
        self.normalArray = [NSMutableArray new];
        self.uvArray = [NSMutableArray new];
        self.triangleFaceArray = [NSMutableArray new];
        self.commentArray = [NSMutableArray new];
    }
    return self;
}

#pragma mark - 导出模型用的量纲

//
static NSString *PrecisionUnit = @"CurrentModelServicePrecisionUnit";

/**
  设定模型导出量纲到用户配置项.
 */
- (void)setModelUnitForExporting:(B3SModelServiceModelUnit)modelPrecisionUnit
{
    [[NSUserDefaults standardUserDefaults] setObject:@(modelPrecisionUnit) forKey:PrecisionUnit];
}

/**
  从当前用户配置项中读取 模型导出量纲.
 */
- (B3SModelServiceModelUnit)modelUnitForExporting
{
    NSNumber *ret = [[NSUserDefaults standardUserDefaults] objectForKey:PrecisionUnit];
    if (ret) {
        return ret.unsignedIntegerValue;
    }
    return B3SModelServicePrecisionUnitMillimeter;
}

/**
  获取用户设定的导出量纲.
 */
- (float)getCurrentPrecisionUnitValue
{
    return [self getScaleValueOfUnit:self.modelUnitForExporting];
}

/**
  量纲枚举对应的顶点坐标缩放倍数.
 */
-(float) getScaleValueOfUnit:(B3SModelServiceModelUnit)unit
{
    switch (unit) {
        case B3SModelServicePrecisionUnitMeter:
            //米. 人脸扫描sdk输出的原始人脸模型的单位就是米,故导出单位为米时，缩放倍数是1
            return 1.f;
            break;
        case B3SModelServicePrecisionUnitDecimeter://分米
            return 10.f;
            break;
        case B3SModelServicePrecisionUnitCentimeter://厘米
            return 100.f;
            break;
        case B3SModelServicePrecisionUnitMillimeter://毫米
            return 1000.f;
            break;
            
        default:
            return 1.f;
            break;
    }
}

-(NSString *) getNameOfUnit:(B3SModelServiceModelUnit)unit
{
    switch (unit) {
        case B3SModelServicePrecisionUnitMeter://米
            return @"meter";
            break;
        case B3SModelServicePrecisionUnitDecimeter://分米
            return @"decimeter";
            break;
        case B3SModelServicePrecisionUnitCentimeter://厘米
            return @"centimeter";
            break;
        case B3SModelServicePrecisionUnitMillimeter://毫米
            return @"millimeter";
            break;
            
        default:
            return @"meter";
            break;
    }
}

//根据量纲缩写名字返回量纲枚举
-(B3SModelServiceModelUnit) determineUnitByName:(NSString*)name
{
    NSString *unitStr = [name lowercaseString];
    if([unitStr isEqualToString:@"meter"]){
        return B3SModelServicePrecisionUnitMeter;
    }else if([unitStr isEqualToString:@"decimeter"]){
        return B3SModelServicePrecisionUnitDecimeter;
    }else if([unitStr isEqualToString:@"centimeter"]){
        return B3SModelServicePrecisionUnitCentimeter;
    }else if([unitStr isEqualToString:@"millimeter"]){
        return B3SModelServicePrecisionUnitMillimeter;
    }
    return B3SModelServicePrecisionUnitUnknown;
}


#pragma mark - 获取模型信息

// 模型编号从1开始计数
- (B3SModelVector3 *)getVector3WithIndex:(NSInteger)index
{
    if (index < 1) {
        return nil;
    }
    
    B3SModelVector3 *v3 = [self.vertexArray objectAtIndex:(index-1)];
    return v3;
}


#pragma mark - 导出模型数据到Obj文件

/**
  将内存的模型数据(含顶点数组/面数组等), 序列化成obj格式的字符串. (使用当前用户设置项中的导出量纲)
@return 返回的字符串可以直接写入到.obj文件中.
 */
- (NSString *)exportObj
{
    return [self exportObjWithUnit:B3SModelServicePrecisionUnitUnknown];
}

/**
  将内存的模型数据(含顶点数组/面数组等), 序列化成obj格式的字符串.
   @param forceUnit 指定导出时量纲. 若为B3SModelServicePrecisionUnitUnknown, 则会使用用户设置项中设定的导出量纲.
 @return 返回的字符串可以直接写入到.obj文件中.
 */
- (NSString *)exportObjWithUnit:(B3SModelServiceModelUnit) forceUnit
{
    NSMutableString *objText = [NSMutableString string];
    //1.确定导出量纲
    B3SModelServiceModelUnit unit=forceUnit;
    if(unit==B3SModelServicePrecisionUnitUnknown){
        //若调用方没有强制指定量纲,则使用用户设置项中的导出量纲
        unit = self.modelUnitForExporting;
    }
    float scaleValue = [self getScaleValueOfUnit:unit];
    
    //2.删除注释中的量纲行
    NSMutableArray *unitLineArr = [[NSMutableArray alloc] init];
    for(NSString *ln in self.commentArray){
        if([[ln stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceCharacterSet] hasPrefix:@"#unit:"]){
            [unitLineArr addObject:ln];
        }
    }
    [self.commentArray removeObjectsInArray:unitLineArr];
    
    // 3.加上当前最新的导出量纲行, 并将所有注释写入obj
    [self.commentArray addObject:[@"#unit:" stringByAppendingString:[self getNameOfUnit:unit]] ];
    for (NSString *line in self.commentArray) {
        [objText appendString:[NSString stringWithFormat:@"%@\n",line]];
    }
    [objText appendString:@"\n"];
    
    
    SCNMatrix4 matrix4 = SCNMatrix4MakeRotation(270.0*M_PI/180.0, 1,0,0);
    //EWMatrix4 * ewMatrix4 = [[[EWMatrix4 alloc] init] makeWithSCNMatrix4:matrix4];
    
    for (int i=0; i<self.vertexArray.count; i++)
    {
        B3SModelVector3 *v3 = [self.normalArray objectAtIndex:i];
        [objText appendString:[NSString stringWithFormat:@"vn %f %f %f \n",v3.x,v3.y,v3.z]];
        
        B3SModelPoint *point = [self.uvArray objectAtIndex:i];
        [objText appendString:[NSString stringWithFormat:@"vt %f %f \n",point.x,point.y]];
        
        B3SModelVector3 *vertexV3 = [self.vertexArray objectAtIndex:i];
        
        //所有顶点围绕x轴心，逆时针旋转90度 (Impress3D需要,其他标准应用不需要)
        //B3SModelVector3 * newVertexV3 = [vertexV3 applyMatrix4:ewMatrix4];
        //[objText appendString:[NSString stringWithFormat:@"v %f %f %f \n",newVertexV3.x,newVertexV3.y,newVertexV3.z]];
        [objText appendString:[NSString stringWithFormat:@"v %f %f %f \n",
                               vertexV3.x*scaleValue,
                               vertexV3.y*scaleValue,
                               vertexV3.z*scaleValue]];
    }
    
    [objText appendString:@"\n"];
    
    for (int i=0; i<self.triangleFaceArray.count; i++)
    {
        B3SModelTriangleFace *tFace = [self.triangleFaceArray objectAtIndex:i];
        
        NSString *line = [NSString stringWithFormat:@"f %ld/%ld/%ld %ld/%ld/%ld %ld/%ld/%ld",
                          (long)tFace.vectorIndex1,(long)tFace.uvIndex1,(long)tFace.normalIndex1,
                          (long)tFace.vectorIndex2,(long)tFace.uvIndex2,(long)tFace.normalIndex2,
                          (long)tFace.vectorIndex3,(long)tFace.uvIndex3,(long)tFace.normalIndex3
                          ];
        [objText appendString:[NSString stringWithFormat:@"%@ \n",line]];
    }
    
    return  objText;
}



#pragma mark - 从OBJ文件中读取数据

/**
  判断obj文件的顶点坐标是什么量纲单位.
 默认是米.
 */
-(B3SModelServiceModelUnit) determineModelUnitOfObjContent:(NSArray<NSString *> *)lines
{
    //obj文件"开头几行"包含类似#unit:meter的注释, 可以有助于快速退出下面的for循环
    //若通篇都不包含,则会遍历整个文件
    for (NSString *line in lines) {
        NSString *ln = [line stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceCharacterSet];
        
        if (ln.length >= 2 && [ln hasPrefix:@"#unit:"]) {
            NSString* unitStr = [ln substringFromIndex:6].lowercaseString;
            if([unitStr isEqualToString:@"meter"]){
                return B3SModelServicePrecisionUnitMeter;
            }else if([unitStr isEqualToString:@"decimeter"]){
                return B3SModelServicePrecisionUnitDecimeter;
            }else if([unitStr isEqualToString:@"centimeter"]){
                return B3SModelServicePrecisionUnitCentimeter;
            }else if([unitStr isEqualToString:@"millimeter"]){
                return B3SModelServicePrecisionUnitMillimeter;
            }
        }
    }
    //没有#unit,默认是米
    return B3SModelServicePrecisionUnitMeter;
}

/**
  判断obj文件的顶点坐标是什么量纲单位.
 默认是米.
 */
-(B3SModelServiceModelUnit) determineModelUnitOfObjFile:(NSURL *)url
{
    NSString *fileContent = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    NSArray<NSString *> *lines = [fileContent componentsSeparatedByString:@"\n"];
    return [self determineModelUnitOfObjContent:lines];
}

/**
 从本地加载obj文件内容到内存中, 构造内存中的顶点数组/纹理坐标数据/面数组等.
 
 @param url 本地obj文件路径, file:// 格式的全路径
 @param knownUnit 若您已知该文件中的顶点坐标是什么量纲, 请指定该参数为对应的量纲枚举,有助于加速加载obj文件.
 */
- (void)loadDataFromObjUrl:(NSURL *)url
                  withUnit:(B3SModelServiceModelUnit) knownUnit
{
    NSString *fileContent = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    NSArray<NSString *> *lines = [fileContent componentsSeparatedByString:@"\n"];
    if(knownUnit==B3SModelServicePrecisionUnitUnknown){
        //若调用方没有明确指定该obj文件是什么量纲,就尝试从文件中解析出量纲
        knownUnit = [self determineModelUnitOfObjContent:lines];
    }
    
    
    for (NSString *ln in lines) {
        NSString *line = [ln stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceCharacterSet];
        if (line.length >= 2) {
            if ([line characterAtIndex:0] == 'v' && [line characterAtIndex:1] == ' ') {
                [self processVertexLine:line withModelUnit:knownUnit];
            } else if ([line characterAtIndex:0] == 'v' && [line characterAtIndex:1] == 'n') {
                [self processNormalLine:line];
            } else if ([line characterAtIndex:0] == 'v' && [line characterAtIndex:1] == 't') {
                [self processUVLine:line];
            } else if ([line characterAtIndex:0] == 'f' && [line characterAtIndex:1] == ' ') {
                [self processFaceIndexLine:line];
            } else if([line characterAtIndex:0] == '#'){
                [self.commentArray addObject:line];
                
            }
        }
    }
}

/**
 处理一行顶点坐标数据.
  对顶点坐标值按照用户量纲进行缩放, 再保存到vertexArray数组.
 顶点坐标数据在obj中是以v+空格打头的.
 */
- (void)processVertexLine:(NSString *)line
            withModelUnit:(B3SModelServiceModelUnit)unit
{
    static NSString *pattern = @"v\\s*([\\-0-9]*\\.[\\-0-9]*)\\s*([\\-0-9]*\\.[\\-0-9]*)\\s*([\\-0-9]*\\.[\\-0-9]*)";
    static NSRegularExpression *regexExp = nil;
    if (regexExp == nil) {
        regexExp = [[NSRegularExpression alloc] initWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:nil];
    }
    NSArray * matchResults = [regexExp matchesInString:line options:0 range:NSMakeRange(0, line.length)];
    
    float scaleValue = [self getScaleValueOfUnit:unit];
    
    for (NSTextCheckingResult *result in matchResults) {
        NSUInteger rangeCount = result.numberOfRanges;
        if (rangeCount == 4) {
            float x = [[line substringWithRange: [result rangeAtIndex:1]] floatValue];
            float y = [[line substringWithRange: [result rangeAtIndex:2]] floatValue];
            float z = [[line substringWithRange: [result rangeAtIndex:3]] floatValue];
            
            //当前文件量纲是unit, 读到内存顶点数组时, 要统一标准化为米.
            [self.vertexArray addObject:[B3SModelVector3 makeX:(x/scaleValue) y:(y/scaleValue) z:(z/scaleValue)]];
        }
    }
}

/**
  处理一行法向量坐标数据.
   法向量在obj文件中是以vn+空格打头的.
 */
- (void)processNormalLine:(NSString *)line {
    static NSString *pattern = @"vn\\s*([\\-0-9]*\\.[\\-0-9]*)\\s*([\\-0-9]*\\.[\\-0-9]*)\\s*([\\-0-9]*\\.[\\-0-9]*)";
    static NSRegularExpression *regexExp = nil;
    if (regexExp == nil) {
        regexExp = [[NSRegularExpression alloc] initWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:nil];
    }
    NSArray * matchResults = [regexExp matchesInString:line options:0 range:NSMakeRange(0, line.length)];
    for (NSTextCheckingResult *result in matchResults) {
        NSUInteger rangeCount = result.numberOfRanges;
        if (rangeCount == 4) {
            float x = [[line substringWithRange: [result rangeAtIndex:1]] floatValue];
            float y = [[line substringWithRange: [result rangeAtIndex:2]] floatValue];
            float z = [[line substringWithRange: [result rangeAtIndex:3]] floatValue];

            [self.normalArray addObject:[B3SModelVector3 makeX:x y:y z:z]];
        }
    }
}

/**
  处理一行顶点纹理坐标数据.
  顶点纹理坐标在obj文件中是以vt+空格打头的.
 */
- (void)processUVLine:(NSString *)line {
    static NSString *pattern = @"vt\\s*([\\-0-9]*\\.[\\-0-9]*)\\s*([\\-0-9]*\\.[\\-0-9]*)";
    static NSRegularExpression *regexExp = nil;
    if (regexExp == nil) {
        regexExp = [[NSRegularExpression alloc] initWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:nil];
    }
    NSArray * matchResults = [regexExp matchesInString:line options:0 range:NSMakeRange(0, line.length)];
    for (NSTextCheckingResult *result in matchResults) {
        NSUInteger rangeCount = result.numberOfRanges;
        if (rangeCount == 3) {
            float x = [[line substringWithRange: [result rangeAtIndex:1]] floatValue];
            float y = [[line substringWithRange: [result rangeAtIndex:2]] floatValue];
            [self.uvArray addObject:[B3SModelPoint makeX:x y:y]];
        }
    }
}

/**
  处理一行三角面索引数据.
   三角面索引在obj文件中是以f+空格打头的.
 */
- (void)processFaceIndexLine:(NSString *)line {
    static NSString *pattern = @"f\\s*([0-9]*)/([0-9]*)/([0-9]*)\\s*([0-9]*)/([0-9]*)/([0-9]*)\\s*([0-9]*)/([0-9]*)/([0-9]*)";
    static NSRegularExpression *regexExp = nil;
    if (regexExp == nil) {
        regexExp = [[NSRegularExpression alloc] initWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:nil];
    }
    NSArray * matchResults = [regexExp matchesInString:line options:0 range:NSMakeRange(0, line.length)];
    for (NSTextCheckingResult *result in matchResults) {
        NSUInteger rangeCount = result.numberOfRanges;
        if (rangeCount == 10) {
            B3SModelTriangleFace *tFace = [B3SModelTriangleFace new];
            
            // f 顶点/UV/法线 顶点/UV/法线 顶点/UV/法线
            NSInteger vertexIndex1 = [[line substringWithRange: [result rangeAtIndex:1]] integerValue];
            NSInteger uvIndex1 = [[line substringWithRange: [result rangeAtIndex:2]] integerValue];
            NSInteger normalIndex1 = [[line substringWithRange: [result rangeAtIndex:3]] integerValue];

            tFace.vectorIndex1 = vertexIndex1;
            tFace.uvIndex1 = uvIndex1;
            tFace.normalIndex1 = normalIndex1;
            
            NSInteger vertexIndex2 = [[line substringWithRange: [result rangeAtIndex:4]] integerValue];
            NSInteger uvIndex2 = [[line substringWithRange: [result rangeAtIndex:5]] integerValue];
            NSInteger normalIndex2 = [[line substringWithRange: [result rangeAtIndex:6]] integerValue];
            
            tFace.vectorIndex2 = vertexIndex2;
            tFace.uvIndex2 = uvIndex2;
            tFace.normalIndex2 = normalIndex2;
            
            NSInteger vertexIndex3 = [[line substringWithRange: [result rangeAtIndex:7]] integerValue];
            NSInteger uvIndex3 = [[line substringWithRange: [result rangeAtIndex:8]] integerValue];
            NSInteger normalIndex3 = [[line substringWithRange: [result rangeAtIndex:9]] integerValue];
            
            tFace.vectorIndex3 = vertexIndex3;
            tFace.uvIndex3 = uvIndex3;
            tFace.normalIndex3 = normalIndex3;
            
            [self.triangleFaceArray addObject:tFace];
        }
    }
}
@end

