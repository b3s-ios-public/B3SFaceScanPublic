//
//  EWVector3.h
//  eyewear-iOS
//
//  Created on 2019/1/22.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SceneKit/SceneKit.h>

@class EWMatrix4;
@class EWQuaternion;

NS_ASSUME_NONNULL_BEGIN

@interface EWVector3 : NSObject

@property (assign, nonatomic) double x;
@property (assign, nonatomic) double y;
@property (assign, nonatomic) double z;

+ (instancetype)makeX:(double)x y:(double)y z:(double)z;

+ (instancetype)makeWithSCNVector3:(SCNVector3)vec;

- (SCNVector3)toSCNVector3;

- (instancetype)set:(double)x
                  y:(double)y
                  z:(double)z;

- (instancetype)negate;

- (double)length;

- (instancetype)clone;

- (double)distanceTo:(EWVector3 *)v;

- (instancetype)applyMatrix4:(EWMatrix4 *)m;

- (instancetype)applyQuaternion:(EWQuaternion *)q;

- (instancetype)sub:(EWVector3 *)v w:(nullable EWVector3 *)w;

- (instancetype)normalize;

- (instancetype)addScaledVector:(EWVector3 *)v s:(double)s;

- (instancetype)applyAxisAngle:(EWVector3 *)axis angel:(double)angle;

- (instancetype)add:(EWVector3 *)v w:(nullable EWVector3 *)w;

- (double)dot:(EWVector3 *)v;

- (instancetype)copy:(EWVector3 *)v;

- (instancetype)crossVectors:(EWVector3 *)a b:(EWVector3 *)b;

- (NSArray *)toArray;

- (instancetype)subVectors:(EWVector3 *)a
                         b:(EWVector3 *)b;

- (instancetype)lerpVectors:(EWVector3 *)v1 v2:(EWVector3 *)v2 alpha:(double)alpha;

- (instancetype)cross:(EWVector3 *)v;

@end

NS_ASSUME_NONNULL_END
