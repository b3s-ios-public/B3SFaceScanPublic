//
//  EWVector3.h
//  eyewear-iOS
//
//  Created on 2019/1/22.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SceneKit/SceneKit.h>

@class EWMatrix4;
@interface B3SModelVector3 : NSObject

@property (assign, nonatomic) float x;
@property (assign, nonatomic) float y;
@property (assign, nonatomic) float z;

+ (instancetype)makeX:(float)x y:(float)y z:(float)z;

- (SCNVector3)toSCNVector3;

- (instancetype)applyMatrix4:(EWMatrix4 *) m;

@end
