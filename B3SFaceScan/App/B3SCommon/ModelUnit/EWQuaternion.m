//
//  EWQuaternion.m
//  eyewear-iOS
//
//  Created on 2019/2/27.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "EWQuaternion.h"
#import "EWMatrix4.h"

@implementation EWQuaternion

+ (instancetype)makeX:(double)x
                    y:(double)y
                    z:(double)z
                    w:(double)w
{
    EWQuaternion *v4 = [EWQuaternion new];
    v4.x = x;
    v4.y = y;
    v4.z = z;
    v4.w = w;
    return v4;
}

- (instancetype)setFromRotationMatrix:(EWMatrix4 *)m
{
    
    // http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/index.htm
    
    // assumes the upper 3x3 of m is a pure rotation matrix (i.e, unscaled)
    
    
    double m11 = m.m11, m12 = m.m12, m13 = m.m13,
    m21 = m.m21, m22 = m.m22, m23 = m.m23,
    m31 = m.m31, m32 = m.m32, m33 = m.m33,
    trace = m11 + m22 + m33,
    s;
    
    if ( trace > 0 ) {
        
        s = 0.5 / sqrt( trace + 1.0 );
        
        self.w = 0.25 / s;
        self.x = ( m32 - m23 ) * s;
        self.y = ( m13 - m31 ) * s;
        self.z = ( m21 - m12 ) * s;
        
    } else if ( m11 > m22 && m11 > m33 ) {
        
        s = 2.0 * sqrt( 1.0 + m11 - m22 - m33 );
        
        self.w = ( m32 - m23 ) / s;
        self.x = 0.25 * s;
        self.y = ( m12 + m21 ) / s;
        self.z = ( m13 + m31 ) / s;
        
    } else if ( m22 > m33 ) {
        
        s = 2.0 * sqrt( 1.0 + m22 - m11 - m33 );
        
        self.w = ( m13 - m31 ) / s;
        self.x = ( m12 + m21 ) / s;
        self.y = 0.25 * s;
        self.z = ( m23 + m32 ) / s;
        
    } else {
        
        s = 2.0 * sqrt( 1.0 + m33 - m11 - m22 );
        
        self.w = ( m21 - m12 ) / s;
        self.x = ( m13 + m31 ) / s;
        self.y = ( m23 + m32 ) / s;
        self.z = 0.25 * s;
        
    }
    
    return self;
    
}

- (instancetype)setFromAxisAngle:(EWVector3 *)axis
                           angle:(double)angle
{
    // http://www.euclideanspace.com/maths/geometry/rotations/conversions/angleToQuaternion/index.htm
    
    // assumes axis is normalized
    
    double halfAngle = angle / 2, s = sin( halfAngle );
    
    self.x = axis.x * s;
    self.y = axis.y * s;
    self.z = axis.z * s;
    self.w = cos( halfAngle );
    
    return self;
    
}

- (instancetype)setFromUnitVectors:(EWVector3 *)vFrom vTo:(EWVector3 *)vTo
{
    
    EWVector3 *v1 = [EWVector3 new];
    double r;
    
    double EPS = 0.000001;
    
    
    if ( !v1 ) v1 = [EWVector3 new];
        
    r = [vFrom dot:vTo] + 1;
        
    if ( r < EPS ) {
        
        r = 0;
        
        if ( fabs( vFrom.x ) > fabs( vFrom.z) ) {
            
            [v1 set:-vFrom.y y:vFrom.x z:0];
            
        } else {
            [v1 set:0 y:- vFrom.z z:vFrom.y];
        }
    } else {
        [v1 crossVectors:vFrom b:vTo];
    }
    
    self.x = v1.x;
    self.y = v1.y;
    self.z = v1.z;
    self.w = r;
    
    return [self normalize];
}

- (instancetype)normalize
{
    double l = [self length];
    
    if ( l == 0 ) {
        
        self.x = 0;
        self.y = 0;
        self.z = 0;
        self.w = 1.f;
        
    } else {
        
        l = 1.f / l;
        
        self.x = self.x * l;
        self.y = self.y * l;
        self.z = self.z * l;
        self.w = self.w * l;
        
    }
    
    return self;
    
}

- (double)length
{
    return sqrt( self.x * self.x + self.y * self.y + self.z * self.z + self.w * self.w );

}

- (NSArray *)toArray
{
    return @[@(self.x),@(self.y),@(self.z),@(self.w)];
}

- (SCNVector4)toSCNVector4
{
    return SCNVector4Make(self.x, self.y, self.z,self.w);
}

@end
