//
//  EWVector3.m
//  eyewear-iOS
//
//  Created on 2019/1/22.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SModelVector3.h"
#import "EWMatrix4.h"

@implementation B3SModelVector3

+ (instancetype)makeX:(float)x y:(float)y z:(float)z
{
    B3SModelVector3 *vector = [B3SModelVector3 new];
    vector.x = x;
    vector.y = y;
    vector.z = z;
    return vector;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%f %f %f",self.x,self.y,self.z];
}

- (SCNVector3)toSCNVector3
{
    return SCNVector3Make(self.x, self.y, self.z);
}

- (instancetype)applyMatrix4:(EWMatrix4 *) m
{
    
    double x = self.x, y = self.y, z = self.z;
    
    double w = 1.0f / ( m.m41 * x + m.m42 * y + m.m43 * z + m.m44 );
    
    self.x = ( m.m11 * x + m.m12 * y + m.m13 * z + m.m14 ) * w;
    self.y = ( m.m21 * x + m.m22 * y + m.m23 * z + m.m24 ) * w;
    self.z = ( m.m31 * x + m.m32 * y + m.m33 * z + m.m34 ) * w;
    
    return self;
    
}

@end
