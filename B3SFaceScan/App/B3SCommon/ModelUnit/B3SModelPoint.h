//
//  EWPoint.h
//  eyewear-iOS
//
//  Created on 2019/1/22.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface B3SModelPoint : NSObject

@property (assign, nonatomic) float x;
@property (assign, nonatomic) float y;

+ (instancetype)makeX:(float)x y:(float)y;

- (CGPoint)toCGPoint;

@end

NS_ASSUME_NONNULL_END
