//
//  EWTriangleFace.h
//  eyewear-iOS
//
//  Created on 2019/1/22.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface B3SModelTriangleFace : NSObject

@property (nonatomic, assign) NSInteger vectorIndex1;
@property (nonatomic, assign) NSInteger uvIndex1;
@property (nonatomic, assign) NSInteger normalIndex1;

@property (nonatomic, assign) NSInteger vectorIndex2;
@property (nonatomic, assign) NSInteger uvIndex2;
@property (nonatomic, assign) NSInteger normalIndex2;

@property (nonatomic, assign) NSInteger vectorIndex3;
@property (nonatomic, assign) NSInteger uvIndex3;
@property (nonatomic, assign) NSInteger normalIndex3;

@end

NS_ASSUME_NONNULL_END
