//
//  EWVector3.m
//  eyewear-iOS
//
//  Created on 2019/1/22.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "EWVector3.h"
#import "EWMatrix4.h"
#import "EWQuaternion.h"

@implementation EWVector3

+ (instancetype)makeX:(double)x y:(double)y z:(double)z
{
    EWVector3 *vector = [EWVector3 new];
    vector.x = x;
    vector.y = y;
    vector.z = z;
    return vector;
}

+ (instancetype)makeWithSCNVector3:(SCNVector3)vec
{
    EWVector3 *vector = [EWVector3 new];
    vector.x = vec.x;
    vector.y = vec.y;
    vector.z = vec.z;
    
    return vector;
}

- (SCNVector3)toSCNVector3
{
    return SCNVector3Make((float)self.x, (float)self.y, (float)self.z);
}

- (instancetype)set:(double)x
                  y:(double)y
                  z:(double)z
{
    self.x = x;
    self.y = y;
    self.z = z;
    
    return self;
}

- (instancetype)negate
{
    self.x = -self.x;
    self.y = -self.y;
    self.z = -self.z;
    
    return self;
}

- (double)length
{
    return sqrt( self.x * self.x + self.y * self.y + self.z * self.z );
}

- (double)distanceTo:(EWVector3 *)to
{
    return sqrt([self distanceToSquared:to]);
}

- (double)distanceToSquared:(EWVector3 *)v
{
    double dx = self.x - v.x, dy = self.y - v.y, dz = self.z - v.z;
    
    return dx * dx + dy * dy + dz * dz;
}

- (instancetype)applyMatrix4:(EWMatrix4 *) m
{
    
    double x = self.x, y = self.y, z = self.z;
    
    double w = 1.0f / ( m.m41 * x + m.m42 * y + m.m43 * z + m.m44 );
    
    self.x = ( m.m11 * x + m.m12 * y + m.m13 * z + m.m14 ) * w;
    self.y = ( m.m21 * x + m.m22 * y + m.m23 * z + m.m24 ) * w;
    self.z = ( m.m31 * x + m.m32 * y + m.m33 * z + m.m34 ) * w;
    
    return self;
    
}

- (instancetype)applyQuaternion:(EWQuaternion *)q
{
    double x = self.x, y = self.y, z = self.z;
    double qx = q.x, qy = q.y, qz = q.z, qw = q.w;
    
    // calculate quat * vector
    
    double ix = qw * x + qy * z - qz * y;
    double iy = qw * y + qz * x - qx * z;
    double iz = qw * z + qx * y - qy * x;
    double iw = - qx * x - qy * y - qz * z;
    
    // calculate result * inverse quat
    
    self.x = ix * qw + iw * - qx + iy * - qz - iz * - qy;
    self.y = iy * qw + iw * - qy + iz * - qx - ix * - qz;
    self.z = iz * qw + iw * - qz + ix * - qy - iy * - qx;
    
    return self;
    
}

- (instancetype)clone
{
    return [EWVector3 makeX:self.x y:self.y z:self.z];
}

- (instancetype)sub:(EWVector3 *)v w:(EWVector3 *)w
{
    if (w) {
        return [self subVectors:v b:w];
    }
    
    self.x -= v.x;
    self.y -= v.y;
    self.z -= v.z;
    
    return self;
    
}

- (instancetype)subVectors:(EWVector3 *)a
                         b:(EWVector3 *)b
{
    self.x = a.x - b.x;
    self.y = a.y - b.y;
    self.z = a.z - b.z;
    
    return self;
}

- (instancetype)normalize
{
    return [self divideScalar:[self length]];
    
}

- (instancetype)divideScalar:(double)scalar
{
    return [self multiplyScalar:1.f/scalar];
    
}

- (instancetype)multiplyScalar:(double)scalar
{
    self.x *= scalar;
    self.y *= scalar;
    self.z *= scalar;
    return self;
}

- (instancetype)addScaledVector:(EWVector3 *)v s:(double)s
{
    self.x += v.x * s;
    self.y += v.y * s;
    self.z += v.z * s;
    return self;
}

- (instancetype)applyAxisAngle:(EWVector3 *)axis angel:(double)angle
{
    EWQuaternion *q = [EWQuaternion new];
    [q setFromAxisAngle:axis angle:angle];

    return [self applyQuaternion:q];

}

- (instancetype)add:(EWVector3 *)v w:(nullable EWVector3 *) w
{
    if (w) {
        return [self addVectors:v b:w];
    }
    
    self.x += v.x;
    self.y += v.y;
    self.z += v.z;
    
    return self;
}

- (instancetype)addVectors:(EWVector3 *)a b:(EWVector3 *)b
{
    self.x = a.x + b.x;
    self.y = a.y + b.y;
    self.z = a.z + b.z;
    
    return self;
}

- (double)dot:(EWVector3 *)v
{
    return self.x * v.x + self.y * v.y + self.z * v.z;
    
}

- (instancetype)copy:(EWVector3 *)v
{
    self.x = v.x;
    self.y = v.y;
    self.z = v.z;
    return self;
}

- (instancetype)cross:(EWVector3 *)v
{
    return [self crossVectors:self  b:v];
}

- (instancetype)cross:(EWVector3 *)v w:(EWVector3 *)w
{
    return [self crossVectors:v  b:w];
}

- (instancetype)crossVectors:(EWVector3 *)a b:(EWVector3 *)b
{
    double ax = a.x, ay = a.y, az = a.z;
    double bx = b.x, by = b.y, bz = b.z;
    
    self.x = ay * bz - az * by;
    self.y = az * bx - ax * bz;
    self.z = ax * by - ay * bx;
    
    return self;
}

- (NSArray *)toArray
{
    return @[@(self.x),@(self.y),@(self.z)];
}

- (instancetype)lerpVectors:(EWVector3 *)v1 v2:(EWVector3 *)v2 alpha:(double)alpha
{
    return [[[self subVectors:v2 b:v1] multiplyScalar:alpha] add:v1 w:nil];
    
}

-(id)copy
{
    return [self clone];
}
@end
