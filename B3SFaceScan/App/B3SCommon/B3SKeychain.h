//
//  B3SKeychain.h
//  B3SCommon
//
//  Created on 2019/5/7.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#ifndef B3SKeychain_h
#define B3SKeychain_h

#import <Foundation/Foundation.h>
#import <Security/Security.h>

@interface B3SKeychain : NSObject

+ (void)save:(NSString *)service account:(NSString *)acc data:(NSData *)data;
+ (void)save:(NSString *)service data:(NSData *)data;

+ (NSData *)load:(NSString *)service;
+ (NSData *)load:(NSString *)service account:(NSString *)acc;

+ (void)delete:(NSString *)service;
+ (void)delete:(NSString *)service account:(NSString *)acc;
@end

#endif /* B3SKeychain_h */
