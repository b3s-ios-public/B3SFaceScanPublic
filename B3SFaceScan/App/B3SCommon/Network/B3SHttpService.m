//
//  B3SHttpService.m
//  zm-mall
//
//  Created on 21/12/2017.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SHttpService.h"
#import <AFNetworking.h>

@interface B3SHttpService ()

@property (nonatomic, copy) NSURL *url;

@end

@implementation B3SHttpService

+ (instancetype)sharedInstance
{
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        AFHTTPSessionManager *manager =[[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:defaultURL]];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        //manager.securityPolicy.allowInvalidCertificates = YES; // 是否允许使用自建证书
        //manager.securityPolicy.validatesDomainName = NO; // 是否验证域名（一般不验证）
        self.manager = manager;
    }
    return self;
}

- (void)switchBaseURL:(NSURL *)url
{
    AFHTTPSessionManager *manager =[[AFHTTPSessionManager alloc] initWithBaseURL:url];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //manager.securityPolicy.allowInvalidCertificates = YES; // 是否允许使用自建证书
    //manager.securityPolicy.validatesDomainName = NO; // 是否验证域名（一般不验证）
    self.manager = manager;
}

@end
