//
//  B3SHttpService.h
//  zm-mall
//
//  Created on 21/12/2017.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <AFNetworking/AFNetworking.h>
@class AFHTTPSessionManager;

typedef void(^CompleteBlock)(id obj);

typedef void(^ProgressBlock)(NSProgress *progress);

#define ERROR_NETWORK_CODE @"-9999"
#define ERROR_NETWORK_MSG @"网络请求失败"

// 线上配置
#define defaultModelURL @"https://scanapi.body3dscale.com"
#define defaultURL @"https://api.body3dscale.com"

#define OSS_CallBack_URL @"https://scanapi.body3dscale.com"

// 内网不带域名配置
//#define defaultModelURL @"http://192.168.31.147:9031"
//#define defaultURL @"http://192.168.31.147:8091"

// 内网带域名配置
//#define defaultModelURL @"http://zm-server1.office.zm:9031"
//#define defaultURL @"http://zm-server2.office.zm:8091"

@interface B3SHttpService : NSObject

+ (instancetype)sharedInstance;

- (void)switchBaseURL:(NSURL *)url;

@property (nonatomic, strong) AFHTTPSessionManager *manager;

@property (nonatomic, copy) void(^commonParametersBlock)(NSMutableDictionary *info);

@property (nonatomic, copy) void(^successCommonBlock)(NSURLSessionDataTask *task, NSDictionary *info,NSDictionary *parameters);

@end
