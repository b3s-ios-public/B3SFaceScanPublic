//
//  B3SServiceEnvelope.h
//  zm-mall
//
//  Created on 16/12/2017.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <Foundation/Foundation.h>
#import "B3SJSONModel.h"
//@class B3SJSONModel;

@interface B3SServiceEnvelope : B3SJSONModel

@property (nonatomic, copy) NSString *code;
@property (nonatomic, copy) NSString *message;

@property (nonatomic, copy) NSDictionary *responseObject;

@end

