//
//  B3SLocalizedHelper.m
//  B3SCommon
//
//  Created on 2019/2/27.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SLocalizedHelper.h"

#import <Foundation/Foundation.h>

@implementation B3SLocalizedHelper

+ (NSString *)getCurrentLang
{
    NSString *currentLang = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    return currentLang;
}

+ (bool)isChinese
{
    NSString *currentLang = [self getCurrentLang];
    if([currentLang rangeOfString:@"zh-Hans"].location != NSNotFound) {
        return true;
    }
    return false;
}

@end
