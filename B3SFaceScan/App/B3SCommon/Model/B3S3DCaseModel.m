//
//  B3S3DCaseModel.m
//  body3dscale
//
//  Created on 26/02/2018.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3S3DCaseModel.h"
#import "B3SJSONModel.h"

@implementation B3S3DCaseModel

- (BOOL)isValid
{
    if ([self.status isEqualToString:@"2"]) {
        return YES;
    }

    return NO;
}

- (instancetype)initWithDictionary:(NSDictionary *)responseObject error:(NSError **)err
{
    if (self = [super init]) {
        if (responseObject) {
            [self setValuesForKeysWithDictionary:responseObject];
            NSString *modelId = [[responseObject objectForKey:@"id"]stringValue];
            self.modelId = modelId;
        }
    }
    return self;
}

@end

