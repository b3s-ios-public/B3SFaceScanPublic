//
//  B3S3DCaseModel.h
//  body3dscale
//
//  Created on 26/02/2018.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <Foundation/Foundation.h>
#import "B3SJSONModel.h"
//@class B3SJSONModel;
//@protocol B3S3DCaseModel;

@interface B3S3DCaseModel : B3SJSONModel

@property (nonatomic, copy) NSString *modelId; // 模型id
@property (nonatomic, copy) NSString *modelObjUrl; // 3D模型OBJ地址
@property (nonatomic, copy) NSString *modelMtlUrl; // 3D模型Mtl地址
@property (nonatomic, copy) NSString *modelImageUrl; // 3D模型纹理地址
@property (nonatomic, copy) NSString *modelCorpImageUrl; // 3D模型默认的截图
@property (nonatomic, copy) NSString *modelLandmarkUrl; //3D模型特征点文件
@property (nonatomic, copy) NSString *status; //3D模型特征点文件的上传状态 文件状态 1:上传中 2:上传成功 3:上传失败
@property (nonatomic, copy) NSString *creatTime;
@property (nonatomic, copy) NSString *type; // 1:扫描模型 2:变形模型 3:牙齿模型

@property (nonatomic, copy) NSString *refModelId; //变形原模型
@property (nonatomic, copy) NSString *region; // 区域列表

@property (nonatomic, copy) NSString * modelName;
@property (nonatomic, strong) UIImage * modelFrontImage; //封面图
@property (nonatomic, strong) NSString * localModelPath; //本地模型path

- (BOOL)isValid;

@end
