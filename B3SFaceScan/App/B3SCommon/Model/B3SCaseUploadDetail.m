//
//  B3SCaseUploadDetail.m
//  body3dscale
//
//  Created on 2018/4/18.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SCaseUploadDetail.h"

@implementation B3SCaseUploadDetail

- (instancetype)initWithDictionary:(NSDictionary *)responseObject error:(NSError **)err
{
    if (self = [super init]) {
        self = [super initWithDictionary:responseObject error:err];
        if (responseObject)
            self.responseObject = responseObject;
    }
    return self;
}

@end
