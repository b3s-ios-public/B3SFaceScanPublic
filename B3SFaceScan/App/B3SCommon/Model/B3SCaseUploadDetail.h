//
//  B3SCaseUploadDetail.h
//  body3dscale
//
//  Created on 2018/4/18.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <Foundation/Foundation.h>
#import "B3SJSONModel.h"
//@class B3SJSONModel;

@interface B3SCaseUploadDetail : B3SJSONModel

@property (nonatomic, copy) NSString *modelid;
@property (nonatomic, copy) NSString *uploadPath;
@property (nonatomic, copy) NSString *accessKeyId;
@property (nonatomic, copy) NSString *accessKeySecret;
@property (nonatomic, copy) NSString *securityToken;
@property (nonatomic, copy) NSString *expiration;
@property (nonatomic, copy) NSString *bucketName;
@property (nonatomic, copy) NSString *endPoint;
@property (nonatomic, copy) NSString *zipcode;

@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *day;

@property (nonatomic, copy) NSDictionary *responseObject;

// 通过JSON串初始化
- (instancetype)initWithDictionary:(NSDictionary *)responseObject error:(NSError **)err;

@end
