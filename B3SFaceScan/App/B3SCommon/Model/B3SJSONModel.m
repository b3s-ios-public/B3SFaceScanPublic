//
//  B3S3DCaseModel.m
//  body3dscale
//
//  Created on 26/02/2018.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SJSONModel.h"

@implementation B3SJSONModel

- (instancetype)initWithDictionary:(NSDictionary *)responseObject error:(NSError **)err
{
    if (self = [super init]) {
        if (responseObject)
            [self setValuesForKeysWithDictionary:responseObject];
    }
    return self;
}

- (void)setNilValueForKey:(NSString *)key {}

- (void)setValue:(id)value forUndefinedKey:(nonnull NSString *)key {}

- (void)setValue:(id)value forKey:(nonnull NSString *)key {
    if (value == nil) {
        return;
    }
    if ([value isKindOfClass:[NSNumber class]]) {
        [super setValue:[(NSNumber *)value stringValue] forKey:key];
    } else{
        [super setValue:value forKey:key];
    }
}
@end

