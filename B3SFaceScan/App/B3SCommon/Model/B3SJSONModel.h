//
//  B3SJSONModel.h
//  B3SCommon
//
//  Created on 2019/8/6.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <Foundation/Foundation.h>

@interface B3SJSONModel : NSObject

// 通过JSON串初始化
- (instancetype)initWithDictionary:(NSDictionary *)responseObject error:(NSError **)err;

@end
