//
//  B3SSceneHelper.h
//  B3SCommon
//
//  Created on 08/03/2018.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SceneKit/SceneKit.h>

@interface B3SSceneHelper : NSObject

+ (SCNScene *)getSceneWithHeadByNode:(SCNNode *)node modelDirUrl:(NSURL *)url;

+ (SCNVector3)getNewVector3WithTransform:(SCNMatrix4)transform oldVector3:(SCNVector3)vec;

@end
