//
//  B3SLocalizedHelper.h
//  B3SCommon
//
//  Created on 2019/2/27.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <Foundation/Foundation.h>

@interface B3SLocalizedHelper : NSObject

+ (NSString *)getCurrentLang;

+ (bool)isChinese;

@end
