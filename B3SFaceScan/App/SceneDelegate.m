//
//  SceneDelegate.m
//  B3SFaceScan
//
//  Created on 2020/7/24.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "SceneDelegate.h"
#import "AppDelegate.h"

API_AVAILABLE(ios(13.0))
@interface SceneDelegate ()
@property(nonatomic, strong) UIWindowScene *windowScene;

@end

@implementation SceneDelegate


- (void)scene:(UIScene *)scene willConnectToSession: (UISceneSession *)session options:(UISceneConnectionOptions *)connectionOptions  API_AVAILABLE(ios(13.0)){
    UIWindowScene *windowScene = (UIWindowScene *)scene;
     UIWindow *window = [[UIWindow alloc] initWithWindowScene:windowScene];
     window.frame = windowScene.coordinateSpace.bounds;
     self.windowScene = windowScene;
     
    [AppDelegate sharedInstance].window = window; // 共用window
    //[[AppDelegate sharedInstance] handlerMainLogic];
}


- (void)sceneDidDisconnect:(UIScene *)scene  API_AVAILABLE(ios(13.0)){
    // Called as the scene is being released by the system.
    // This occurs shortly after the scene enters the background, or when its session is discarded.
    // Release any resources associated with this scene that can be re-created the next time the scene connects.
    // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
}


- (void)sceneDidBecomeActive:(UIScene *)scene  API_AVAILABLE(ios(13.0)){
    // Called when the scene has moved from an inactive state to an active state.
    // Use this method to restart any tasks that were paused (or not yet started) w API_AVAILABLE(ios(13.0))hen the scene was inactive.
}


- (void)sceneWillResignActive:(UIScene *)scene  API_AVAILABLE(ios(13.0)){
    // Called when the scene will move from an active state to an inactive state.
    // This may occur due to temporary interruptions (ex.  API_AVAILABLE(ios(13.0))an incoming phone call).
}


- (void)sceneWillEnterForeground:(UIScene *)scene {
    // Called as the scene transitions from the background to the foreground.
    // Use this method to undo the changes made on entering the background.
}


- (void)sceneDidEnterBackground:(UIScene *)scene  API_AVAILABLE(ios(13.0)){
    // Called as the scene transitions from the foreground to the background.
    // Use this method to save data, release shared resources, and store enough scene-specific state information
    // to restore the scene back to its current state.
}


@end
