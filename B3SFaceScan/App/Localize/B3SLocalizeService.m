//
//  B3SLocalizeService.m
//  body3dscale
//
//  Created on 2018/8/11.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SLocalizeService.h"
#import <Aspects.h>
#import "B3SHttpService+Localize.h"
#import "B3STranslate.h"
#import <ReactiveCocoa.h>

@interface B3SLocalizeService ()

@property (nonatomic, strong) NSMutableDictionary *localDict;
@property (nonatomic, strong) NSMutableDictionary *reverseLookupDict;

@end

// Localizing UI text and voice
@implementation B3SLocalizeService

+ (instancetype)sharedInstance
{
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        @weakify(self);
        self.localDict = [NSMutableDictionary dictionary];
        self.reverseLookupDict = [NSMutableDictionary dictionary];
        
        NSArray *langArr = [[NSUserDefaults standardUserDefaults] valueForKey:@"AppleLanguages"];
        NSString *language = langArr.firstObject;

        //all ui texts default to english version.
        //if ios locale is chinese, we hook UILabel/UIButton/AttributedString to make it show chinese version 
        if (![[language componentsSeparatedByString:@"-"].firstObject isEqualToString:@"zh"]) {
            NSError *error = nil;
            
            [UILabel aspect_hookSelector:@selector(awakeFromNib) withOptions:AspectPositionAfter usingBlock:^(id<AspectInfo> info) {
                UILabel *label = info.instance;
                label.text = label.text;
                
            } error:&error];
                
            [UILabel aspect_hookSelector:@selector(setText:) withOptions:AspectPositionInstead usingBlock:^(id<AspectInfo> info,NSString *text) {
                UILabel *label = info.instance;
                NSString *transText = NSLocalizedString(text, nil);

                if (transText && transText.length > 0) {
                     [info.originalInvocation setArgument:&transText atIndex:2];
                } else {
                     [info.originalInvocation setArgument:&text atIndex:2];
                }
               
                [info.originalInvocation invoke];
                
                label.minimumScaleFactor = 0.5;
                label.adjustsFontSizeToFitWidth = YES;
                
                if (label.tintColor != [UIApplication sharedApplication].keyWindow.rootViewController.view.tintColor) {
                    label.textColor = label.tintColor;
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [label.superview layoutIfNeeded];
                });
            } error:&error];
            
            [NSClassFromString(@"NSConcreteMutableAttributedString") aspect_hookSelector:@selector(initWithString:)
                                                                             withOptions:AspectPositionInstead
                                                                              usingBlock:^(id<AspectInfo> info,NSString *text) {
                NSString *transText = NSLocalizedString(text, nil);
                [info.originalInvocation setArgument:&transText atIndex:2];
                [info.originalInvocation invoke];
            } error:&error];
            
            [NSClassFromString(@"NSConcreteMutableAttributedString") aspect_hookSelector:@selector(initWithString:attributes:)
                                                                             withOptions:AspectPositionInstead
                                                                              usingBlock:^(id<AspectInfo> info,NSString *text, NSDictionary<NSAttributedStringKey, id> *dict) {
                NSString *transText = NSLocalizedString(text, nil);
                [info.originalInvocation setArgument:&transText atIndex:2];
                [info.originalInvocation invoke];
            } error:&error];
            
            [UIButton aspect_hookSelector:@selector(setTitle:forState:)
                              withOptions:AspectPositionInstead
                               usingBlock:^(id<AspectInfo> info,NSString *text, UIControlState state) {
                              NSString *transText = NSLocalizedString(text, nil);
                              [info.originalInvocation setArgument:&transText atIndex:2];
                              [info.originalInvocation invoke];
                          } error:&error];
            
            NSAssert(!error, @"aspect error");
        }
    }
    return self;
}

- (NSString *)getTransResultFrom:(NSString *)from to:(NSString *)to text:(NSString *)text
{
    NSString *key = [NSString stringWithFormat:@"%@_%@_%@",from,to,text];
    return [self.localDict objectForKey:key];
}

- (void)setTransResultFrom:(NSString *)from to:(NSString *)to fromText:(NSString *)text toText:(NSString *)toText
{
    NSString *key = [NSString stringWithFormat:@"%@_%@_%@",from,to,text];
    [self.localDict setObject:toText forKey:key];
}

- (NSString *)getReverseLookupWithKey:(NSString *)key
{
    if (!key) {
        return nil;
    }
    NSString *value = [self.reverseLookupDict objectForKey:key];
    return value ? : key;
}

- (NSString *)lookupWithKey:(NSString *)key
{
    NSArray *langArr = [[NSUserDefaults standardUserDefaults] valueForKey:@"AppleLanguages"];
    NSString *language = langArr.firstObject;
    
    if (![[language componentsSeparatedByString:@"-"].firstObject isEqualToString:@"zh"]) {
        
        return NSLocalizedString(key, nil);
    }
    
    return key;
}

#pragma mark - Utils

- (BOOL)checkIsChinese:(NSString *)string{
    for (int i=0; i<string.length; i++) {
        unichar ch = [string characterAtIndex:i];
        if (0x4E00 <= ch  && ch <= 0x9FA5) {
            return YES;
        }
    }
    return NO;
}


@end
