//
//  B3SHttpService+Localize.h
//  body3dscale
//
//  Created on 2018/8/13.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SHttpService.h"

@interface B3SHttpService (Localize)

- (void)translateText:(NSString *)text from:(NSString *)from to:(NSString *)to complete:(CompleteBlock)complete;

@end
