//
//  B3STranslate.h
//  body3dscale
//
//  Created on 2018/8/13.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "JSONModel.h"

@protocol B3STranslateResult;

@interface B3STranslate : JSONModel

@property (nonatomic, copy) NSString *from;
@property (nonatomic, copy) NSString *to;
@property (nonatomic, strong) NSArray<B3STranslateResult> *trans_result;

@end

@interface B3STranslateResult : JSONModel

@property (nonatomic, copy) NSString *src;
@property (nonatomic, copy) NSString *dst;

@end
