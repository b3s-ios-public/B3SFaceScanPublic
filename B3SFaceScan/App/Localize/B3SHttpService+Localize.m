//
//  B3SHttpService+Localize.m
//  body3dscale
//
//  Created on 2018/8/13.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SHttpService+Localize.h"
#import <YYKit.h>
#import <AFNetworking.h>
#import "B3STranslate.h"

@implementation B3SHttpService (Localize)

- (void)translateText:(NSString *)text from:(NSString *)from to:(NSString *)to complete:(CompleteBlock)complete
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithDictionary:@{@"from":from,
                                                                                      @"to":to,
                                                                                      @"q":text,
                                                                                      @"appid":@"20180813000193778",
                                                                                      @"salt":@(random()),
                                                                                      }];
    NSString *key = @"H_wjDR9qX2rPz50MOmNN";
    NSString *str = [NSString stringWithFormat:@"%@%@%@%@",
                     [parameters objectForKey:@"appid"],
                     [parameters objectForKey:@"q"],
                     [parameters objectForKey:@"salt"],
                     key];
    [parameters setObject:[str md5String] forKey:@"sign"];
    
    [self.manager POST:@"https://fanyi-api.baidu.com/api/trans/vip/translate" 
      parameters:parameters
        headers:nil
        progress:^(NSProgress * _Nonnull uploadProgress) {} 
         success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                    NSError *error = nil;
                    B3STranslate *model = [[B3STranslate alloc] initWithDictionary:responseObject error:&error];
                    if (complete) {
                        complete(model);
                    }
                    if (!model) {
                        NSLog(@"translateText : %@",responseObject);
                    }
                } 
          failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              if (complete) {
                  complete(nil);
              }
          }];
}

@end
