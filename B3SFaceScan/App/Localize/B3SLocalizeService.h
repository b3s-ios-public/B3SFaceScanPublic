//
//  B3SLocalizeService.h
//  body3dscale
//
//  Created on 2018/8/11.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <Foundation/Foundation.h>

@interface B3SLocalizeService : NSObject

+ (instancetype)sharedInstance;

- (NSString *)getReverseLookupWithKey:(NSString *)key;

- (NSString *)lookupWithKey:(NSString *)key;

@end
