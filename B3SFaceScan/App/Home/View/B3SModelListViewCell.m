//
//  B3SModelListViewCell.m
//  B3SFaceScan
//
//  Created on 2020/8/3.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SModelListViewCell.h"
#import "B3S3DCaseModel.h"

@interface B3SModelListViewCell ()
@property (nonatomic, strong) UIView * containerView;
@property (nonatomic, strong) UILabel * timeLabel;
@property (nonatomic, strong) UILabel * modelLabelName;
@property (nonatomic, strong) UIImageView * modelImageView;

@property (nonatomic, strong)B3S3DCaseModel * model;
@end

@implementation B3SModelListViewCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.containerView];
        [self.containerView addSubview:self.timeLabel];
        [self.containerView addSubview:self.modelLabelName];
        [self.containerView addSubview:self.modelImageView];
        
        [self configeLayoutSubviews];
    }
    
    return self;
}

- (void)configeLayoutSubviews{
    
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.top.mas_equalTo(10);
        make.right.equalTo(self.contentView.mas_right).with.offset(-16);
        make.bottom.equalTo(self.contentView.mas_bottom);
    }];
    

    
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(18);
        make.top.mas_equalTo(33);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(34);
    }];
    
    
    [self.modelImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.containerView);
        make.right.equalTo(self.containerView.mas_right).with.offset(-10);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(80);
    }];

    
    [self.modelLabelName mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.timeLabel.mas_right).with.offset(10);
        make.right.equalTo(self.modelImageView.mas_left).offset(-10);
        make.centerY.equalTo(self.modelImageView.mas_centerY);
        make.height.mas_equalTo(21);
    }];
    
    
}

- (UIView *)containerView{
    
    if (!_containerView) {
        _containerView  = [[UIView alloc] init];
        _containerView.backgroundColor = HexColorWithAlpha(0x000000, 0.2);
        _containerView.layer.cornerRadius = 5.0;
        _containerView.layer.masksToBounds = YES;
    }
    return _containerView;
}

- (UILabel *)timeLabel{
    
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.font = [UIFont systemFontOfSize:12.0];
        _timeLabel.numberOfLines = 2;
        _timeLabel.textColor = HexColorWithAlpha(0xFFFFFF, 0.5);
    }
    return _timeLabel;
}

- (UILabel *)modelLabelName{
    
    if (!_modelLabelName) {
     
        _modelLabelName = [[UILabel alloc] init];
        _modelLabelName.textColor = [UIColor whiteColor];
        _modelLabelName.font = [UIFont boldSystemFontOfSize:15.0];
        _modelLabelName.textAlignment = NSTextAlignmentCenter;
    }
    return _modelLabelName;
}

- (UIImageView *)modelImageView{
    
    if (!_modelImageView) {
        _modelImageView = [[UIImageView alloc] init];
    }
    
    return _modelImageView;
}

- (void)setModel:(B3S3DCaseModel *)model{
    
    _model = model;
    
    self.timeLabel.text = model.creatTime;
    self.modelLabelName.text = model.modelName;
    self.modelImageView.image = model.modelFrontImage;
}


@end
