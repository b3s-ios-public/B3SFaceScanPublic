//
//  B3SModelListViewCell.h
//  B3SFaceScan
//
//  Created on 2020/8/3.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <UIKit/UIKit.h>
@class B3S3DCaseModel;


@interface B3SModelListViewCell : UITableViewCell


- (void)setModel:(B3S3DCaseModel *)model;

@end
