//
//  B3SHomeViewController.m
//  B3SFaceScan
//
//  Created on 2020/7/30.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SHomeViewController.h"
#import "B3SSettingViewController.h"
#import "B3SScanHomeViewController.h"
#import "B3SPreviewViewController.h"
#import <WTAuthorizationTool/WTAuthorizationTool.h>
#import <AVFoundation/AVFoundation.h>
#import <Toast.h>
#import "B3SLoginViewController.h"
#import "B3SModelListViewCell.h"
#import "B3S3DCaseModel.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <SSZipArchive/SSZipArchive.h>
#import "B3SUserManager.h"
#import "B3SModelService.h"

@interface B3SHomeViewController ()
@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * dataArray;
@property (nonatomic, strong) UIDocumentInteractionController * documentVC;
@end

@implementation B3SHomeViewController


+ (instancetype)new
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    return [sb instantiateViewControllerWithIdentifier:NSStringFromClass(self)];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArray = [[NSMutableArray alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToScan:) name:goToScanNoticifation object:nil];
    [self showBackgroundView];
    self.gk_navLeftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"setting"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemClick:)];
    self.gk_navRightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"camera"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(rightBarButtonItemClick:)];
    
    self.gk_navTitle = NSLocalizedString(@"模型列表", nil);
    self.gk_navBackgroundColor = [UIColor clearColor];
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(GK_IS_iPhoneX? 88 : 64);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
    [self.tableView setDelegate:(id<UITableViewDelegate> _Nullable)self];
    [self.tableView setDataSource:(id<UITableViewDataSource> _Nullable)self];
    [self.tableView registerClass:[B3SModelListViewCell class] forCellReuseIdentifier:@"cell"];
    self.tableView.rowHeight = 111;
    self.tableView.backgroundColor = [UIColor clearColor];
    
    [self setUpData];
    
    // 长按删除
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc]
      initWithTarget:self action:@selector(handleLongPress:)];
    [self.tableView addGestureRecognizer:longPressGesture];

    
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
}

- (void)setUpData{
    

    
    //每次启动沙盒路径都会变化，需要根据modelId去匹配
    
    NSString *documents = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *destFilePath = [documents stringByAppendingPathComponent:[NSString stringWithFormat:@"/B3SFaceScan/ModelFiles/"]];
    
    NSFileManager * manger = [NSFileManager defaultManager];

    NSError * error = nil;
    NSArray * dataArr = [manger contentsOfDirectoryAtURL:[NSURL fileURLWithPath:destFilePath] includingPropertiesForKeys:nil options:NSDirectoryEnumerationSkipsHiddenFiles error:&error];
    for (int i = 0; i<dataArr.count; i++) {
        
        NSURL * currentUrl  = [dataArr objectAtIndex:i];
        
        NSString * timeString = [currentUrl lastPathComponent];

        
          B3S3DCaseModel * model = [B3S3DCaseModel new];
          model.creatTime = timeString;

          model.localModelPath = currentUrl.resourceSpecifier;
          NSArray <NSURL *>* modelFiles = [manger contentsOfDirectoryAtURL:currentUrl includingPropertiesForKeys:nil options:NSDirectoryEnumerationSkipsHiddenFiles error:&error];
        [modelFiles enumerateObjectsUsingBlock:^(NSURL * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            if ([obj.lastPathComponent isEqualToString:@"0.png"]) {
                
                model.modelFrontImage =  [UIImage imageWithData:[NSData dataWithContentsOfURL:obj]];
                *stop = YES;
            }
        }];
        
        [modelFiles enumerateObjectsUsingBlock:^(NSURL * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            if ([obj.pathExtension isEqualToString:@"txt"]) {
                NSString * lastPath = [obj lastPathComponent];
                
                model.modelName =  [lastPath substringToIndex:lastPath.length - 4];
                *stop = YES;
            }
        }];
        
        [self.dataArray addObject:model];
    }

    [self.tableView reloadData];
}

- (void)leftBarButtonItemClick:(id)sender{
    
    [self.navigationController pushViewController:[B3SSettingViewController new] animated:YES];
}

- (void)goToScan:(NSNotification *)noti{
    //授权登陆成功·
     [self goto3DScanWithComplete:nil];
}

- (void)rightBarButtonItemClick:(id)sender{
    
    
    B3SUserManager * manager = [B3SUserManager sharedInstance];
    if (manager.isAuthorization) {
        [self goto3DScanWithComplete:nil];
    }else{
           B3SLoginViewController * logVC = [B3SLoginViewController new];
            logVC.canScan= YES;
           [self.navigationController pushViewController:logVC animated:YES];
    }

}


- (void)goto3DScanWithComplete:(dispatch_block_t)complete
{

   B3SScanHomeViewController *vc = [B3SScanHomeViewController new];
    __weak typeof(vc) wvc = vc;
    __weak typeof(self) wself = self;
    
    [WTAuthorizationTool requestCameraAuthorization:^(WTAuthorizationStatus status) {
        if (status == WTAuthorizationStatusAuthorized) {
            if (![AVCaptureDevice defaultDeviceWithDeviceType:AVCaptureDeviceTypeBuiltInTrueDepthCamera mediaType:AVMediaTypeVideo position:AVCaptureDevicePositionFront]) { //
                NSString *text = NSLocalizedString(@"This device unsupport scan beacause of lack front depth camera", nil);
                [[UIApplication sharedApplication].keyWindow makeToast:text];
            } else {
                vc.handleComplete = ^(NSString *modelPath, NSError * error) {
                    
                    if (!error) {
                                          
                        
                        //1 先把模型数据拷贝到documents下面
                       NSString * desPath = [self copyFileToDocumentsWithOriginPath:modelPath modelName:@"unnamed" customCopyFile:@[@"0.png"]];
                
                        NSLog(@"modelPath = %@", desPath);
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            B3S3DCaseModel * model = [B3S3DCaseModel new];
                            model.creatTime = [self timeStringOfCurrentDate];
                            model.modelName = @"unnamed";
                            model.localModelPath = desPath;
                            
                            NSData * data = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@",desPath,@"0.png"]]];
                            model.modelFrontImage = [UIImage imageWithData:data];
                            [self.dataArray insertObject:model atIndex:0];
                            [self.tableView beginUpdates];
                            
                            [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
                            [self.tableView endUpdates];
                            
                            B3SPreviewViewController *pvc = [B3SPreviewViewController new];
                            pvc.modelPath = desPath;
                            pvc.modelName = model.modelName;

                            [wvc.navigationController popViewControllerAnimated:NO]; //先退出扫描
                            [wself.navigationController pushViewController:pvc animated:YES];
                            
                            if (complete) {
                                complete();
                            }
                        });
                      
                    }

                };
                
                [wself.navigationController pushViewController:wvc animated:YES];
            }
            
            
        } else {
            NSString *text = NSLocalizedString(@"未取得相机的使用权限，请到设置中进行相应授权", nil);
            [[UIApplication sharedApplication].keyWindow makeToast:text];
        }
    }];
}


- (NSString *)copyFileToDocumentsWithOriginPath:(NSString *)filePath modelName:(NSString *)modelName customCopyFile:(NSArray *)customCopyFile
{
    //1 创建目标文件目录
    NSError *error = nil;
    NSString * timePath = [self timeStringOfCurrentDate];
    NSString *documents = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *destFilePath = [documents stringByAppendingPathComponent:[NSString stringWithFormat:@"/B3SFaceScan/ModelFiles/%@",timePath]];
    NSFileManager *mgr = [NSFileManager defaultManager];
    [mgr createDirectoryAtPath:destFilePath withIntermediateDirectories:YES attributes:@{} error:&error];
    NSString * path = [NSString stringWithFormat:@"%@/%@.txt",destFilePath,modelName];
 
    [modelName writeToURL:[NSURL fileURLWithPath:path] atomically:YES encoding:NSUTF8StringEncoding error:&error];
    if (error) {
        NSLog(@"%@", error);
    }
    
    // 2 拷贝文件到目标目录
    NSArray *coreFiles =  @[@"mesh.png",@"mesh.obj",@"mesh.mtl",@"ldmk3d.yml",@"corp.jpg",@"UUID.log",@"crop.png",  ];
    NSMutableArray *coreFileArray;
    if (customCopyFile && [customCopyFile count] > 0) {
        coreFileArray = [[NSMutableArray alloc]initWithObjects:coreFiles, customCopyFile, nil];
    } else {
        coreFileArray = [[NSMutableArray alloc]initWithObjects:coreFiles, nil];
    }
    for (NSArray *coreFiles in coreFileArray) {
        for (NSString *fileName in coreFiles) {
            @try {
                [mgr copyItemAtPath:[filePath stringByAppendingPathComponent:fileName] toPath:[destFilePath stringByAppendingPathComponent:fileName] error:&error];
            } @catch (NSException *exception) {
                NSLog(@"%@",exception);
            } @finally {
            }
        }
    }
    
    return destFilePath;
}

static  NSDateFormatter * dateFormatter = nil;

- (NSString *)timeStringOfCurrentDate{
    
    NSDate * date = [NSDate date];
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
    }
    dateFormatter.dateFormat = @"yyyy-MM-dd-HH:mm:ss";
    return  [dateFormatter stringFromDate:date];
    
}



- (UITableView *)tableView{
    
    if (!_tableView) {
        _tableView = [[UITableView alloc] init];
        _tableView.backgroundColor = [UIColor clearColor];
    }
    return _tableView;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    B3SModelListViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    B3S3DCaseModel * model = [self.dataArray objectAtIndex:indexPath.row];
    
    [cell setModel:model];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    B3S3DCaseModel * model = [self.dataArray objectAtIndex:indexPath.row];
    B3SPreviewViewController *pvc = [B3SPreviewViewController new];
    pvc.modelPath = model.localModelPath;
    pvc.modelName = model.modelName;
    if (model.localModelPath) {
        [self.navigationController pushViewController:pvc animated:YES];
    }
   
    
}


- (void)handleLongPress:(UILongPressGestureRecognizer *)gesture{
    
    if (gesture.state == UIGestureRecognizerStateBegan) {
        
        CGPoint location = [gesture locationInView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
        if (!indexPath){
            return;
        }
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        UIAlertAction * deleteAction = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            
          UIAlertController * sureAlert =  [UIAlertController alertControllerWithTitle:@"Warning" message:@"Are you sure delete the model? This Action unsupport undo" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            UIAlertAction * sureAction = [UIAlertAction actionWithTitle:@"Confirm" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                 
                B3S3DCaseModel * deleteModel = [self.dataArray objectAtIndex:indexPath.row];
                
                [self deleteModelFilesWithModelPath:deleteModel.localModelPath];
                
                [self.dataArray removeObjectAtIndex: indexPath.row];
                
                
                [self.view makeToast:@"Delete Success" duration:0.5 position:CSToastPositionCenter];
                [self.tableView beginUpdates];
                [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
                [self.tableView endUpdates];
              }];
            
            [sureAlert addAction:cancelAction];
            [sureAlert addAction:sureAction];
            
            
            [self presentViewController:sureAlert animated:YES completion:nil];
            
        }];
        
        UIAlertAction * exportAction = [UIAlertAction actionWithTitle:@"Export" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [self exportModelWithIndexPath:indexPath];
        }];

        
        UIAlertAction * renameAction = [UIAlertAction actionWithTitle:@"Rename" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [self renameModelWithIndexPath:indexPath];
        }];

        
        
        [alert addAction:cancelAction];
        [alert addAction:deleteAction];
        [alert addAction:exportAction];
        [alert addAction:renameAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)deleteModelFilesWithModelPath:(NSString *)modelPath{

  //  NSString * realPath = [modelPath substringToIndex:modelPath.length -1];
    
    NSError * error = nil;
    if(![[NSFileManager defaultManager] removeItemAtPath:modelPath error:&error]){
        NSLog(@"%@", error);
    }
    
}


- (void)renameModelWithIndexPath:(NSIndexPath *)indexPath{
    
      
    UIAlertController * sureAlert =  [UIAlertController alertControllerWithTitle:@"Rename" message:@"Please input the name you want to modify" preferredStyle:UIAlertControllerStyleAlert];
      
      UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
          
      }];
      UIAlertAction * sureAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
          B3S3DCaseModel * model = [self.dataArray objectAtIndex: indexPath.row];
          
          NSString * text = [[sureAlert.textFields firstObject] text];
          
          NSString * newName = (text && ![text isEqualToString:@""])? text:@"Deafult Name";
          model.modelName = newName;
          //删除txt文件，生成新的txt
          NSFileManager * manger = [NSFileManager defaultManager];
          NSString * modelPath = model.localModelPath;
          NSError * error = nil;
          NSArray <NSURL *>* modelFiles = [manger contentsOfDirectoryAtURL:[NSURL fileURLWithPath:modelPath] includingPropertiesForKeys:nil options:NSDirectoryEnumerationSkipsHiddenFiles error:&error];
          
          //遍历文件找到后缀名
          __block NSString * txtPath = nil;
          [modelFiles enumerateObjectsUsingBlock:^(NSURL * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
              if ([obj.pathExtension isEqualToString:@"txt"]) {
                  
                  txtPath = obj.resourceSpecifier;
                  *stop = YES;
              }
          }];
          if( [manger fileExistsAtPath:txtPath]){
              [manger removeItemAtPath:txtPath error:&error];
          } // 删除旧名字
          
          //写入新名字
             NSString * path = [NSString stringWithFormat:@"%@/%@.txt",modelPath,newName];
          
             [newName writeToURL:[NSURL fileURLWithPath:path] atomically:YES encoding:NSUTF8StringEncoding error:&error];
             if (error) {
                 NSLog(@"%@", error);
             }
          
          [self.view makeToast:@"Modify Success" duration:0.5 position:CSToastPositionCenter];
          [self.tableView reloadData];
      
        }];
      
      [sureAlert addAction:cancelAction];
      [sureAlert addAction:sureAction];
      
     [sureAlert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        
         textField.placeholder = @"Deafult Name";
    }];
      
      [self presentViewController:sureAlert animated:YES completion:nil];
      
    
}


- (void)exportModelWithIndexPath:(NSIndexPath *)indexPath{
    
    
    B3S3DCaseModel * currentModel = [self.dataArray objectAtIndex:indexPath.row];
  
     MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];

     hud.label.text = @"正在准备导出的模型文件";
    
     NSString * modelPath = currentModel.localModelPath;
    
      BOOL isExsit = [[NSFileManager defaultManager] fileExistsAtPath:modelPath];
       if(!isExsit) {
           hud.label.text = @"导出模型失败";
           [hud hideAnimated:YES afterDelay:1];
           return;
       }
       
       NSFileManager *fileMgr = [NSFileManager defaultManager];
       
      NSString *zipPath = [modelPath stringByAppendingPathComponent:[NSString stringWithFormat:@"export_%@.zip", currentModel.modelName]];
       BOOL isZipExist = [fileMgr fileExistsAtPath:zipPath];

       void(^showDocumentController)(void) = ^(void) {
           
           NSURL * fileUrl = [NSURL fileURLWithPath:zipPath];
           UIDocumentInteractionController *vc = [UIDocumentInteractionController interactionControllerWithURL:fileUrl];
           [vc presentOptionsMenuFromRect:self.view.bounds inView:self.view animated:YES];
           self.documentVC = vc;
       };
       
       if (isZipExist) {
           [fileMgr removeItemAtPath:zipPath error:nil];
       }

       NSString *currentModelDir = modelPath;
       
       B3SModelService *modelService = [B3SModelService new];
       dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
           NSURL *objFileUrl = [NSURL fileURLWithPath:[currentModelDir stringByAppendingPathComponent:@"mesh.obj"]];
           //先加载obj文件到内存顶点数组/面数组
           [modelService loadDataFromObjUrl:objFileUrl withUnit:B3SModelServicePrecisionUnitUnknown];
           NSError *error = nil;
           //再按照用户当前设定的导出量纲来导出到mesh_export.obj文件
           NSString *exportedObjPath = [currentModelDir stringByAppendingPathComponent:@"mesh_export.obj"];
           [[modelService exportObj] writeToFile:exportedObjPath
                                      atomically:YES
                                        encoding:NSUTF8StringEncoding
                                           error:&error];
           
           if (zipPath) {
               [SSZipArchive createZipFileAtPath:zipPath withFilesAtPaths:@[
                                                                            [currentModelDir stringByAppendingPathComponent:@"mesh_export.obj"],
                                                                            [currentModelDir stringByAppendingPathComponent:@"mesh.png"],
                                                                            [currentModelDir stringByAppendingPathComponent:@"mesh.mtl"],
                                                                            [currentModelDir stringByAppendingPathComponent:@"ldmk3d.yml"]
                                                                            ]];
               
               dispatch_async(dispatch_get_main_queue(), ^{
                   [hud hideAnimated:YES];
                   showDocumentController();
               });
           } else {
               hud.label.text = @"导出模型失败";
               [hud hideAnimated:YES afterDelay:3];

           }
       });
}



@end
