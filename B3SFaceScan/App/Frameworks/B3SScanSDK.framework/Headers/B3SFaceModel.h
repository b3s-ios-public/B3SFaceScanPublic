//
//  B3SFaceModel.h
//
//  Copyright © 2020 Body3DScale. All rights reserved.
//
//  final model: directory, 3d landmark array, mesh object. 
//

#ifndef B3SFaceModel_h
#define B3SFaceModel_h

#define FRONTAL_LANDMARKS_COUNT 68
#define LEFT_LANDMARKS_COUNT 8
#define RIGHT_LANDMARKS_COUNT 8
#define RIGHT_EAR_LANDMARKS_COUNT 20
#define LEFT_EAR_LANDMARKS_COUNT 20


#import <UIKit/UIKit.h>
#import "B3SPoints.h"

@class B3SLandmark3D;
@class B3SFaceMesh;

//=============================
// MARK: == final face model ==
//=============================

/**
  @brief the scanned face model result, containing:
  - a face mesh file, 
  - a texture image file, 
  - a list of 3d landmarks.
  - a frontal face image file
  
  @discussion
  You don't need to create an instance of this class by yourself. 
  Instead,when B3SScanner calls your implementation 
  of the `finishHandleWithModelPath` callback, you can use
  the B3SScanner.modelInfo property, which is an instance of this class and 
  stands for the final scanning result. You call `getMeshInfo` to load 
  the face mesh file into memory, call `getLandmark3D` to get the list of 
  3D facial landmarks, call `getMeshImage` to  get the texture image, 
  and call `getFrontalImage` to get the snapshot of frontal face.
 */
@interface B3SFaceModel : NSObject

/**
  @brief 
    the directory path where B3SScanner saves the final model 
    files after scanning.
  @discussion
    this path is actually the same as the arugment of 
    `finishHandleWithModelPath` callback.
 */
@property (nonatomic, copy) NSString *modelPath;

/**
 * @brief init an instance of this class with specified model path.
 * you can directly use the B3SScanner.modelInfo instance, or you create an 
 * instance using this method.
 * 
 * @param path  the director path where final model files saved.
 */
- (instancetype)initModelPath:(NSString *)path;

/**
 * @brief 
 *  load the mesh file(.obj) into memory object, i.e. an instance
 *  of the `B3SFaceMesh` class
 *  
 * @return an instance of `B3SFaceMesh`
 * @see B3SFaceMesh
 */
- (B3SFaceMesh *)getMeshInfo;

/**
 * @brief load the 3d landmarks.
 *
 * @return an instance of B3SLandmark3D
 * @see B3SLandmark3D 
 */
- (B3SLandmark3D *)getLandmark3D;

/**
 * @brief load the texture image as UIImage object
 *
 * @return UIImage object
 */
- (UIImage *)getMeshImage;

/**
 * @brief 
 *  load the picture of the frontal face captured during scanning. 
 *
 * @return UIImage of frontal face.
 */
- (UIImage *)getFrontalImage;

- (void)reset;

@end

//======================
// MARK: ==3d landmark==
//======================
/**
  @class B3SLandmark3D
  @brief 3d landmark vertices on the scanned facial mesh surface.
  @note What is a landmark vertex?
  B3SScanner provide you 68 or 108 3d landmark points of the scanned model.
  Those points are actually vetices on the facial mesh, 
  not a simple 3d point floating somewhere above the surface of the mesh.
  The difference between a vertex and a simple floating 3d point is that:
  a vertex is part of the mesh geometry, you can get its 3d coordinates 
  by vertex index into mesh.obj file, and a vertex is connected with other 
  surrounding vertices of the mesh; however a floating 3d point is just a 
  standalone and isolated point in 3d space, maybe it's perfectly positioned 
  on the mesh surface, but there isn't any topological relationship with 
  the mesh vertices or triangle facets.
  
  One advantage to create a vertex, instead of a floating point, 
  for each 3d facial landmark is that you can directly deform the facial 
  mesh by moving the landmark vertex using polygon mesh processing 
  algorithm like ARAP.

  So B3SLandmark3D object contain 3 lists of 3d landmark vertices, one is the list 
  of 3d landmarks on the frontal face view of the mesh, and the other 2 are left 
  and right face landmarks, each of which contains only the landmarks along 
  the outline of jaw.
  
  each 3d landmark is represented as a vertex id(vid, start from 0) of 
  the mesh vertex array(i.e. `B3SFaceMesh.vertices`). You can get its 
  3d position by using the vid as an index of the mesh vertex array. 
 */
@interface B3SLandmark3D : NSObject

/**
  @brief 68 3d landmarks array of the frontal face.
   the contour landmarks' position might be inaccurate.
 */
@property (nonatomic, strong) NSMutableArray *frontalVertIdxs;

/**
  @brief 3d contour landmark of the left side face.
  this contour is along the outline of the mandible. 
 */
@property (nonatomic, strong) NSMutableArray *leftVertIdxs;

/**
  @brief 3d contour landmark of the right side face.
  this contour is along the outline of the mandible. 
 */
@property (nonatomic, strong) NSMutableArray *rightVertIdxs;

/**
  @brief 3d contour landmark of the left ear.
 */
@property (nonatomic, strong) NSMutableArray *leftEarLdmks;

/**
  @brief 3d contour landmark points of the right ear.
 */
@property (nonatomic, strong) NSMutableArray *rightEarLdmks;
@end



//===============================
// MARK: == simple mesh object ==
//===============================
/**
  @brief simple Mesh object
 */
@interface B3SFaceMesh : NSObject

/**
  @brief all vertex positions
 */
@property (nonatomic, strong) NSMutableArray<B3SPoint3f *> *vertices;

/**
  @brief all vertex texture coordinates (u,v).
  
  if you find that the texture is rendered up side down, you have to
  modify the v component as follow:
    v = 1-v;
 */
@property (nonatomic, strong) NSMutableArray<B3SPoint2f *> *texCoords;

/**
  @brief all vertex normals.
 */
@property (nonatomic, strong) NSMutableArray<B3SPoint3f *> *normals;

/**
  @brief an array of triangle faces.
  
  @discussion
    Each face is an triangle, i.e. composed of 3 vertices. 
    Each vertex of a face has 3 kinds of attribute: 
      vertex-position, vertex-texcoord, vertex-normal.
    
    By convention, we describe a face as a tuple of vertex attribute indices,
    for example the first triangle face, denoted as face#0, composed of 3 
    vertices(V#0,V#1,V#2), arranged counter-clockwise, depicted as following: 
  @code
  // v0 represents an index into `vertices` array
  // vt0 represents an index into `texCoords` array
  // vn0 represents an index into `normals` array
     
                V#1 (v1,vt1,vn1)
                 /\
                /  \
           V#2 /____\ V#0 
    (v2,vt2,vn2)      (v0,vt0,vn0)
     
  // the `faces` array is organized as:
  [ 
    [B3SPoint3i{v0,vt0,vn0},B3SPoint3i{v1,vt1,vn1},B3SPoint3i{v2,vt2,vn2}], //face#0
    [B3SPoint3i{v3,vt3,vn3},B3SPoint3i{v4,vt4,vn4},B3SPoint3i{v5,vt5,vn5}], //face#1
    .....
  ] 
  @endcode
 */
@property (nonatomic, strong) NSMutableArray <NSArray<B3SPoint3i *> *>*faces;

/**
 * @brief init an instance of this class.
 *  It's not recommended to directly use this method.
 *  Instead, please use B3SFaceModel `getMeshInfo` method.
 *
 * @param count  capacity size of internal NSMutableArray.
 */
- (instancetype)initWithCount:(NSInteger)count;
@end





// MARK: == .obj file loader ==

/**
  @brief 
    mesh file(.obj) loader.
    Usually it's unnecessary to use this class directly.
    Instead, use the B3SFaceModel `getMeshInfo` method.
 */
@interface B3SMeshLoader : NSObject

/**
  @brief single instance
 */
+ (instancetype)shareInstance;

/**
 * @brief load an .obj file
 *
 * @param path  absolute path to the .obj file
 * @return a B3SFaceMesh instance
 * @see B3SFaceMesh
 */
-(B3SFaceMesh *)loadObj :(NSString *)path;

/**
 * @brief delete an .obj file
 *
 * @param path  absolute path to the .obj file
 * @return true - deleted; false - deletion failed.
 */
-(bool)delObj :(NSString *)path;
@end

#endif /* B3SFaceModel_h */
