//
//  B3SPoints.h
//
//  Copyright © 2020 Body3DScale. All rights reserved.
//

#ifndef B3SPoints_h
#define B3SPoints_h

#import <Foundation/Foundation.h>

/**
  @brief 2D point of integer coordinates
 */
@interface B3SPoint2i : NSObject

/// X
@property (nonatomic, assign) int x;

/// Y
@property (nonatomic, assign) int y;

/**
 * @brief init an instance of this class
 *
 * @param x  X coordinate value
 * @param y  Y coordinate value
 */
- (instancetype)initWithValue:(int)x y:(int)y;
@end

/**
  @brief 2D point of float coordinates
 */ 
@interface B3SPoint2f : NSObject

/// X
@property (nonatomic, assign) float x;

/// Y
@property (nonatomic, assign) float y;

/**
 * @brief init an instance of this class
 *
 * @param x  X coordinate value
 * @param y  Y coordinate value
 */
- (instancetype)initWithValue:(float)x y:(float)y;
@end

/**
  @brief 3D point of integer value
 */ 
@interface B3SPoint3i : NSObject

/// X
@property (nonatomic, assign) int x;

/// Y
@property (nonatomic, assign) int y;

/// Z
@property (nonatomic, assign) int z;

 /**
 * @brief init an instance of this class
 * @param x  X 
 * @param y  Y 
 * @param z  Z 
 */
- (instancetype)initWithValue:(int)x y:(int)y z:(int)z;
@end

/**
  @brief 3D point of float value
 */ 
@interface B3SPoint3f : NSObject

/// X
@property (nonatomic, assign) float x;

/// Y
@property (nonatomic, assign) float y;

/// Z
@property (nonatomic, assign) float z;

/**
 * @brief init an instance of this class
 * @param x  X 
 * @param y  Y 
 * @param z  Z 
 */
- (instancetype)initWithValue:(float)x y:(float)y z:(float)z;
@end

#endif /* B3SPoints_h */
