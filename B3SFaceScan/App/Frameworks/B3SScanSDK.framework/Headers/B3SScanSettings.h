//
//  B3SScanSettings.h
//
//  Copyright © 2020 Body3DScale. All rights reserved.
//
//
#import <Foundation/Foundation.h>
/**
 @enum B3STextureSize
 @brief 
  enumerations used to specify the texture resolution 
  for B3SScanner.scanSettings.
 */
typedef NS_ENUM(NSUInteger,B3STextureSize){
    /// resolution：3000+ x 2500 pixels, file format:.png, file-bytes:8MB+
    Largest,
    /// resolution：3000+ x 2500 pixels, file-format:.png(lossy compressed),file-bytes:3MB+
    Large,
    /// resolution：2000+ x 1200 pixels, file-format:png file-bytes:2MB+
    Normal
};

/**
  @class B3SScanSettings
  @brief 
    settings used to configure B3SScanner's behavior.
 */
@interface B3SScanSettings: NSObject

/**
  @brief 
    the minimum head-to-camera distance required to start scan, in meters, 
    default to 0.32 meter.
    
  @discussion
    There is a distance measured internally from user's head center to the 
    camera in realtime before scanning. if the distance is smaller than
    this property, B3SScanner will disallow starting scan.
    Lower bound of this property is 0.25 meter.
 */
@property (nonatomic, assign) float minDistance;

/**
  @brief 
    the maximum allowed head-to-camera distance to start scan, in meters, 
    default to 0.44 meter.
    
  @discussion
    There is a distance measured internally from user's head center to the 
    camera in realtime before scanning. if the distance is bigger than  
    this value, B3SScanner will disallow starting scan.
    Upper bound of this property is 0.60 meter
 */
@property (nonatomic, assign) float maxDistance;

/**
 @brief 
  the enum instance used to specify the resolution of the texture image
  of the final face model. default to Largest.
 */
@property (nonatomic, assign) B3STextureSize textureResolution;

/**
  @brief 
    whether enable facial retouching for the texture iamge, 
    it's the so-called facial beautification. default to YES.
  @discussion
    It retouches the texture image by Asian beautification rules: 
    whiten skin & flatten details, etc. If it's undesirable, please set this 
    property to NO before starting scan.(use B3SScanner.scanSettings property).  
 */
@property (nonatomic, assign) BOOL retouchFacialTexture;



/**
 @brief
    This value indicates to what extent the facial texture will be retouched.
     
    The default value is 25, a bigger value means 
    retouching the facial texture more heavily.
    Set it to 0 or set `retouchFacialTexture` to NO, if you prefer no retouching at all.
    
 @discussion
    The acceptable range of this value is from 0 to 100.
    if you specify a value beyond the range, the default value 
    will be used instead.
    
    if `retouchFacialTexture` is set to NO, this value has no effects.
      
*/

@property (nonatomic, assign) float retouchExtent;

/**
  @brief 
    specify whether B3SScanner should save the model 
    files to local storage of your device. default to YES.
 */
//@property (nonatomic, assign) BOOL isStoreMesh;

/**
  Unavailable, reserved. 
 */
@property (nonatomic, assign) BOOL saveFrontFacialIntrinsic;

/**
  @brief 
    whether the frontal face image should be saved.
    default to NO.
 */
@property (nonatomic, assign) BOOL saveFrontFacialImage;

/**
    Unavailable, reserved.
 */ 
@property (nonatomic, assign) BOOL saveFrontFacialDepth;

/**
  Unavailable, reserved.
 */
@property (nonatomic, assign) BOOL saveMeshRotateMatrix;

/**
  @brief
    whether we should make the face mesh upright(head is upward, jaw downward). 
    default to YES.
 */
@property (nonatomic, assign) BOOL makeMeshUpRight;


/**
  @brief
    whether we should align the origin of the coordinate frame of the face 
    model to the geometric center of the model. 
    default to NO, meaning that the origin is aligned with the nose tip.
 */
@property (nonatomic, assign) BOOL alignMeshOriginWithGeoCenter;

/**
  enable 106 facial landmarks, instead of ibug 68 landmarks.
  default to YES, meaning ibug68 landmarks will be generated for the reconstructed
  facial model.
 */
@property (nonatomic, assign) BOOL enable106Ldmk;

/**
 when YES, the scanner will invoke remote Ear-Landmark-Locating service,
 to generate 3d ear landmark points.
 */
@property (nonatomic, assign) BOOL enableEarLdmk;

/**
 the host address of the remote Ear-Landmark-Locating server.
 default to nil, which is only meaningful in dev mode.
 
 Note: You have to specify this property if enableEarLdmk is YES. 
 */
@property (nonatomic, assign) NSString * earLdmkServiceHost;

@end
