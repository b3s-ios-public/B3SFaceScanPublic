//
//  B3SScanQuality.h
//
//  Copyright © 2020 Body3DScale. All rights reserved.
// assessing the quality of the scanning procedure and the final model.

#import <Foundation/Foundation.h>
@class B3SScanScore;


/**
  @brief 
    assessing the quality of the scanning procedure and the final model.
  @discussion
    It contains an assessment of the user's head turning movement,
    and the estimated quality of the final model(i.e. B3SFaceModel).
    
    It only useful when you want teach your user to try getting a better face model.
 */
@interface B3SScanQuality : NSObject

/**
  @brief
    indicate whether the user's head had ever been moved out of screen. 
 */
@property (nonatomic, assign) bool isBeyondScreen;

/**
  @brief
   how many times the user's head was moved out of the screen(camera's FoV) during
   the head turning movement.  
 */
@property (nonatomic, assign) int beyondScreenCount;

/**
  @brief
    the leftmost way that the user's head had ever been during the 
    head turning movement, expressed by the absolute value of the yaw 
    angle in degree.
 */
@property (nonatomic, assign) float maxLeftYawAngle;

/**
  @brief
    the rightmost way that the user's head had ever been during the 
    head turning movement, expressed by the absolute value of the yaw 
    angle in degree.
 */
@property (nonatomic, assign) float maxRightYawAngle;

/**
  @brief
    the maximum pitch angle that the user's head had ever been during
    head turning movement, in absolute value of degree.
 */
@property (nonatomic, assign) float maxPitchAngle;

/**
  @brief
    the maximum roll angle that the user's head had ever been during 
    head turning movement, in absolute value of degree.
 */
@property (nonatomic, assign) float maxRollAngle;

/**
  @brief 
    indicate whether both the leftmost and rightmost yaw angles are big enough.
  
  @discussion
  at least one side of the final face model will be imcomplete or imperfect 
  if either one of those two yaw angles is insufficiently big(70 degree). 
 */
@property (nonatomic, assign) bool isAngleInsufficient;

/**
  @brief 
    indicate whether the user's head is raised up or lowered down too much during
    head turning movement.
 */
@property (nonatomic, assign) bool isPitchExceed;

/**
  @brief 
    indicate whether the user's head is tilted to the left or right shoulder too much
    during the head turning movement.
    
  @discussion
    we recomment to keep the head and neck upright while turning head to 
    the left and right side, not tilted toward the shoulders. 
    With that said, the algorithm usually still works well.
 */
@property (nonatomic, assign) bool isRollExceed;

/**
  @brief 
  the nearest distance of head to camera that the user had ever been during
  the head turning movement, in meter.
 */
@property (nonatomic, assign) float minDistance;

/**
 @brief 
  the farthest distance of head to camera that the user had ever been during
  the head turning movement, in meter.
 */
@property (nonatomic, assign) float maxDistance;

/// reserved.
@property (nonatomic, assign) int receiverFrameCount;

/// reserved.
@property (nonatomic, assign) int handleFrameCount;

/**
  @brief
    indicate whether the enviroment light is too bright.
  @discussion
    too bright environment, especially the highlight in user's background, is 
    bad for camera imaging and 3d scanning.
 */
@property (nonatomic, assign) bool isHighlight;

/**
  @brief
    indicate whether the environment light is too dim.
  @discussion
    dim environment is bad for camera imaging.
 */
@property (nonatomic, assign) bool isLowlight;

/**
  @brief
    indicate whether the user‘s hairstyle is long hair. reserved.
 */
@property (nonatomic, assign) bool isLongHair;

/**
  @brief
    how many times the user's head turning movement is over-speed(too fast).
 */
@property (nonatomic, assign) int overspeedFrameCout;

/**
 * @brief
 *  reserved.
 * @param isInsideScreen n/a
 */
- (void)setBeyondScreen:(bool)isInsideScreen;

/**
 * @brief reserved.
 * @param pitch pitch
 * @param yaw yaw
 * @param roll roll
 */
- (void)setEuler:(float)pitch yaw:(float)yaw roll:(float)roll;

/**
 * @brief reserved.
 * @param distance distance
 */
- (void)setDistance:(float)distance;

/**
 * @brief get the estimated quality of the final model.
 */
- (B3SScanScore*)getScore;

/**
 * @brief reserved.
 */
- (void)printScore;

/**
 * @brief reserved.
 */
- (void)print;

/**
 * @brief reserved.
 */
- (void)reset;
@end


/**
 * @brief The estimated quality of the final face model.
 */
@interface B3SScanScore : NSObject

/**
  @brief the estimated quality, expressed as a score(0~100).
  
  usually, 'score>=70' represent the quality of the face model is acceptable;
  'score>=80', the quality is good enough.
 */
@property (nonatomic, assign) float score;

/**
  @brief  
    an array of code nubmers, each indicates a bad movement during user's head turning.
  @discussion
    Specifically, possible codes and descriptions are:
  @code
      code      description
      5001  the leftmost angle your head turn is not enough
      5002  the rightmost angle your head turn is not enough
      5003  your hairstyle is long hair, bad for scanning by turning head.
      5008  head turning movement is overspeed several times,please turn slowly.
  @endcode
 */
@property (nonatomic, strong) NSArray <NSNumber *>*statuses;

/**
  @brief
    please refer to the documentation of `statuses` property.
 */
@property (nonatomic, strong) NSArray <NSString *>*descs;
@end
