//
//  B3SAlertCancelScanViewController.m
//  body3dscale
//
//  Created on 2018/7/11.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SAlertCancelScanViewController.h"
#import <ReactiveCocoa.h>
#import "BlocksKit+UIKit.h"

@interface B3SAlertCancelScanViewController ()

@property (weak, nonatomic) IBOutlet UIButton *okButton;

@property (weak, nonatomic) IBOutlet UILabel *subLabel;
@end

@implementation B3SAlertCancelScanViewController

+ (instancetype)new
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Scan" bundle:nil];
    return [sb instantiateViewControllerWithIdentifier:NSStringFromClass(self)];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    __weak typeof(self) wself = self;
    self.cancelButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        
        if (wself.cancelBlock) {
            wself.cancelBlock();
            wself.cancelBlock = nil;
        }
        
        [wself dismiss];
        return [RACSignal empty];
    }];
    
    self.okButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        if (wself.completeBlock) {
            wself.completeBlock();
            wself.completeBlock = nil;
        }
        
        [wself dismiss];
        return [RACSignal empty];
    }];
    
    self.subLabel.backgroundColor = [UIColor clearColor];
    [self.subLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [self.subLabel setFont:[UIFont systemFontOfSize:11]];
    NSMutableAttributedString *content = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"效果不好？查看具体原因"]];
    NSRange contentRange = {0,[content length]};
    [content addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:contentRange];
    self.subLabel.attributedText = content;
    
    UITapGestureRecognizer *tapGR = [UITapGestureRecognizer bk_recognizerWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        
        if (wself.reasonBlock) {
            wself.reasonBlock();
            wself.reasonBlock = nil;
        }
        
        [wself dismiss];
    }];
    [self.subLabel addGestureRecognizer:tapGR];
    self.subLabel.userInteractionEnabled = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
