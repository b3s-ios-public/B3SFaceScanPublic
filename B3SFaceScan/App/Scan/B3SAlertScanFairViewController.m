//
//  B3SAlertScanFairViewController.m
//  body3dscale
//
//  Created on 2018/8/7.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SAlertScanFairViewController.h"
#import <ReactiveCocoa.h>

@interface B3SAlertScanFairViewController ()

@end

@implementation B3SAlertScanFairViewController

+ (instancetype)new
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Scan" bundle:nil];
    return [sb instantiateViewControllerWithIdentifier:NSStringFromClass(self)];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    __weak typeof(self) wself = self;
    self.cancelButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        
        if (wself.cancelBlock) {
            wself.cancelBlock();
            wself.cancelBlock = nil;
        }
        
        [wself dismiss];
        return [RACSignal empty];
    }];
    
    self.rightButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        if (wself.complete) {
            wself.complete();
            wself.complete = nil;
        }
        
        [wself dismiss];
        return [RACSignal empty];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
