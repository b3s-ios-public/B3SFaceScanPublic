//
//  B3SPreviewViewController.h
//  body3dscale
//
//  Created on 28/02/2018.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SceneKit/SceneKit.h>
#import <ModelIO/ModelIO.h>
#import <SceneKit/ModelIO.h>

@interface B3SPreviewViewController : B3SBaseViewController

@property (nonatomic, copy) NSString * modelPath;
@property (nonatomic, copy) NSString * modelName;
@end
