//
//  B3SScanGuideViewController.m
//  body3dscale
//
//  Created on 2018/5/31.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SScanGuideViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <ReactiveCocoa.h>
//#import "B3SReachabilityService.h"
#import "B3SLocalizedHelper.h"

@interface B3SScanGuideViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *tutorialImageView;
@property (weak, nonatomic) IBOutlet UIButton *scanButton;

@property (strong, nonatomic) AVPlayer *myPlayer;//播放器
@property (strong, nonatomic) AVPlayerItem *item;//播放单元
@property (strong, nonatomic) AVPlayerLayer *playerLayer;//播放界面（layer）

@property (weak, nonatomic) IBOutlet UIView *playerView;

@end

@implementation B3SScanGuideViewController

+ (instancetype)new
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Scan" bundle:nil];
    return [sb instantiateViewControllerWithIdentifier:NSStringFromClass(self)];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.scanButton.layer.cornerRadius = CGRectGetHeight(self.scanButton.frame)/2.f;
    
    NSString *scanStandardUrl;
    if([B3SLocalizedHelper isChinese]) {
        scanStandardUrl = @"https://zmimage.oss-cn-shenzhen.aliyuncs.com/video/scan_standard.mp4";
    } else{
        scanStandardUrl = @"https://zmimage.oss-cn-shenzhen.aliyuncs.com/video/scan_standard_en.mp4";
    }
    NSURL *mediaURL = [NSURL URLWithString:scanStandardUrl];
    
    @weakify(self);
    
    void(^loadVideo)(void) = ^(void) {
        @strongify(self);
        self.item = [AVPlayerItem playerItemWithURL:mediaURL];
        self.myPlayer = [AVPlayer playerWithPlayerItem:self.item];
        self.playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.myPlayer];
        self.playerLayer.cornerRadius = 15.f;
        self.playerLayer.masksToBounds = YES;
        [self.playerView.layer addSublayer:self.playerLayer];
        self.playerLayer.videoGravity = @"AVLayerVideoGravityResizeFill";
        [self.myPlayer play];
    };

    loadVideo();
    
    self.scanButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
        return [RACSignal empty];
    }];
    
    [[[NSNotificationCenter defaultCenter] rac_addObserverForName:AVPlayerItemDidPlayToEndTimeNotification object:self.myPlayer.currentItem] subscribeNext:^(id x) {
        @strongify(self);
        [self.myPlayer seekToTime:CMTimeMake(0, 1)];
        [self.myPlayer play];
    }];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.playerLayer.frame = self.playerView.bounds;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.playerLayer.frame = self.playerView.bounds;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
