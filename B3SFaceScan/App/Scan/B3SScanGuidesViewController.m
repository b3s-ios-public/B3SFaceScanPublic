//
//  B3SScanGuidesViewController.m
//  body3dscale
//
//  Created on 2018/8/8.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SScanGuidesViewController.h"
#import "B3SScanGuide2_1ViewController.h"

@interface B3SScanGuidesViewController ()

@end

@implementation B3SScanGuidesViewController

+ (instancetype)new
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Scan" bundle:nil];
    return [sb instantiateViewControllerWithIdentifier:NSStringFromClass(self)];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"扫描教程";
    [self.tabBar setHidden:YES];
    
    // 动态切换教程步骤2
    if (B3SScanTypeLookup == self.scanType){
        
        NSMutableArray *childVc = [self.viewControllers mutableCopy];
        [childVc replaceObjectAtIndex:1 withObject:[B3SScanGuide2_1ViewController new]];
        [self setViewControllers:childVc animated:NO];
    }
    
}

@end
