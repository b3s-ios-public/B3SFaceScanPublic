//
//  B3SPreviewViewController.m
//  body3dscale
//
//  Created on 28/02/2018.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SPreviewViewController.h"
#import <YYKit.h>
#import "MBProgressHUD+B3S.h"
#import "B3SAlertCancelScanViewController.h"
#import "B3SAlertFeedbackViewController.h"
#import "B3SSceneHelper.h"
#import <Toast.h>
#import <SSZipArchive.h>
#import "B3SScanPoorReasonViewController.h"
#import "BlocksKit+UIKit.h"
#import <POP.h>
#import "B3SScanHelpViewController.h"
#import "B3SScanDefectViewController.h"
#import <B3SScanSDK/B3SScanQuality.h>
#import "B3SModelService.h"
#import "NSDictionary+JSON.h"

@interface B3SPreviewViewController ()<UIDocumentInteractionControllerDelegate>

@property (nonatomic, strong) SCNNode *node;
@property (strong, nonatomic) IBOutlet SCNView *mainView;
@property (weak, nonatomic) IBOutlet UIButton *exportButton;

@property (nonatomic, strong) UIDocumentInteractionController * documentVC;
@property (nonatomic, assign) BOOL isUploaded;

@property (nonatomic, assign) BOOL isShowAlert;
@end

@implementation B3SPreviewViewController

+ (instancetype)new
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Scan" bundle:nil];
    return [sb instantiateViewControllerWithIdentifier:NSStringFromClass(self)];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.mainView.allowsCameraControl = YES;
    self.mainView.autoenablesDefaultLighting = NO;
    self.mainView.backgroundColor = [UIColor blackColor];
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 11.0) {
        //左右只允许正负90, 上下不允许转动(maximumVerticalAngle设置0不会生效)
        if (@available(iOS 11.0, *)) {
            //self.mainView.defaultCameraController.maximumVerticalAngle = 0.001;
            self.mainView.defaultCameraController.maximumVerticalAngle = 15;
            self.mainView.defaultCameraController.minimumVerticalAngle = -45;
            self.mainView.defaultCameraController.maximumHorizontalAngle = 90;
            self.mainView.defaultCameraController.minimumHorizontalAngle = -90;
            
                self.mainView.defaultCameraController.maximumVerticalAngle = 15;
                self.mainView.defaultCameraController.minimumVerticalAngle = -45;
           
        }
    }
    self.view.backgroundColor = [UIColor blackColor];
    

    
    self.isUploaded = NO;
    

    self.isShowAlert = YES;
    
    // 捏合手势
    UIPinchGestureRecognizer *pinchGR = [[UIPinchGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
//        @strongify(self);
        UIPinchGestureRecognizer *pinch = (UIPinchGestureRecognizer *)sender;
        SCNMatrix4 t = self.node.transform;
        self.node.transform = SCNMatrix4MakeScale((t.m11 + pinch.velocity/10.f) > 1.f ? (t.m11 + pinch.velocity/10.f) : 1.0,
                                                  (t.m22 + pinch.velocity/10.f) > 1.f ? (t.m22 + pinch.velocity/10.f) : 1.0,
                                                  (t.m33 + pinch.velocity/10.f) > 1.f ? (t.m33 + pinch.velocity/10.f) : 1.0);
    }];
    [self.mainView addGestureRecognizer:pinchGR];
    
    // 点击手势
    UITapGestureRecognizer *tap2Gr = [[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
//        @strongify(self);
        self.node.transform = SCNMatrix4MakeScale(2.2, 2.2, 2.2);
    }];
    tap2Gr.numberOfTapsRequired = 2;
    [self.mainView addGestureRecognizer:tap2Gr];
    
    UIRotationGestureRecognizer *rotationGR = [[UIRotationGestureRecognizer alloc]
                                               bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
                                                   
                                               }];
    
    [self.mainView addGestureRecognizer:rotationGR];
    
    [self setupNavItems];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    

    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];

    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (!self.isUploaded) {
        SCNScene *scene = [B3SSceneHelper getSceneWithHeadByNode:_node modelDirUrl:[NSURL fileURLWithPath:self.modelPath]];
        self.mainView.scene = scene;
        self.node = scene.rootNode.childNodes.firstObject;
        
        NSString *pngFilePath = [self.modelPath stringByAppendingPathComponent:@"crop.png"];
        [UIImageJPEGRepresentation(self.mainView.snapshot, 0.7) writeToFile:pngFilePath  atomically:YES];
                
        self.isUploaded = YES;
    }
    

//
//    NSString *score = [NSString stringWithFormat:@"%.0f",[self.scanManager.scanInfo getScore].score];
//    NSNumber *scoreReasonCode = [self.scanManager.scanInfo getScore].statuses.firstObject;
//    
//    NSDictionary *scoreDescs = @{
//      @(5001):@"亲，您左转角度不够，请转够角度哦",
//      @(5002):@"亲，您右转角度不够，请转够角度哦",
//      @(5003):@"亲，您留着长头发呢，请把头发盘起来哦",
//      @(5004):@"亲，您转动过程中离屏幕太远了，请保持相对手机距离不变哦",
//      @(5005):@"亲，您转动过程中离屏幕太近了，请保持相对手机距离不变哦",
//      @(5006):@"亲，您背景光线太暗，请到更加光亮的地方哦",
//      @(5007):@"亲，您背景光线曝光了，请到不曝光的地方哦",
//      @(5008):@"亲，您转动太快了，请慢慢转头哦",
//      @(5009):@"亲，您转动过程中低头/抬头了，请保持头部水平旋转哦",
//      @(5010):@"亲，您转动过程中头部倾斜了，请保持头部水平旋转哦",
//      @(5011):@"亲，您转出屏幕，请保持头部水平旋转哦"
//      };
//
//    NSString *scoreReason = [scoreDescs objectForKey:scoreReasonCode];
    
    
}


- (IBAction)exportModelPressed:(id)sender{
    

        MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];

        hud.label.text = @"正在准备导出的模型文件";
       
        NSString * modelPath = self.modelPath;
       
         BOOL isExsit = [[NSFileManager defaultManager] fileExistsAtPath:modelPath];
          if(!isExsit) {
              hud.label.text = @"导出模型失败";
              [hud hideAnimated:YES afterDelay:1];
              return;
          }
          
          NSFileManager *fileMgr = [NSFileManager defaultManager];
          
    
    // 压缩的路径，需要docuemnts下才可以
         NSString *zipPath = [modelPath stringByAppendingPathComponent:[NSString stringWithFormat:@"export_%@.zip", self.modelName]];
          BOOL isZipExist = [fileMgr fileExistsAtPath:zipPath];
                    
          if (isZipExist) {
              [fileMgr removeItemAtPath:zipPath error:nil];
          }

          NSString *currentModelDir = modelPath; //原始模型目录
          
          B3SModelService *modelService = [B3SModelService new];
          dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
              NSURL *objFileUrl =[NSURL fileURLWithPath:[currentModelDir stringByAppendingPathComponent:@"mesh.obj"]];
              [modelService loadDataFromObjUrl:objFileUrl withUnit:B3SModelServicePrecisionUnitUnknown];
              NSError *error = nil;
              NSString *exportedObjFilePath = [currentModelDir stringByAppendingPathComponent:@"mesh_export.obj"];
              //根据用户设置的导出单位，生成了新的obj文件
              [[modelService exportObj] writeToFile:exportedObjFilePath
                                         atomically:YES
                                           encoding:NSUTF8StringEncoding
                                              error:&error];
              
              if (zipPath) {
                  [SSZipArchive createZipFileAtPath:zipPath withFilesAtPaths:@[
                                                                               [currentModelDir stringByAppendingPathComponent:@"mesh_export.obj"],
                                                                               [currentModelDir stringByAppendingPathComponent:@"mesh.png"],
                                                                               [currentModelDir stringByAppendingPathComponent:@"mesh.mtl"],
                                                                               [currentModelDir stringByAppendingPathComponent:@"ldmk3d.yml"]
                                                                               ]];
                  
                  dispatch_async(dispatch_get_main_queue(), ^{
                      [hud hideAnimated:YES];
                      [self showDocumentControllerWithZipPath:zipPath];
                  });
              } else {
                  hud.label.text = @"导出模型失败";
                  [hud hideAnimated:YES afterDelay:3];

              }
          });
}

- (void)showDocumentControllerWithZipPath:(NSString * )zipPath{
    
    UIDocumentInteractionController *vc = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:zipPath]];
    [vc presentOptionsMenuFromRect:self.view.bounds inView:self.view animated:YES];
    [vc setDelegate:(id<UIDocumentInteractionControllerDelegate> _Nullable)self];
    self.documentVC = vc;
    
}


@end
