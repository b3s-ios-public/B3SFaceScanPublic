//
//  B3SAlertScanFairViewController.h
//  body3dscale
//
//  Created on 2018/8/7.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SAlertController.h"

@interface B3SAlertScanFairViewController : B3SAlertController

@property (nonatomic, copy) dispatch_block_t complete;

@end
