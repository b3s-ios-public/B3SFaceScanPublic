//
//  B3SScanPoorReasonViewController.m
//  body3dscale
//
//  Created on 2018/7/11.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SScanPoorReasonViewController.h"
#import "B3SAlertNormalViewController.h"
#import <ReactiveCocoa.h>
#import <AVFoundation/AVFoundation.h>

@interface B3SScanPoorReasonViewController ()

@property (weak, nonatomic) IBOutlet UIButton *repeatScanButton;

@property (weak, nonatomic) IBOutlet UIView *videoLayerView;
@property (weak, nonatomic) IBOutlet UIView *view_3;

@property (strong, nonatomic) AVPlayer *myPlayer;//播放器
@property (strong, nonatomic) AVPlayerItem *item;//播放单元
@property (strong, nonatomic) AVPlayerLayer *playerLayer;//播放界面（layer）
@end

@implementation B3SScanPoorReasonViewController

+ (instancetype)new
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Scan" bundle:nil];
    return [sb instantiateViewControllerWithIdentifier:NSStringFromClass(self)];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"以下情况会导致扫描效果不佳";
    
    self.repeatScanButton.layer.cornerRadius = 20.f;
    
    @weakify(self);
    
    self.repeatScanButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        @strongify(self);
        B3SAlertNormalViewController *vc = [B3SAlertNormalViewController new];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.text = @"确定要放弃当前模型重新扫描吗?";
        vc.complete = ^{
            @strongify(self);
            [self.navigationController popToRootViewControllerAnimated:NO];
//            [[B3SRouteService sharedInstance] goto3DScan];
        };
        
        [self presentViewController:vc animated:NO completion:NULL];
        return [RACSignal empty];
    }];
    
    NSURL *mediaURL = [[NSBundle mainBundle] URLForResource:@"首页_轮播_1" withExtension:@"mp4"];
    void(^loadVideo)(void) = ^(void) {
        @strongify(self);
        self.item = [AVPlayerItem playerItemWithURL:mediaURL];
        self.myPlayer = [AVPlayer playerWithPlayerItem:self.item];
        self.playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.myPlayer];
        self.playerLayer.cornerRadius = 15.f;
        self.playerLayer.masksToBounds = YES;
        [self.videoLayerView.layer addSublayer:self.playerLayer];
        self.playerLayer.videoGravity = @"AVLayerVideoGravityResizeAspect";
        [self.myPlayer play];
    };
    
    [[[NSNotificationCenter defaultCenter] rac_addObserverForName:AVPlayerItemDidPlayToEndTimeNotification object:self.myPlayer.currentItem] subscribeNext:^(id x) {
        @strongify(self);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            @strongify(self);
            [self.myPlayer seekToTime:CMTimeMake(0, 1)];
            [self.myPlayer play];
        });
    }];
    
//    [RACObserve(self.item, status) subscribeNext:^(NSNumber *x) {
//        @strongify(self);
//        if (x.integerValue == AVPlayerItemStatusFailed) {
//            [[B3SReachabilityService sharedInstance].statusSignal subscribeNext:^(NSNumber *x) {
//                @strongify(self);
//                if ([[B3SReachabilityService sharedInstance] isReachable]) {
//                    loadVideo();
//                    self.playerLayer.frame = self.videoLayerView.bounds;
//                }
//            }];
//        }
//    }];
    
    loadVideo();
    
    self.view_3.layer.cornerRadius = 15.f;
    self.view_3.layer.masksToBounds = YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.playerLayer.frame = self.videoLayerView.bounds;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

}

@end
