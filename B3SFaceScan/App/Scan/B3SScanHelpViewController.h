//
//  B3SScanHelpViewController.h
//  body3dscale
//
//  Created on 2018/8/11.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <UIKit/UIKit.h>

@interface B3SScanHelpViewController : B3SBaseViewController

@property (nonatomic, copy) dispatch_block_t completeBlock;

@end
