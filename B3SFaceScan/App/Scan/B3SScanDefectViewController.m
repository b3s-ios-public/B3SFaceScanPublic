//
//  B3SScanDefectViewController.m
//  body3dscale
//
//  Created on 2018/9/7.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SScanDefectViewController.h"
#import <ReactiveCocoa.h>
#import "BlocksKit+UIKit.h"

@interface B3SScanDefectViewController ()

@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UIView *blankView;

@property (weak, nonatomic) IBOutlet UIView *alertView;
@property (weak, nonatomic) IBOutlet UIButton *goodButton;

@end

@implementation B3SScanDefectViewController

+ (instancetype)new
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Scan" bundle:nil];
    return [sb instantiateViewControllerWithIdentifier:NSStringFromClass(self)];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    @weakify(self);
    [RACObserve(self, message) subscribeNext:^(NSString *x) {
        @strongify(self);
        
        self.textLabel.text = x;
    }];
    
    self.alertView.layer.cornerRadius = 5.f;
    
    self.goodButton.layer.cornerRadius = CGRectGetHeight(self.goodButton.frame)/2.f;
    self.goodButton.layer.masksToBounds = YES;
    
    self.goodButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        @strongify(self);
        
        [self dismissViewControllerAnimated:NO completion:NULL];
        
        if (self.viewBlock) {
            self.viewBlock();
        }
        
        return [RACSignal empty];
    }];
    
    [self.alertView bk_whenTapped:^{
        @strongify(self);
        [self dismissViewControllerAnimated:YES completion:NULL];
    }];
    
    [self.blankView bk_whenTapped:^{
        @strongify(self);
        [self dismissViewControllerAnimated:YES completion:NULL];
    }];
    
}



@end
