//
//  B3SScanChild1ViewController.m
//  body3dscale
//
//  Created on 2019/5/8.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SScanChild1ViewController.h"

@interface B3SScanChild1ViewController ()

@end

@implementation B3SScanChild1ViewController

+ (instancetype)new
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Scan" bundle:[NSBundle bundleForClass:self]];
    return [sb instantiateViewControllerWithIdentifier:NSStringFromClass(self)];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.gk_navigationBar.hidden = YES;
    self.title = @"";
    self.pageIndex = 0;
}

@end
