//
//  body3dscale
//
//  Created on 2019/5/9.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SBaseViewController.h"
#import <SceneKit/SceneKit.h>
#import <SceneKit/ModelIO.h>
#import <B3SScanSDK/B3SFaceReport.h>

NS_ASSUME_NONNULL_BEGIN

@interface B3SBaseScanChildViewController : B3SBaseViewController

@property(nonatomic, assign) NSInteger pageIndex;

/// 指示点视图
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *indicatesView;

/// 示例模型
@property (weak, nonatomic) IBOutlet UIView *previewView;
@property (weak, nonatomic) IBOutlet SCNView *previewSceneView;

/// 相机画面渲染视图
@property (weak, nonatomic) IBOutlet UIView *renderView;
@property (weak, nonatomic) IBOutlet UIView *depthView;
@property (weak, nonatomic) IBOutlet UIView *smallView;

///// 围绕y轴旋转
//@property(nonatomic, assign) CGFloat rotateToY;
//
///// 围绕x轴旋转
//@property(nonatomic, assign) CGFloat rotateToX;

@property (nonatomic, strong) B3SFaceReport *faceInfo;
@property (assign, nonatomic) float maxYaw;
@property (assign, nonatomic) float minYaw;
@property (assign, nonatomic) float maxPitch;
@property (assign, nonatomic) float minPitch;
@property (assign, nonatomic) BOOL isScanning;

/// 人脸对准框
@property (weak, nonatomic) IBOutlet UIImageView *faceRectView;

@end

NS_ASSUME_NONNULL_END
