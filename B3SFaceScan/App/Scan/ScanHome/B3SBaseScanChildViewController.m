//
//  B3SBaseScanChildViewController.m
//  body3dscale
//
//  Created on 2019/5/9.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SBaseScanChildViewController.h"
#import <YYKit.h>
#import <ReactiveCocoa.h>

@interface B3SBaseScanChildViewController ()<ViewControllerProtocol>

@end

@implementation B3SBaseScanChildViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //[self.navigationController.navigationBar setHidden:YES];
    
    //  导航栏
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.gk_navigationBar.hidden = YES;
    [self setupViews];
    [self setupLayouts];
    [self setupRAC];
}

- (void)setupViews{
    
//    self.previewView.hidden = YES;
//
//    self.previewSceneView.scene = [SCNScene sceneNamed:@"anim.scn"];
//    self.previewSceneView.allowsCameraControl = NO;
//    self.previewSceneView.backgroundColor = [UIColor whiteColor];
//    self.previewSceneView.scene.rootNode.transform = SCNMatrix4MakeScale(0.8, 0.8, 0.8);
//
//    if (@available(iOS 11.0, *)) {
//        self.previewSceneView.defaultCameraController.maximumVerticalAngle = 0.001;
//        self.previewSceneView.defaultCameraController.maximumHorizontalAngle = 90;
//        self.previewSceneView.defaultCameraController.minimumHorizontalAngle = -90;
//    }
}

- (void)setupLayouts{
    
    for (UIImageView *dotView in self.indicatesView) {
        dotView.backgroundColor = [UIColor colorWithHexString:@"0x3b3b3b"];
        dotView.layer.cornerRadius = 6.f;
    }
}

- (void)setupRAC{
    
    @weakify(self);
//    [RACObserve(self, rotateToY) subscribeNext:^(NSNumber *y) {
//        @strongify(self);
//
//        SCNAction *action = [SCNAction rotateToX:0 y:y.floatValue z:0 duration:3];
//        [self.previewSceneView.scene.rootNode.childNodes.firstObject runAction:action];
//    }];
//
//    [RACObserve(self, rotateToX) subscribeNext:^(NSNumber *x) {
//        @strongify(self);
//
//        SCNAction *action = [SCNAction rotateToX:x.floatValue y:0 z:0 duration:3];
//        [self.previewSceneView.scene.rootNode.childNodes.firstObject runAction:action];
//    }];
    
    // 监听人脸角度信息
    [RACObserve(self, faceInfo) subscribeNext:^(B3SFaceReport *faceInfo) {
        @strongify(self);
        
        float yaw = faceInfo.yaw; // 绕y轴旋转的偏航角
        NSMutableArray *indexs = [NSMutableArray new];
        [indexs addObject:@(0)]; // 中间点默认高亮

        if (yaw > 0) {
            if (yaw > self.maxYaw) {
                self.maxYaw = faceInfo.yaw;
            }
        } else {
            if (yaw < self.minYaw) {
                self.minYaw = yaw;
            }
        }

        if (yaw >= 10 || self.maxYaw >= 10) {
            [indexs addObject:@(1)];
        }

        if (yaw >= 15 || self.maxYaw >= 15) {
            [indexs addObject:@(2)];
        }

        if (yaw >= 20 || self.maxYaw >= 20) {
            [indexs addObject:@(3)];
        }

        if (yaw >= 25 || self.maxYaw >= 25) {
            [indexs addObject:@(4)];
        }

        if (yaw >= 30 || self.maxYaw >= 30) {
            [indexs addObject:@(5)];
        }

        if (yaw >= 35 || self.maxYaw >= 35) {
            [indexs addObject:@(6)];
        }

        if (yaw >= 42 || self.maxYaw >= 42) {
            [indexs addObject:@(7)];
        }

        if (yaw >= 50 || self.maxYaw >= 50) {
            [indexs addObject:@(8)];
        }

        if (yaw <= -10 || self.minYaw <= -10) {
            [indexs addObject:@(-1)];
        }

        if (yaw <= -15 || self.minYaw <= -15) {
            [indexs addObject:@(-2)];
        }

        if (yaw <= -20 || self.minYaw <= -20) {
            [indexs addObject:@(-3)];
        }

        if (yaw <= -25 || self.minYaw <= -25) {
            [indexs addObject:@(-4)];
        }

        if (yaw <= -30 || self.minYaw <= -30) {
            [indexs addObject:@(-5)];
        }

        if (yaw <= -35 || self.minYaw <= -35) {
            [indexs addObject:@(-6)];
        }

        if (yaw <= -42 || self.minYaw <= -42) {
            [indexs addObject:@(-7)];
        }

        if (yaw <= -50 || self.minYaw <= -50) {
            [indexs addObject:@(-8)];
        }
        
        // 抬头
        if (yaw < 15 || yaw > -15){
            float pitch = faceInfo.pitch; // 绕x轴旋转的俯仰角
            //NSLog(@"pitch:%f", pitch);
            
            if (pitch > 0) {
                if (pitch > self.maxPitch) {
                    self.maxPitch = pitch;
                }
            } else {
                if (pitch < self.minPitch) {
                    self.minPitch = pitch;
                }
            }
            
            if (pitch >= 15 || self.maxPitch >= 15) {
                [indexs addObject:@(101)];
            }
            
            if (pitch >= 25 || self.maxPitch >= 25) {
                [indexs addObject:@(102)];
            }
        }
        
        
        [self displayIndicatesView:indexs];
    }];
}

/// 更新指示点视图
- (void)displayIndicatesView:(NSMutableArray *)indexs{
    
    UIColor *color = [UIColor colorWithHexString:@"0x3ad3aa"];
    UIColor *defaultColor = [UIColor colorWithHexString:@"0x3b3b3b"];
    
    for (UIImageView *view in self.indicatesView) {
        
        if([indexs containsObject:@(view.tag)] == YES){
            view.backgroundColor = color;
        }else{
            view.backgroundColor = defaultColor;
        }
    }
    
#ifdef DEBUG
    NSMutableString *result = [[NSMutableString alloc] init];
    for (NSNumber *index in indexs) {
        [result appendString:[index stringValue]];
    }
  //  NSLog(@"indexs: %@", result);
#endif
}

@end
