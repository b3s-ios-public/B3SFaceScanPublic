//
//  B3SScanChild2ViewController.m
//  body3dscale
//
//  Created on 2019/5/8.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SScanChild2ViewController.h"

@interface B3SScanChild2ViewController ()

@end

@implementation B3SScanChild2ViewController

+ (instancetype)new
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Scan" bundle:[NSBundle bundleForClass:self]];
    return [sb instantiateViewControllerWithIdentifier:NSStringFromClass(self)];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"";
    self.pageIndex = 1;
    
}

@end
