//
//  B3SScanChild2ViewController.h
//  body3dscale
//
//  Created on 2019/5/8.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SBaseViewController.h"
#import "B3SBaseScanChildViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface B3SScanChild2ViewController : B3SBaseScanChildViewController

@end

NS_ASSUME_NONNULL_END
