//
//  B3SScanPageViewController.h
//  body3dscale
//
//  Created on 2019/5/8.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <UIKit/UIKit.h>
#import "B3SBaseScanChildViewController.h"

NS_ASSUME_NONNULL_BEGIN

@protocol B3SSCanPageViewControllerDataSource <NSObject>

/// 一共有多少页
- (NSInteger)numbersOfPage;

/// 每页对应的VC
- (UIViewController *)previewController:(NSInteger)index;

/// 选择子页面下标
//- (void)didSelected:(NSInteger)index;

@end

@interface B3SScanPageViewController : UIPageViewController

@property(nonatomic, weak) id<B3SSCanPageViewControllerDataSource> pageDataSource;

/// 当前选择页面的下标
@property(atomic, assign) NSInteger index;

/// 当前选择子控制器
- (B3SBaseScanChildViewController *)currPage;

/// 选择子页面
- (void)selectChildPage:(NSInteger)index;

@end

NS_ASSUME_NONNULL_END
