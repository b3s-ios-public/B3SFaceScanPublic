//
//  B3SScanPageViewController.m
//  body3dscale
//
//  Created on 2019/5/8.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SScanPageViewController.h"
#import "UIViewController+UI.h"

@interface B3SScanPageViewController ()<UIPageViewControllerDataSource, UIPageViewControllerDelegate>

@end

@implementation B3SScanPageViewController

+ (instancetype)new
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Scan" bundle:[NSBundle bundleForClass:self]];
    return [sb instantiateViewControllerWithIdentifier:NSStringFromClass(self)];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //[self.navigationController.navigationBar setHidden:YES];
    
    //  导航栏
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataSource = self;
    self.delegate = self;
    
    UIViewController * vc = [self.pageDataSource previewController:0];
    [self setViewControllers:@[vc] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:NULL];
}

/// 当前选择子控制器
- (B3SBaseScanChildViewController *)currPage{
    return (B3SBaseScanChildViewController *)[self.pageDataSource previewController:self.index];
}

/// 选择子页面
- (void)selectChildPage:(NSInteger)index{
    
    __weak typeof(self) wSelf = self;
    UIPageViewControllerNavigationDirection direction = index > self.index ? UIPageViewControllerNavigationDirectionForward : UIPageViewControllerNavigationDirectionReverse;
    UIViewController * vc = [self.pageDataSource previewController:index];
    [self setViewControllers:@[vc] direction:direction animated:NO completion:^(BOOL finished) {
        if (finished) {
            dispatch_async(dispatch_get_main_queue(), ^{
                wSelf.index = index;
                [wSelf impactFeedback];
            });
        }
    }];
}

#pragma mark - UIPageViewControllerDataSource
- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController{
    return [self.pageDataSource numbersOfPage];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController{
    
    NSInteger index = ((B3SBaseScanChildViewController *)viewController).pageIndex;
    if (index == 0 || index == NSNotFound) {
        return nil;
    }
    return [self.pageDataSource previewController:index - 1];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController{
    
    NSInteger index = ((B3SBaseScanChildViewController *)viewController).pageIndex;
    NSInteger count = [self.pageDataSource numbersOfPage];
    if (index == NSNotFound || index == count - 1) {
        return nil;
    }
    return [self.pageDataSource previewController:index + 1];
}

#pragma mark - UIPageViewControllerDelegate
- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray<UIViewController *> *)pendingViewControllers{
    
    // 转场动画未执行完，关闭用户交互
    //pageViewController.view.userInteractionEnabled = NO;
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed{
    
    //if (completed && finished){
    // 转场动画完成，开启用户交互
    //pageViewController.view.userInteractionEnabled = YES;
    //}
    
    self.index = ((B3SBaseScanChildViewController *)(pageViewController.viewControllers.firstObject)).pageIndex;
    //[self.pageDataSource didSelected:self.index];
    [self impactFeedback];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
