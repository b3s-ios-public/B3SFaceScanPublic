//
//  B3SScanHomeViewController.h
//  body3dscale
//
//  Created on 2019/5/8.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SBaseViewController.h"
#import "B3SCaseUploadDetail.h"

#import <B3SScanSDK/B3SScanSDK.h>

typedef void(^HandleComplete)(NSString *modelPath, NSError * error);

@interface B3SScanHomeViewController : B3SBaseViewController

@property (nonatomic, copy) HandleComplete handleComplete;
@property (nonatomic, strong) B3SScanner *scanManager;
@end
