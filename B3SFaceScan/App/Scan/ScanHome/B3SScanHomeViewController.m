//
//  B3SScanHomeViewController.m
//  body3dscale
//
//  Created on 2019/5/8.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SScanHomeViewController.h"
#import "UIViewController+UI.h"
#import "UIDevice+Model.h"
#import <YYKit.h>
#import "B3SScanGuidesViewController.h"
#import "B3SBaseScanChildViewController.h"
#import "B3SScanChild1ViewController.h"
#import "B3SScanChild2ViewController.h"
#import "B3SAlertScanFairViewController.h"
#import <Masonry.h>
#import <ReactiveCocoa.h>

#import <AVFoundation/AVFoundation.h>
#import "B3SSpeechService.h"
#import "B3SScanPageViewController.h"
#import <CoreMotion/CoreMotion.h>

#define ScanInterval 2.5    // 扫描时间间隔
#define ScanUpInterval 1.5  // 抬头扫描时间间隔

@interface B3SScanHomeViewController ()<ViewControllerProtocol, B3SSCanPageViewControllerDataSource>

// 陀螺仪
@property (nonatomic, strong) CMMotionManager *cmMotionManager;

@property(nonatomic, strong) NSArray *childsVc;
@property(atomic, assign) NSInteger index;

@property (assign, nonatomic) BOOL isScanning;
@property (nonatomic, strong) CIImage *currentCameraCIImage;
@property (assign, nonatomic) BOOL rightAngle;

/// 分页管理器
@property(nonatomic, strong) B3SScanPageViewController *pageViewController;
@property (weak, nonatomic) IBOutlet UIView *containerView;

/// 视频画面
@property (weak, nonatomic) IBOutlet UIView *renderView;
/// 人脸对准框
@property (weak, nonatomic) IBOutlet UIImageView *faceRectView;

/// 扫描时,模特引导用户转头的动画界面(该view包含一个sceneView,其中有一个白模头部模型在匀速地转动)
@property (weak, nonatomic) IBOutlet UIView *previewView;
@property (weak, nonatomic) IBOutlet SCNView *previewSceneView;

/// 扫描按钮
@property (weak, nonatomic) IBOutlet UIButton *scanBtn;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttons;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scanBtnWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scanBtnHeight;

/// 底部工具条
@property (weak, nonatomic) IBOutlet UIView *toolBar;

// 结束视图
@property (weak, nonatomic) IBOutlet UIView *finishView;
@property (weak, nonatomic) IBOutlet UIImageView *finishImageView;
@property (weak, nonatomic) IBOutlet UIImageView *finishUpImageView;

@property (weak, nonatomic) IBOutlet UILabel *finishLabel;

/// 扫描提示标志记录
@property (strong, nonatomic) NSMutableDictionary *scanFlags;
/// 触感标志记录
@property (strong, nonatomic) NSMutableDictionary *impactFlags;

/// 忽略发型
@property (nonatomic, assign) BOOL ignoreHair;

/// 围绕y轴旋转
@property(nonatomic, assign) CGFloat rotateToY;

/// 围绕x轴旋转
@property(nonatomic, assign) CGFloat rotateToX;

/// 语言转化
@property (strong, nonatomic) UILabel *speechLabel;

// 显示控制进度条
@property (nonatomic, assign) float genModelProgressDisplay;

@end

@implementation B3SScanHomeViewController

// MARK: == life cycle ==
+ (instancetype)new
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Scan" bundle:[NSBundle bundleForClass:self]];
    return [sb instantiateViewControllerWithIdentifier:NSStringFromClass(self)];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.gk_navigationBar.hidden = NO;
    self.gk_navBackgroundColor = [UIColor clearColor];
    self.gk_navTitleColor = [UIColor whiteColor];
    self.gk_navTitle = @"Scan";
    [self setupNavItems];
   
    self.childsVc = @[[B3SScanChild1ViewController new], [B3SScanChild2ViewController new]];
    
    self.scanFlags = [NSMutableDictionary dictionary];
    self.impactFlags = [NSMutableDictionary dictionary];
    self.speechLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [self setupScanManager];
    [self setupMotionManager];
    
    [self setupNotify];
    [self setupViews];
    [self setupLayouts];
    [self setupRAC];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"B3SScanHomeViewController dealloc");
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

    // 主界面重新展示的时候都需要启动摄像头,初始化扫描算法资源
    [self openCameraAndStartStream];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    });
    

    BOOL isShow = [[NSUserDefaults standardUserDefaults] boolForKey:@"B3SScanViewController_Guides"];
    if (!isShow) {
       
        [self helpAction:nil];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"B3SScanViewController_Guides"];
    }
    
    //初始化audiosession
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil]; // 会停止其它音频播放，并且能在后台播放
    [audioSession setActive:YES error:nil];
}



- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    //离开时关闭相机,但不清理扫描算法资源
    [self closeCameraAndStopStream];
}

// MARK: ==open & close camera==

-(void) openCameraAndStartStream
{
  // specify a layer of app's UIView for previewing camera video stream
  [self.scanManager setCameraPreviewRootLayer:self.renderView.layer];
  
  // open camera and prepare resources for scanning.
  // `mirror` argument defaults to NO. you can change to YES if you like.
  [self.scanManager startCamera:NO withCompletion:^(NSError *error) {
    if(error){
      //couldn't open camera(see the documentation of `startCamera`)
      __weak typeof(self) wself=self;
      dispatch_async(dispatch_get_main_queue(), ^{
        //here please inform your user
        __strong typeof(wself) sself=wself;
        sself.gk_navTitle=error.localizedDescription;
      });
    }
  }];
  
}

-(void) closeCameraAndStopStream
{
  // close camera, detach the preview layer from app's UIView.
  [self.scanManager stopCamera];
}


/// 初始化扫描管理对象
- (void)setupScanManager{
    // 初始化扫描管理服务
    self.scanManager = [B3SScanner new];
    
    self.scanManager.scanSettings.retouchExtent = 32.f;
    self.scanManager.scanSettings.saveFrontFacialImage = YES;
    // 配置渲染引擎代理回调block
    __weak typeof(self) wself = self;
    self.scanManager.finishHandleWithModelPath = ^(NSString *modelPath, NSError * error) {
        
        if (!error) {
            if (wself.handleComplete) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
                    wself.handleComplete(modelPath,nil);
                });
            }
        }else{
            
        }

    };
    
    // 配置异常终止回调block
    self.scanManager.abortWithError = ^(int code, NSString *desc) {
        
        //NSUInteger code = wself.scanManager.genModelErrorCode;
        NSString *message = @"";
        
        if (code == 4000) {
            message = @"亲，您转的太快了，请重新扫描哦";
        } else if (code == 40052) {
            message = @"亲，抬头过高，请重新扫描哦";
        } else if ( code >= 3998 && code <= 4016) {
            message = @"亲，扫描出错了，请重新扫描哦";
        } else if ( code >= 4017 && code <= 4046) {
            message = @"亲，模型生成出错了，请重新扫描哦";
        } else {
            message = @"亲，扫描出错了，请将问题反馈给我们哦";
        }
        
        NSLog(@"错误码: %lu , 内容: %@, 原始内容: %@",(unsigned long)code, message, desc);
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"异常" message:message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"知道了" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSLog(@"mgr close");
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                  [wself.navigationController popViewControllerAnimated:YES];
            }); // 延迟1s再退出，保证sdk内部线程干完事情，虽然有点戳。
          
        }];
        [alertController addAction:action];
        [wself presentViewController:alertController animated:YES completion:NULL];
        
        [wself.scanManager cancel:^{
            
        }];
        [wself.scanManager stopCamera];
        [wself finishScanWithOutHandling];
    };
    
}

- (void)willEnterForeground
{
    // 主界面重新展示的时候都需要启动摄像头
    [self openCameraAndStartStream];
}

- (void)setupMotionManager{
    
    if (self.cmMotionManager == nil) {
        CMMotionManager *manager = [[CMMotionManager alloc] init];
        self.cmMotionManager = manager;
    }
    
    //判断陀螺仪可不可以，判断陀螺仪是不是开启
    if ([self.cmMotionManager isGyroAvailable] && ![self.cmMotionManager isGyroActive]){
        
        //告诉manager，更新频率是10Hz
        self.cmMotionManager.gyroUpdateInterval = 0.1;
        //Push方式获取和处理数据
        __weak typeof(self) wSelf = self;
        [self.cmMotionManager startDeviceMotionUpdatesToQueue:[NSOperationQueue mainQueue]
                                                  withHandler:^(CMDeviceMotion * _Nullable motion,
                                                                NSError * _Nullable error) {
                                                      
                                                      // Gravity 获取手机的重力值在各个方向上的分量，根据这个就可以获得手机的空间位置，倾斜角度等
                                                      
                                                      double gravityX = motion.gravity.x;
                                                      double gravityY = motion.gravity.y;
                                                      double gravityZ = motion.gravity.z;
                                                      
                                                      // 获取手机的倾斜角度(zTheta是手机与水平面的夹角， xyTheta是手机绕自身旋转的角度)：
                                                      double zTheta = atan2(gravityZ,sqrtf(gravityX * gravityX + gravityY * gravityY)) / M_PI * 180.0;

                                                      
                                                      // 手机角度判断
                                                      //NSLog(@"zTheta:%f", zTheta);
                                                      if (fabs(zTheta) <= 30) {
                                                          wSelf.rightAngle = YES;
                                                      }else{
                                                          wSelf.rightAngle = NO;
                                                      }
                                                  }];
    }
}

- (BOOL)navigationShouldPopOnBackButton
{
    //关掉相机，避免新的数据进入RenderManager
    [self closeCameraAndStopStream];
    
    //给scanManager置取消状态，scanManager各个线程会检查此状态并在适当的时机结束自己
    [self.scanManager cancel:^{
        
    }];
    
    
    return YES;
}

- (void)setupNotify{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willEnterForeground)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
}

- (void)setupViews{
    
    self.finishView.hidden = YES;
    self.previewView.hidden = YES;
    
    self.pageViewController = (B3SScanPageViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"B3SScanPageViewController"];
    self.pageViewController.pageDataSource = self;
    [self.containerView addSubview:self.pageViewController.view];
    [self addChildViewController:self.pageViewController];

    // 转头引导动画用到的模型
    self.previewSceneView.scene = [SCNScene sceneNamed:@"head-rotate-2.scn"];
    self.previewSceneView.allowsCameraControl = NO;
    self.previewSceneView.backgroundColor = [UIColor whiteColor];
    self.previewSceneView.scene.rootNode.transform = SCNMatrix4MakeScale(0.8, 0.8, 0.8);
    //若开启sceneView内的默认灯光,则需要修改上述.scn中加在人头模型上的直射灯光的强度,改小一点
    //self.previewSceneView.autoenablesDefaultLighting=YES;
    
    if (@available(iOS 11.0, *)) {
        self.previewSceneView.defaultCameraController.maximumVerticalAngle = 0.001;
        self.previewSceneView.defaultCameraController.maximumHorizontalAngle = 90;
        self.previewSceneView.defaultCameraController.minimumHorizontalAngle = -90;
    }
    
    NSInteger type = [[NSUserDefaults standardUserDefaults] integerForKey:@"B3SScanHomeViewController_Type"];
    if (type) {
        
        [self.pageViewController selectChildPage:type];
    }
}

- (void)setupLayouts{
    
    if ([UIDevice isIpad] == YES) {
        self.scanBtnWidth.constant = 44;
        self.scanBtnHeight.constant = 44;
    } else {
        self.scanBtnWidth.constant = 64;
        self.scanBtnHeight.constant = 64;
    }

    for (UIButton *btn in self.buttons) {
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithRed:0 green:0.841 blue:0.658 alpha:1] forState:UIControlStateSelected];
        btn.backgroundColor = [UIColor clearColor];
    }
    
//    [self.pageViewController.view.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        if ([obj isKindOfClass:[UIScrollView class]])
//        {
//            UIScrollView *scrollView = (UIScrollView *)obj;
//            scrollView.bounces = NO; // 禁止反弹效果
//        }
//    }];
    
    [self.pageViewController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.containerView);
    }];
}

- (void)setupRAC{
    
    [self.scanBtn setImage:[UIImage imageNamed:@"scan_enable"] forState:UIControlStateNormal];
    [self.scanBtn setImage:[UIImage imageNamed:@"scan_disable"] forState:UIControlStateDisabled];
    
    @weakify(self);
    __weak typeof(self) wself = self;
    self.scanBtn.rac_command = [[RACCommand alloc] initWithEnabled:[RACObserve(self.faceRectView, highlighted) map:^id(NSNumber *value) {
        return @(!value.boolValue);
    }] signalBlock:^RACSignal *(UIButton *sender) {
        @strongify(self);
        
        if (!sender.selected) {
            sender.selected = YES;
        
            [wself.impactFlags removeAllObjects];
            
            NSError *error=[wself.scanManager startScan];
            if(error){
                [self.view makeToast:[error localizedDescription] duration:0.3 position:CSToastPositionCenter];
                return [RACSignal empty];
            }
            
            // 开始扫描
            [self startGuideVoiceWithComplete:^(void){
           
              wself.isScanning = YES;
              sender.hidden = YES;
              
            }];
        }
        return [RACSignal empty];
    }];
    
    [RACObserve(self.pageViewController, index) subscribeNext:^(NSNumber *i) {
        @strongify(self);
        
        NSInteger index = i.integerValue;
        self.index = index;
        [self onSelected:index];
        
        NSString *key = @"B3SScanHomeViewController_Type";
        [[NSUserDefaults standardUserDefaults] setInteger:self.index forKey:key];
    }];
    
    // 绕y轴旋转
    [RACObserve(self, rotateToY) subscribeNext:^(NSNumber *y) {
        @strongify(self);
        
        SCNAction *action = [SCNAction rotateToX:0 y:y.floatValue z:0 duration:ScanInterval];
        [self.previewSceneView.scene.rootNode.childNodes.firstObject runAction:action];
    }];
    
    // 绕x轴旋转
    [RACObserve(self, rotateToX) subscribeNext:^(NSNumber *x) {
        @strongify(self);
        
        SCNAction *action = [SCNAction rotateToX:x.floatValue y:0 z:0 duration:ScanUpInterval];
        [self.previewSceneView.scene.rootNode.childNodes.firstObject runAction:action];
    }];
    
    [RACObserve(self, isScanning) subscribeNext:^(NSNumber *x) {
        @strongify(self);
        
        // 扫描中禁止切换扫描模式
        self.pageViewController.view.userInteractionEnabled = ![x boolValue];
    }];
    
    // 监听人脸识别信息
    [RACObserve(self.scanManager, faceInfo) subscribeNext:^(B3SFaceReport *faceInfo) {
        @strongify(self);
        
        dispatch_async_on_main_queue(^{
            @strongify(self);
            
            UIImageView *faceRectView = self.faceRectView;
            if (!faceInfo) {
                faceRectView.highlighted = YES;
                return ;
            }
            
            if (self.rightAngle == NO) {
                NSString *text = @"请平举手机";
                NSString *key = @"upright";
                [wself speak:text key:key];
                faceRectView.highlighted = YES;
                return ;
            }
            
            if (self.isScanning) {
                // 更新顶部指示点视图
                [self.pageViewController currPage].faceInfo = faceInfo;
                
                // 触感
                if(faceInfo.yaw >= 50){
                    [self impactFlag: 50];
                }
                
                if(faceInfo.yaw <= -50){
                    [self impactFlag: -50];
                }
                
                if(faceInfo.pitch >= 25){
                    [self impactFlag: 25];
                }
            }
            
            //self.faceInfo = faceInfo;
            faceRectView.highlighted = NO;
            switch (faceInfo.distanceInfo) {
                case 0: // 距离合适
                    
                    switch (faceInfo.eulerAnglesInfo) {
                        case 1:
                        {
                            faceRectView.highlighted = YES;
                            NSString *text = @"请低头";
                            NSString *key = @"eulerAnglesInfo_1";
                            [wself speak:text key:key];
                        }
                            break;
                        case 2:
                        {
                            faceRectView.highlighted = YES;
                            NSString *text = @"请抬头";
                            NSString *key = @"eulerAnglesInfo_2";
                            [wself speak:text key:key];
                        }
                            break;
                        case 3:
                        {
                            faceRectView.highlighted = YES;
                            NSString *text = @"请平视";
                            NSString *key = @"eulerAnglesInfo_3";
                            [wself speak:text key:key];
                        }
                            break;
                        case 4:
                        {
                            faceRectView.highlighted = YES;
                            NSString *text = @"请平视";
                            NSString *key = @"eulerAnglesInfo_4";
                            [wself speak:text key:key];
                        }
                            break;
                            
                        default:
                            break;
                    }
                    break;
                case 1: // 距离太远
                {
                    faceRectView.highlighted = YES;
                    NSString *text = @"请近一点";
                    NSString *key = @"distanceInfo_1";
                    [wself speak:text key:key];
                }
                    break;
                case 2: // 距离太近
                {
                    faceRectView.highlighted = YES;
                    NSString *text = @"请远一点";
                    NSString *key = @"distanceInfo_2";
                    [wself speak:text key:key];
                }
                    break;
                default:
                    break;
            }
            
            // 发型太长影响扫描
            if (faceInfo.hairInfo != 0 && !self.ignoreHair) {
                faceRectView.highlighted = YES;
                B3SAlertScanFairViewController *vc = [B3SAlertScanFairViewController new];
                vc.cancelBlock = ^{
                    @strongify(self);
                    self.ignoreHair = YES;
                };
                vc.complete = ^{
                    @strongify(self);
                    [self.navigationController popViewControllerAnimated:YES];
                };
                [self presentViewController:vc animated:NO completion:NULL];
            }
            
            // 矩形框等比例缩小, 用于在personLayer展示人脸框, personLayer固定高度为500
            CGRect previewRect=[self.scanManager getCameraPreviewLayerFrameRect];
            float scaleY = previewRect.size.height/ faceInfo.imgHeight;
            float scaleX = previewRect.size.width/ faceInfo.imgWidth;
            // 人脸框往上平移
            float faceRectY = faceInfo.rect.origin.y * scaleY;
            float faceRectX = faceInfo.rect.origin.x * scaleX;
            float faceRectHeight = faceInfo.rect.size.height * scaleY;
            float faceRectWidth = faceInfo.rect.size.width * scaleX;
            
            // 缩小人脸区域、增强用户体验
            faceRectX *= 1.1;
            faceRectY *= 1.1;
            faceRectHeight *= 0.9;
            faceRectWidth *= 0.9;
            
            CGRect rect = CGRectMake(faceRectX, faceRectY, faceRectWidth, faceRectHeight);
            
            if (!CGRectContainsRect(faceRectView.frame, rect) && !self.isScanning){
                faceRectView.highlighted = YES;
                NSString *text = @"请对准圆形框";
                NSString *key = @"PleaseHeadCenter";
                [wself speak:text key:key];
            }

            if (faceRectView.highlighted == NO && !self.isScanning) { // 非扫描中 成功聚焦
                wself.gk_navTitle = NSLocalizedString(@"点击底部按钮开始扫描", nil);
            }
            
        });
    }];
}

#pragma mark - B3SSCanPageViewControllerDataSource
- (UIViewController *)previewController:(NSInteger)index{
    return self.childsVc[index];
}

- (NSInteger)numbersOfPage{
    return self.childsVc.count;
}



#pragma mark - Action
/// 模式切换点击事件
- (IBAction)itemAction:(UIButton *)sender {
    
    if(self.isScanning == YES){ // 扫描中禁止切换扫描模式
        return;
    }
    
    NSInteger index = sender.tag;
    [self.pageViewController selectChildPage:index];
}

/// 扫描教程
- (IBAction)helpAction:(UIButton *)sender {
    
    B3SScanGuidesViewController *vc = [B3SScanGuidesViewController new];
    vc.scanType = self.index == 1 ? B3SScanTypeLookup : B3SScanTypeDefault;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)turnBack:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Method
// 停止扫描，进入后处理(postprocessing)，监听后处理进度
- (void)finishCamera
{
    // 关闭摄像头
    [self closeCameraAndStopStream];
    // 停止扫描，进入后处理
    [self.scanManager stopScan];
    //监听后处理的进度值
    [self finishScan];
    //绘制转圈圈动画
    __weak typeof(self) wself = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [wself finishHandling];
    });
}
//监听genModel的进度值
- (void)finishScan
{
    const float P = 0.8;
    self.genModelProgressDisplay = 0;
    __weak typeof(self) wself = self;
    [[RACSignal interval:0.2 onScheduler:[RACScheduler mainThreadScheduler]] subscribeNext:^(id x) {
        wself.finishLabel.text = @"图像正在处理中";
        if (wself.scanManager.genModelProgress == 1) {
            wself.genModelProgressDisplay = 1;
        } else {
            wself.genModelProgressDisplay = P * wself.genModelProgressDisplay + (1.0 - P) * wself.scanManager.genModelProgress;
        }
        wself.finishLabel.text = [NSString stringWithFormat:@"%@ %.0f%%",wself.finishLabel.text, wself.genModelProgressDisplay * 100.f];
    }];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        wself.finishView.hidden = NO;
        
        UIImage *image = [UIImage imageNamed:@"icon_scan_finish" inBundle:[NSBundle bundleForClass:self.class] compatibleWithTraitCollection:nil];
        wself.finishImageView.image = image;
        wself.finishLabel.text = @"扫描结束";
        wself.gk_navTitle = @"";
    });
}
//绘制转圈圈的进度动画
- (void)finishHandling
{
    __weak typeof(self) wself = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        wself.finishView.hidden = NO;
        wself.finishImageView.image = [UIImage imageNamed:@"icon_scanning_arrow"];;
        wself.finishLabel.text = @"图像正在处理中";
        wself.finishUpImageView.image = [UIImage imageNamed:@"icon_scaning_face"];
        //wself.indicateView.hidden = YES;
    });
    
    NSDate *now = [NSDate date];
    [[[RACSignal interval:0.5 onScheduler:[RACScheduler mainThreadScheduler]]
      map:^id(NSDate *date) {
          return @(date.timeIntervalSince1970 - now.timeIntervalSince1970);
      }] subscribeNext:^(NSNumber *second) {
          CGAffineTransform transform = CGAffineTransformMakeRotation(second.integerValue % 2 * M_PI);
          [UIView animateWithDuration:0.5
                                delay:0.0f
                              options:UIViewAnimationOptionCurveLinear
                           animations:^{
                               wself.finishImageView.transform = transform;
                           } completion:^(BOOL isFinish){
                           }];
      }];
}

- (void)finishScanWithOutHandling
{
    __weak typeof(self) wself = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        wself.finishView.hidden = NO;
        
        UIImage *image = [UIImage imageNamed:@"icon_scan_finish" inBundle:[NSBundle bundleForClass:self.class
                                                                           ] compatibleWithTraitCollection:nil];
        wself.finishImageView.image = image;
        wself.finishLabel.text = @"扫描结束";
    });
}

/// 切换按钮选择样式
- (void)onSelected:(NSInteger)tag{
    
    NSLog(@"tag %ld", tag);
    for (UIButton *btn in self.buttons) {
        btn.selected = btn.tag == tag ? YES : NO;
    }
}

- (void)speak:(NSString *)text{
    
    self.gk_navTitle = NSLocalizedString(text, nil);
    self.speechLabel.text = text;
    [[B3SSpeechService sharedInstance] speak:self.speechLabel.text force:YES]; // 注意中英文语言适配
}

- (void)speak:(NSString *)text key:(NSString *)key{
    
    self.gk_navTitle = NSLocalizedString(text, nil);
    
    if (![self.scanFlags objectForKey:key]) {
        self.speechLabel.text = text;
        if (![[B3SSpeechService sharedInstance] speak:self.speechLabel.text]) { // 注意中英文语言适配
            [self.scanFlags setObject:@YES forKeyedSubscript:key];
        }
    }
}

/// 触感记录
- (void)impactFlag:(NSInteger)value{
    
    NSString *key = [NSString stringWithFormat:@"%ld", value];
    if (![self.impactFlags objectForKey:key]) {
        [self.impactFlags setObject:@YES forKeyedSubscript:key];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            // 触感
            [self impactFeedback];
        });
    }
}
// 开始扫描过程中的播放语音提示
- (void)startGuideVoiceWithComplete:(dispatch_block_t)complete
{
    self.gk_navTitle = NSLocalizedString(@"开始扫描", nil);
    self.speechLabel.text = self.gk_navTitle;
    [[B3SSpeechService sharedInstance] speak:self.speechLabel.text];
    
    @weakify(self);
    __weak typeof(self) wself = self;
    NSDate *now = [NSDate date];

    // 开始扫描回调: 调用scanManager.startScan
    if (complete) {
        complete();
    }

    //启用一个RAC定时器
    //RACSignal *rac_viewWillDisappear = [self rac_signalForSelector:@selector(viewWillDisappear:)];
    
    /// rac_finish观察结果为YES则暂停定时器
    RACSignal *rac_finish = [RACObserve(self.finishView, hidden) filter:^BOOL(NSNumber *value) {
        //NSNumber *result = [[NSNumber alloc] initWithBool:!value.boolValue];
        //NSLog(@"result %@", result);
        return !value.boolValue;
    }];
    
    //组合信号
    //RACSignal *rac_mergeSignal = [rac_finish merge:rac_viewWillDisappear];
    
    // 高亮
    CGFloat brightness = [UIScreen mainScreen].brightness;
    [UIScreen mainScreen].brightness = 1.0f;
    
    __block float turnLeft = 9999.0; // 模型到达最左时间
    __block float turnRight = 9999.0; // 模型到达最右时间
    __block float turnUp = 9999.0; // 模型到达最上时间
    
    // 语音提示定时器
    [[[[RACSignal interval:0.5 onScheduler:[RACScheduler mainThreadScheduler]]
       takeUntil:rac_finish] map:^id(NSDate *date) {
        return @(date.timeIntervalSince1970 - now.timeIntervalSince1970);
    }] subscribeNext:^(NSNumber *sec) {
        @strongify(self)
        
        float x = [[NSString stringWithFormat:@"%.1f", sec.floatValue] floatValue];
        //NSLog(@"sec: %f", x);
        if (self.index == 0) {
            /*
            switch (sec.integerValue) {
                case 1: // 准备开始
                {
                    [wself speak:@"向左慢慢转头"];
                    self.toolBar.hidden = YES;
                    self.renderView.hidden = YES;
                    wself.previewView.hidden = NO;
                }
                    break;
                case 2:
                {
                    wself.rotateToY = -1;
                    //[wself speak:@"向左慢慢转头"];
                    //self.toolBar.hidden = YES;
                    //self.renderView.hidden = YES;
                    //wself.previewView.hidden = NO;
                }
                    break;
                case 4:
                {
                    [wself speak:@"转回中间"];
                    wself.rotateToY = 0;
                }
                    break;
                case 6:
                {
                    [wself speak:@"向右慢慢转头"];
                    wself.rotateToY = 1;
                }
                    break;
                case 8:
                {
                    [wself speak:@"转回中间"];
                    wself.rotateToY = 0;
                }
                    break;
                case 11:
                {
                    wself.finishView.hidden = NO;
                    [wself finishCamera];
                }
                    break;
                default:
                    break;
            }*/
            
            if (x == 1.0) { // 1s后准备开始
                
                [wself speak:@"向左慢慢转头"];
                self.toolBar.hidden = YES;
                self.renderView.hidden = YES;
                wself.previewView.hidden = NO;
            } else if (x == 2.0) {
                
                wself.rotateToY = -1;
            } else if (x == 2.0 + ScanInterval) { // 最左边
                
                turnLeft = x;
                [wself speak:@"转回中间"];
                wself.rotateToY = 0;
            } else if (x == turnLeft + ScanInterval) {
                
                [wself speak:@"向右慢慢转头"];
                wself.rotateToY = 1;
             } else if (x == turnLeft + ScanInterval * 2) { // 最右边
                 
                 turnRight = x;
                 [wself speak:@"转回中间"];
                 wself.rotateToY = 0;
             } else if (x >= turnRight + ScanInterval + 1.0) {
                 
                 NSLog(@" 结束啦！ sec: %f", x);
                  wself.finishView.hidden = NO;
                  [wself finishCamera];
            }
            
        }else{
            /*
            switch (sec.integerValue) {
                case 1:
                {
                    [wself speak:@"向上慢慢抬头"];
                    self.toolBar.hidden = YES;
                    self.renderView.hidden = YES;
                    wself.previewView.hidden = NO;
                }
                    break;
                case 2:
                {
                    wself.rotateToX = -0.5;
                    //[wself speak:@"向上慢慢抬头"];
                    //self.toolBar.hidden = YES;
                    //self.renderView.hidden = YES;
                    //wself.previewView.hidden = NO;
                }
                    break;
                case 4:
                {
                    [wself speak:@"转回中间"];
                    wself.rotateToX = 0;
                }
                    break;
                case 6:
                {
                    [wself speak:@"向左慢慢转头"];
                    wself.rotateToY = -1;
                }
                    break;
                case 8:
                {
                    [wself speak:@"转回中间"];
                    wself.rotateToY = 0;
                }
                    break;
                case 10:
                {
                    [wself speak:@"向右慢慢转头"];
                    wself.rotateToY = 1;
                }
                    break;
                case 12:
                {
                    [wself speak:@"转回中间"];
                    wself.rotateToY = 0;
                }
                    break;
                case 15:
                {
                    wself.finishView.hidden = NO;
                    [wself finishCamera];
                }
                    break;
                default:
                    break;
            }*/
            
            if (x == 1.0) { // 1s后准备开始
                
                [wself speak:@"向上慢慢抬头"];
                self.toolBar.hidden = YES;
                self.renderView.hidden = YES;
                wself.previewView.hidden = NO;
            } else if (x == 2.0) { // 模型向上旋转
                
                wself.rotateToX = -0.5;
            } else if (x == 2.0 + ScanUpInterval) { // 最上边
                
                turnUp = x;
                [wself speak:@"转回中间"];
                wself.rotateToX = 0;
            } else if (x == turnUp + ScanUpInterval) {
                
                [wself speak:@"向左慢慢转头"];
                wself.rotateToY = -1;
            } else if (x == turnUp + ScanUpInterval + ScanInterval) { // 最左边
                
                turnLeft = x;
                [wself speak:@"转回中间"];
                wself.rotateToY = 0;
            } else if (x == turnLeft + ScanInterval) {
                
                [wself speak:@"向右慢慢转头"];
                wself.rotateToY = 1;
            } else if (x == turnLeft + ScanInterval * 2) { // 最右边
                
                turnRight = x;
                [wself speak:@"转回中间"];
                wself.rotateToY = 0;
            } else if (x >= turnRight + ScanInterval + 1.0) {
                
                NSLog(@" 结束啦！ sec: %f", x);
                wself.finishView.hidden = NO;
                [wself finishCamera];
            }
        }
    } completed:^{
        NSLog(@"语音提示定时器销毁");
        [UIScreen mainScreen].brightness = brightness;
    }];
}

@end
