//
//  B3SScanGuidesViewController.h
//  body3dscale
//
//  Created on 2018/8/8.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <UIKit/UIKit.h>


typedef enum : NSUInteger {
    B3SScanTypeDefault = 0, // 默认水平扫描人头模型
    B3SScanTypeLookup = 1, // 包含抬头扫描人头模型
} B3SScanType;

@interface B3SScanGuidesViewController : UITabBarController

@property(nonatomic, assign) B3SScanType scanType;

@end
