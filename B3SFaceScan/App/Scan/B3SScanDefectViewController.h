//
//  B3SScanDefectViewController.h
//  body3dscale
//
//  Created on 2018/9/7.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <UIKit/UIKit.h>

@interface B3SScanDefectViewController : B3SBaseViewController

@property (nonatomic, copy) NSString *message;

@property (nonatomic, copy) dispatch_block_t viewBlock;

@end
