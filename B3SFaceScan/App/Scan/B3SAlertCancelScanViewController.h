//
//  B3SAlertCancelScanViewController.h
//  body3dscale
//
//  Created on 2018/7/11.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SAlertController.h"

@interface B3SAlertCancelScanViewController : B3SAlertController

@property (nonatomic, copy) dispatch_block_t completeBlock;

@property (nonatomic, copy) dispatch_block_t reasonBlock;

@end
