//
//  B3SScanGuide2ViewController.m
//  body3dscale
//
//  Created on 2018/8/8.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SScanGuide2ViewController.h"
#import <ReactiveCocoa.h>

@interface B3SScanGuide2ViewController ()

@property (weak, nonatomic) IBOutlet UIButton *doneButton;

@end

@implementation B3SScanGuide2ViewController

+ (instancetype)new
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Scan" bundle:nil];
    return [sb instantiateViewControllerWithIdentifier:NSStringFromClass(self)];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    @weakify(self);
    self.doneButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        @strongify(self);
        if (self.tabBarController.selectedIndex < (self.tabBarController.viewControllers.count - 1)) {
            self.tabBarController.selectedIndex += 1;
        }
        return [RACSignal empty];
    }];
    
    self.doneButton.layer.cornerRadius = CGRectGetHeight(self.doneButton.frame)/2.f;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
