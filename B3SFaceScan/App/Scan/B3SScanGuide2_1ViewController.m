//
//  B3SScanGuide2_1ViewController.m
//  body3dscale
//
//  Created on 2019/5/8.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SScanGuide2_1ViewController.h"
#import <ReactiveCocoa.h>

@interface B3SScanGuide2_1ViewController ()

@property (weak, nonatomic) IBOutlet UIButton *doneButton;

@end

@implementation B3SScanGuide2_1ViewController

+ (instancetype)new
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Scan" bundle:nil];
    return [sb instantiateViewControllerWithIdentifier:NSStringFromClass(self)];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    @weakify(self);
    self.doneButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        @strongify(self);
        
        [self.navigationController popViewControllerAnimated:YES];
        return [RACSignal empty];
    }];
    
    self.doneButton.layer.cornerRadius = CGRectGetHeight(self.doneButton.frame)/2.f;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
