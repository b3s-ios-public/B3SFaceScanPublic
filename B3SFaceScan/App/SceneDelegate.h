//
//  SceneDelegate.h
//  B3SFaceScan
//
//  Created on 2020/7/24.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

