//
//  B3SSpeechService.h
//  body3dscale
//
//  Created on 2018/8/15.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <Foundation/Foundation.h>

@interface B3SSpeechService : NSObject

+ (instancetype)sharedInstance;

- (BOOL)speak:(NSString *)speech;

- (BOOL)speak:(NSString *)speech force:(BOOL)isForce;

- (BOOL)stop;

@end
