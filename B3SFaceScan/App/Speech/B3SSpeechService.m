//
//  B3SSpeechService.m
//  body3dscale
//
//  Created on 2018/8/15.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SSpeechService.h"
#import <AVFoundation/AVFoundation.h>

@interface B3SSpeechService ()<AVSpeechSynthesizerDelegate>

@property (nonatomic, strong) AVSpeechSynthesizer *speecher;

@end

@implementation B3SSpeechService

+ (instancetype)sharedInstance
{
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.speecher = [[AVSpeechSynthesizer alloc] init];
        self.speecher.delegate = self;
        
    }
    return self;
}

/*
 语言种类 : Language
 
 ar-SA  沙特阿拉伯（阿拉伯文）
 
 en-ZA, 南非（英文）
 
 nl-BE, 比利时（荷兰文）
 
 en-AU, 澳大利亚（英文）
 
 th-TH, 泰国（泰文）
 
 de-DE, 德国（德文）
 
 en-US, 美国（英文）
 
 pt-BR, 巴西（葡萄牙文）
 
 pl-PL, 波兰（波兰文）
 
 en-IE, 爱尔兰（英文）
 
 el-GR, 希腊（希腊文）
 
 id-ID, 印度尼西亚（印度尼西亚文）
 
 sv-SE, 瑞典（瑞典文）
 
 tr-TR, 土耳其（土耳其文）
 
 pt-PT, 葡萄牙（葡萄牙文）
 
 ja-JP, 日本（日文）
 
 ko-KR, 南朝鲜（朝鲜文）
 
 hu-HU, 匈牙利（匈牙利文）
 
 cs-CZ, 捷克共和国（捷克文）
 
 da-DK, 丹麦（丹麦文）
 
 es-MX, 墨西哥（西班牙文）
 
 fr-CA, 加拿大（法文）
 
 nl-NL, 荷兰（荷兰文）
 
 fi-FI, 芬兰（芬兰文）
 
 es-ES, 西班牙（西班牙文）
 
 it-IT, 意大利（意大利文）
 
 he-IL, 以色列（希伯莱文，阿拉伯文）
 
 no-NO, 挪威（挪威文）
 
 ro-RO, 罗马尼亚（罗马尼亚文）
 
 zh-HK, 香港（中文）
 
 zh-TW, 台湾（中文）
 
 sk-SK, 斯洛伐克（斯洛伐克文）
 
 zh-CN, 中国（中文）
 
 ru-RU, 俄罗斯（俄文）
 
 en-GB, 英国（英文）
 
 fr-FR, 法国（法文）
 
 hi-IN  印度（印度文）
 */

- (BOOL)speak:(NSString *)speech
{
    return [self speak:speech force:NO];
}

- (BOOL)speak:(NSString *)speech force:(BOOL)isForce
{
    NSArray *langArr = [[NSUserDefaults standardUserDefaults] valueForKey:@"AppleLanguages"];
    NSString *language = langArr.firstObject;
    NSString *shortLang = [language componentsSeparatedByString:@"-"].firstObject;
    
    if ([shortLang isEqualToString:@"zh"]) {
        language = @"zh_CN";
    } else {
        language = @"en-US";
    }
    
    return [self speak:speech language:language force:isForce];
}

- (BOOL)speak:(NSString *)speech language:(NSString *)language
{
    return [self speak:speech language:language force:NO];
}

- (BOOL)speak:(NSString *)speech language:(NSString *)language force:(BOOL)isForce
{
    if ([self.speecher isSpeaking] && !isForce) {
        
        NSLog(@"speak fail: %@ | language: %@ ",speech,language);
        return NO;
    }
    
    /** 语言设置 */
    AVSpeechSynthesisVoice *speechVoice = [AVSpeechSynthesisVoice voiceWithLanguage:language];
    /** 发声设置 */
    AVSpeechUtterance *utterance = [AVSpeechUtterance speechUtteranceWithString:speech];
    utterance.voice = speechVoice;
    utterance.rate = 0.5;/** 语速 */
    utterance.volume = 10.0;/** 音量 */
    utterance.postUtteranceDelay = 0;/** 读完一段后停顿 */
    
    [self.speecher speakUtterance:utterance];
    
    NSLog(@"speak success: %@ | language: %@ ",speech,language);
    return YES;
}


- (BOOL)stop{
  return [self.speecher stopSpeakingAtBoundary:AVSpeechBoundaryImmediate];
}

#pragma mark -  Delegate

- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer willSpeakRangeOfSpeechString:(NSRange)characterRange utterance:(AVSpeechUtterance *)utterance
{
//    NSLog(@"location == %lu  length == %lu",(unsigned long)characterRange.location,(unsigned long)characterRange.length);
    NSLog(@"speech string == %@",[utterance.speechString substringWithRange:characterRange]);
}

@end
