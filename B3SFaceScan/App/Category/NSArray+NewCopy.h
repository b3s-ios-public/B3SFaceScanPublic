//
//  NSArray+DeepCopy.h
//  body3dscale
//
//  Created on 2018/5/23.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (DeepCopy)

- (NSMutableArray *)newCopy;

@end
