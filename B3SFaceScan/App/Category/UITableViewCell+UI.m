//
//  UITableViewCell+UI.m
//  body3dscale
//
//  Created on 02/04/2018.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "UITableViewCell+UI.h"

@implementation UITableViewCell (UI)

+ (NSString *)identifier
{
    return NSStringFromClass(self);
}

@end
