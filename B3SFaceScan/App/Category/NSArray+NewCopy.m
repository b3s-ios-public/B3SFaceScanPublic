//
//  NSArray+DeepCopy.m
//  body3dscale
//
//  Created on 2018/5/23.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "NSArray+NewCopy.h"

@implementation NSArray (DeepCopy)

- (NSMutableArray *)newCopy
{
    NSMutableArray *returnArray = [[NSMutableArray alloc] initWithCapacity:self.count];
    
    for(id oneValue in self) {
        id oneCopy = nil;
        
        if([oneValue respondsToSelector:@selector(newCopy)]) {
            oneCopy = [oneValue newCopy];
        } else if([oneValue conformsToProtocol:@protocol(NSMutableCopying)]) {
            oneCopy = [oneValue mutableCopy];
        } else if([oneValue conformsToProtocol:@protocol(NSCopying)]){
            oneCopy = [oneValue copy];
        } else {
            oneCopy = oneValue;
        }
        
        [returnArray addObject:oneCopy];
    }
    
    return [returnArray copy];
}

@end
