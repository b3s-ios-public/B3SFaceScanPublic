//
//  SCNNode+Teeth.m
//  body3dscale
//
//  Created on 2019/4/17.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "SCNNode+Teeth.h"
#import <SceneKit/ModelIO.h>

@implementation SCNNode (Teeth)

/**
 通过MDLObject模型直接创建SCNNode节点
 
 @param mesh 模型名
 @param diffuseName 纹理图片名
 @return 节点
 */
+ (SCNNode *)nodeWithMesh:(NSString *)mesh diffuse:(NSString *)diffuse {
    
    // SCNNode通过MDLObject模型直接创建节点
    NSString *path0 = [[NSBundle mainBundle] pathForResource:mesh ofType:nil];
    NSURL *url0 = [NSURL URLWithString: path0];
    MDLAsset *asset0 = [[MDLAsset alloc] initWithURL: url0];
    //[mdlAsset loadTextures];
    
    // 通过nodeWithMDLObject
    SCNNode *node = [SCNNode nodeWithMDLObject: [asset0 objectAtIndex:0]];
    //node.position = SCNVector3Make(0, 0, 20);
    //node.scale = SCNVector3Make(0.1, 0.1, 0.1);
    //node.transform = SCNMatrix4MakeScale(0.25, 0.25, 0.25);
    //node.eulerAngles = SCNVector3Make(-M_PI_2, 0, 0); // 欧拉角 绕X轴逆时针旋转90度
    
    node.geometry.firstMaterial.lightingModelName = SCNLightingModelPhysicallyBased;
    node.geometry.firstMaterial.diffuse.contents = [UIImage imageNamed: diffuse];
    //node.geometry.firstMaterial.metalness.contents = [UIColor colorWithWhite:0.1f alpha:1.f];
    //node.geometry.firstMaterial.roughness.contents = [UIColor colorWithWhite:0.5f alpha:1.f];
    return node;
}

/**
 通过模型目录创建SCNNode节点
 
 @param url 模型目录URL
 @return 节点
 */
+ (SCNNode *)nodeWithDir:(NSURL *)url{
    
    // SCNNode通过MDLObject模型直接创建节点
    MDLAsset *asset = [[MDLAsset alloc] initWithURL:[url URLByAppendingPathComponent:@"mesh.obj"]];
//    NSData *data = [NSData dataWithContentsOfURL:[url URLByAppendingPathComponent:@"mesh.png"]];
//    UIImage *image = [UIImage imageWithData:data];
    
    NSString *path = [url.path stringByAppendingString:@"/mesh.png"];
    UIImage *image = [UIImage imageWithContentsOfFile: path];
    
    // 通过nodeWithMDLObject
    SCNNode *node = [SCNNode nodeWithMDLObject: [asset objectAtIndex:0]];
    node.geometry.firstMaterial.lightingModelName = SCNLightingModelPhysicallyBased;
    node.geometry.firstMaterial.diffuse.contents = image;
    return node;
}


@end
