//
//  NSDictionary+JSON.h
//  body3dscale
//
//  Created on 2018/8/29.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (JSON)

+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;

- (NSString *)toJsonStr;

@end
