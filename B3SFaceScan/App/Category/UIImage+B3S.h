//
//  UIImage+B3S.h
//  body3dscale
//
//  Created on 2018/6/11.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (B3S)

- (UIImage *)imageByApplyingAlpha:(CGFloat) alpha;

- (UIImage *)compressImageToByte:(NSUInteger)maxLength;

/**
 创建纯颜色图片

 @param color 颜色
 @return UIImage对象
 */
- (instancetype)b3s_initWithColor:(UIColor *)color;

@end
