//
//  UI_Device+InterfaceOrientation.h
//  body3dscale
//
//  Created on 2019/4/23.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIDevice (InterfaceOrientation)

/**
 进入界面屏幕自动设置屏幕方向
 
 @param interfaceOrientation 屏幕方向. UIInterfaceOrientationLandscapeRight横屏
 */
+ (void)setOrientation:(UIInterfaceOrientation)interfaceOrientation;
    
@end

NS_ASSUME_NONNULL_END
