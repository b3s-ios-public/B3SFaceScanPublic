//
//  UIImageView+B3S.m
//  body3dscale
//
//  Created on 2018/5/2.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "UIImageView+B3S.h"
#import <YYKit.h>

@implementation UIImageView (B3S)


- (void)b3s_setImageURL:(NSURL *)imageURL
{
    [self cancelCurrentImageRequest];
    
    static UIImage *defaultImage = nil;
    
    if (!defaultImage) {
        defaultImage = [UIImage imageNamed:@"icon_default_image"];
        defaultImage = [defaultImage imageByResizeToSize:self.frame.size contentMode:UIViewContentModeScaleAspectFit];
    }
    
    UIColor *lastBackgroundColor = self.backgroundColor;
    
    
    self.backgroundColor = [UIColor colorWithHexString:@"f5f5f5"];
    
    UIViewContentMode lastMode = self.contentMode;
    
    self.contentMode = UIViewContentModeCenter;
    
    __weak typeof(self) wself = self;
    [self setImageWithURL:imageURL placeholder:defaultImage
                  options:YYWebImageOptionShowNetworkActivity | YYWebImageOptionSetImageWithFadeAnimation
               completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
        if (stage == YYWebImageStageFinished && image) {
            wself.contentMode = lastMode;
            wself.backgroundColor = lastBackgroundColor;
//            wself.image = [image imageByCropToRect:CGRectMake(image.size.width/2.f - CGRectGetWidth(wself.frame)/2.f, image.size.height/2.f - CGRectGetHeight(wself.frame)/2.f, CGRectGetWidth(wself.frame), CGRectGetHeight(wself.frame))];
        }
    }];
}

- (void)b3s_setImageStr:(NSString *)imageURLStr
{
    [self b3s_setImageURL:[NSURL URLWithString:imageURLStr]];
}

@end
