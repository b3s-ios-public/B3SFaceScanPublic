//
//  B3SSceneKitHelper.h
//  body3dscale
//
//  Created on 02/04/2018.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SceneKit/SceneKit.h>

@interface B3SSceneKitHelper : NSObject

+ (void)printSCNMatrix4:(SCNMatrix4)m;

+ (void)printSCNVector3:(SCNVector3)v;

@end
