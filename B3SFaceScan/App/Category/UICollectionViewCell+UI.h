//
//  UICollectionViewCell+UI.h
//  body3dscale
//
//  Created on 31/03/2018.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICollectionViewCell (UI)

+ (NSString *)identifier;

@end
