//
//  UI_Device+InterfaceOrientation.m
//  body3dscale
//
//  Created on 2019/4/23.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "UIDevice+InterfaceOrientation.h"

@implementation UIDevice (InterfaceOrientation)

/**
 进入界面屏幕自动设置屏幕方向
 
 @param interfaceOrientation 屏幕方向. UIInterfaceOrientationLandscapeRight横屏
 */
+ (void)setOrientation:(UIInterfaceOrientation)interfaceOrientation{
    
    if([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)]) {
        SEL selector = NSSelectorFromString(@"setOrientation:");
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDevice instanceMethodSignatureForSelector:selector]];
        [invocation setSelector:selector];
        [invocation setTarget:[UIDevice currentDevice]];
        int val = interfaceOrientation; // 屏幕方向
        [invocation setArgument:&val atIndex:2];
        [invocation invoke];
    }
}

@end
