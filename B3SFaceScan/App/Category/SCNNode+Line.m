//
//  SCNNode+Line.m
//  body3dscale
//
//  Created on 2018/4/26.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "SCNNode+Line.h"

@implementation SCNNode (Line)

+ (SCNVector3)normalizeVector:(SCNVector3) iv
{
    float length = sqrt(iv.x * iv.x + iv.y * iv.y + iv.z * iv.z);
    if (length == 0) {
        return SCNVector3Make(0.0, 0.0, 0.0);
    }
    
    return SCNVector3Make( iv.x / length, iv.y / length, iv.z / length);
}

+ (SCNNode *)buildLineInTwoPointsWithRotation:(SCNVector3)startPoint
                                     endPoint:(SCNVector3)endPoint
                                       radius:(CGFloat)radius
                                        color:(UIColor *)color
                                   lengthRate:(float)lr
{
    SCNNode *node = [SCNNode node];
    SCNVector3 w = SCNVector3Make(endPoint.x-startPoint.x,
                                  endPoint.y-startPoint.y,
                                  endPoint.z-startPoint.z);
    CGFloat l = sqrt(w.x * w.x + w.y * w.y + w.z * w.z);
    
    if (l == 0.0) {
        // two points together.
        SCNSphere *sphere = [SCNSphere sphereWithRadius:radius];
        sphere.firstMaterial.diffuse.contents = color;
        node.geometry = sphere;
        node.position = startPoint;
        return node;
    }
    
    SCNCylinder *cyl = [SCNCylinder cylinderWithRadius:radius height:l*lr];
    cyl.firstMaterial.diffuse.contents = color;
    node.geometry = cyl;
    
    //original vector of cylinder above 0,0,0
    SCNVector3 ov = SCNVector3Make(0, l/2.0,0);
    //target vector, in new coordination
    SCNVector3 nv = SCNVector3Make((endPoint.x - startPoint.x)/2.0, (endPoint.y - startPoint.y)/2.0,
                                   (endPoint.z-startPoint.z)/2.0);
    
    // axis between two vector
    SCNVector3 av = SCNVector3Make( (ov.x + nv.x)/2.0, (ov.y+nv.y)/2.0, (ov.z+nv.z)/2.0);
    
    //normalized axis vector
    SCNVector3 av_normalized = [self normalizeVector:av];
    float q0 = 0.0; //cos(angel/2), angle is always 180 or M_PI
    float q1 = av_normalized.x; // x' * sin(angle/2)
    float q2 = av_normalized.y; // y' * sin(angle/2)
    float q3 = av_normalized.z; // z' * sin(angle/2)
    
    float r_m11 = q0 * q0 + q1 * q1 - q2 * q2 - q3 * q3;
    float r_m12 = 2 * q1 * q2 + 2 * q0 * q3;
    float r_m13 = 2 * q1 * q3 - 2 * q0 * q2;
    float r_m21 = 2 * q1 * q2 - 2 * q0 * q3;
    float r_m22 = q0 * q0 - q1 * q1 + q2 * q2 - q3 * q3;
    float r_m23 = 2 * q2 * q3 + 2 * q0 * q1;
    float r_m31 = 2 * q1 * q3 + 2 * q0 * q2;
    float r_m32 = 2 * q2 * q3 - 2 * q0 * q1;
    float r_m33 = q0 * q0 - q1 * q1 - q2 * q2 + q3 * q3;
    
    SCNMatrix4 transform;
    transform.m11 = r_m11;
    transform.m12 = r_m12;
    transform.m13 = r_m13;
    transform.m14 = 0.0;
    
    transform.m21 = r_m21;
    transform.m22 = r_m22;
    transform.m23 = r_m23;
    transform.m24 = 0.0;
    
    transform.m31 = r_m31;
    transform.m32 = r_m32;
    transform.m33 = r_m33;
    transform.m34 = 0.0;
    
    transform.m41 = (startPoint.x + endPoint.x) / 2.0;
    transform.m42 = (startPoint.y + endPoint.y) / 2.0;
    transform.m43 = (startPoint.z + endPoint.z) / 2.0;
    transform.m44 = 1.0;
    
    node.transform = transform;
    return node;
}


@end
