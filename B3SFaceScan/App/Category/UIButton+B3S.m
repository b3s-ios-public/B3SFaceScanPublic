//
//  UIButton+B3S.m
//  body3dscale
//
//  Created on 2018/5/9.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "UIButton+B3S.h"
#import <YYKit.h>
#import <Aspects.h>
#import <ReactiveCocoa.h>

@implementation UIButton (B3S)

- (void)setupImage:(UIImage *)image text:(NSString *)text normalCoror:(UIColor *)normalColor
{
    UIImage *normalImage = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImage *selectedImage = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImage *disableImage = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    if (!normalColor) {
        normalColor = [UIColor colorWithHexString:@"3b3b3b"];
    }
    
    UIColor *focusColor = [UIColor colorWithHexString:@"20cfa0"];
    UIColor *disableColor = [UIColor colorWithHexString:@"d9d9d9"];
    
    if (image) {
        UIGraphicsBeginImageContextWithOptions(image.size, NO, normalImage.scale);
        [normalColor set];
        [normalImage drawInRect:CGRectMake(0, 0, image.size.width, normalImage.size.height)];
        normalImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        
        UIGraphicsBeginImageContextWithOptions(image.size, NO, selectedImage.scale);
        [focusColor set];
        [selectedImage drawInRect:CGRectMake(0, 0, image.size.width, selectedImage.size.height)];
        selectedImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        
        UIGraphicsBeginImageContextWithOptions(image.size, NO, disableImage.scale);
        [disableColor set];
        [disableImage drawInRect:CGRectMake(0, 0, image.size.width, disableImage.size.height)];
        disableImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        ;
        [self setImage:normalImage forState:UIControlStateNormal];
        [self setImage:selectedImage forState:UIControlStateSelected];
        [self setImage:selectedImage forState:UIControlStateHighlighted];
        [self setImage:disableImage forState:UIControlStateDisabled];
    }
    
    UIFont *font = [UIFont systemFontOfSize:11.f];
    [self.titleLabel setFont:font];
    [self setTitle:text forState: UIControlStateNormal];
    [self setTitleColor:normalColor forState:UIControlStateNormal];
    [self setTitleColor:focusColor forState:UIControlStateSelected];
    [self setTitleColor:focusColor forState:UIControlStateHighlighted];
    
    if (!text) {
        return;
    }
    
    //按钮图片和标题总高度
    CGFloat totalHeight = (self.imageView.frame.size.height + self.titleLabel.frame.size.height);
    // 图片和标题相差距离
    CGFloat spaceHeight = 20;
    
    CGFloat textWidth = [text heightForFont:font width:CGFLOAT_MAX];
    
    //设置按钮图片居中
    [self setImageEdgeInsets:UIEdgeInsetsMake(-(totalHeight - self.imageView.frame.size.height),
                                              CGRectGetWidth(self.frame)/2.f - self.imageView.center.x,
                                              0.0,
                                              -textWidth)];
    //设置按钮标题居中
    [self setTitleEdgeInsets:UIEdgeInsetsMake(spaceHeight,
                                              -self.imageView.frame.size.width,
                                              -(totalHeight - self.titleLabel.frame.size.height),
                                              0.f
                                              )];
    
    
    @weakify(self);
    void(^setupCenter)(void) = ^(void) {
        @strongify(self);
        self.imageView.center = CGPointMake(CGRectGetWidth(self.frame)/2, self.imageView.center.y);
        self.titleLabel.center = CGPointMake(self.imageView.center.x, self.titleLabel.center.y);
    };
    
    
    NSError *error = nil;
    [self aspect_hookSelector:@selector(layoutSubviews) withOptions:AspectPositionAfter usingBlock:^(void) {
        
        
        setupCenter();
    } error:&error];
    
    NSAssert(!error, @"%@ hook error ",NSStringFromClass(self.class));
    
    setupCenter();
}

- (void)setupImage:(UIImage *)image text:(NSString *)text
{
    [self setupImage:image text:text normalCoror:nil];
}

- (void)setHomeTitleCenter
{
    UIFont *font = [UIFont systemFontOfSize:12.f];
    [self.titleLabel setFont:font];
    
    //按钮图片和标题总高度
    CGFloat totalHeight = (self.imageView.frame.size.height + self.titleLabel.frame.size.height);
    // 图片和标题相差距离
    CGFloat spaceHeight = 5;
    
    CGFloat textWidth = [self.titleLabel.text heightForFont:self.titleLabel.font width:CGFLOAT_MAX];
    
    //设置按钮图片居中
    [self setImageEdgeInsets:UIEdgeInsetsMake(-(totalHeight - self.imageView.frame.size.height),
                                              CGRectGetWidth(self.frame)/2.f - self.imageView.center.x,
                                              0.0,
                                              -textWidth)];
    //设置按钮标题居中
    [self setTitleEdgeInsets:UIEdgeInsetsMake(spaceHeight,
                                              -self.imageView.frame.size.width,
                                              -(totalHeight - self.titleLabel.frame.size.height),
                                              0.f
                                              )];
    
    
    @weakify(self);
    void(^setupCenter)(void) = ^(void) {
        @strongify(self);
        self.imageView.center = CGPointMake(CGRectGetWidth(self.frame)/2, self.imageView.center.y);
        self.titleLabel.center = CGPointMake(self.imageView.center.x, self.titleLabel.center.y);
    };
    
    
    NSError *error = nil;
    [self aspect_hookSelector:@selector(layoutSubviews) withOptions:AspectPositionAfter usingBlock:^(void) {
        
        
        setupCenter();
    } error:&error];
    
    NSAssert(!error, @"%@ hook error ",NSStringFromClass(self.class));
    
    setupCenter();
}

- (void)b3s_normalButton
{
    
}


- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent*)event
{
    CGRect bounds = self.bounds;
    //若原热区小于44x44，则放大热区，否则保持原大小不变
    CGFloat widthDelta = MAX(44.0 - bounds.size.width, 0);
    CGFloat heightDelta = MAX(44.0 - bounds.size.height, 0);
    bounds = CGRectInset(bounds, -0.5 * widthDelta, -0.5 * heightDelta);
    return CGRectContainsPoint(bounds, point);
}

@end
