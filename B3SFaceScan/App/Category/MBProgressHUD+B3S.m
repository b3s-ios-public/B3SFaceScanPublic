//
//  MBProgressHUD+B3S.m
//  body3dscale
//
//  Created on 26/02/2018.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "MBProgressHUD+B3S.h"

@implementation MBProgressHUD (B3S)

+ (void)show
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [MBProgressHUD showHUDAddedTo:window animated:YES];
}

+ (void)hidden
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [MBProgressHUD hideHUDForView:window animated:YES];
}

+ (MBProgressHUD *)showToast:(NSString *)toast view:(UIView *)view
{
    if (view) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.label.text = toast;
        [hud hideAnimated:YES afterDelay:1];
        return hud;
    }
    
    
    NSLog(@"showToast : %@",toast);
    
    return nil;
}

+ (MBProgressHUD *)showToast:(NSString *)toast
{
    UIView *view = [UIApplication sharedApplication].keyWindow;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.label.text = toast;
    [hud hideAnimated:YES afterDelay:1];
    
    NSLog(@"showToast : %@",toast);
    return hud;
}

+ (MBProgressHUD *)showLoadingWithView:(UIView *)view
{
    if (view) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        return hud;
    } else {
        UIView *view = [UIApplication sharedApplication].keyWindow;
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        return hud;
    }
    return nil;
}

- (BOOL)willDealloc
{
    return NO;
}

@end
