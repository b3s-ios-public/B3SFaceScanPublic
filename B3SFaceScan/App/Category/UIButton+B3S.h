//
//  UIButton+B3S.h
//  body3dscale
//
//  Created on 2018/5/9.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (B3S)

- (void)setupImage:(UIImage *)image text:(NSString *)text;
- (void)setupImage:(UIImage *)image text:(NSString *)text normalCoror:(UIColor *)normalColor;

- (void)setHomeTitleCenter;

@end
