//
//  NSString+B3S.m
//  body3dscale
//
//  Created on 2018/9/14.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "NSString+B3S.h"

@implementation NSString (B3S)


- (NSString *)base64EncodedString;
{
    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
    return [data base64EncodedStringWithOptions:0];
}

- (NSString *)base64DecodedString
{
    NSData *data = [[NSData alloc] initWithBase64EncodedString:self options:0];
    NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return str;
}

@end
