//
//  UIViewController+UI.m
//  body3dscale
//
//  Created on 02/04/2018.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "UIViewController+UI.h"
#import <Aspects.h>
#import <YYKit.h>
#import "BlocksKit+UIKit.h"
#import <ReactiveCocoa.h>

@implementation UIViewController (UI)

+ (void)load
{
    NSError *error = nil;
    [UIViewController aspect_hookSelector:@selector(viewDidLoad) withOptions:AspectPositionBefore usingBlock:^(id<AspectInfo> aspects){
    } error:&error];
    
#ifdef DEBUG
    [UIViewController aspect_hookSelector:NSSelectorFromString(@"dealloc") withOptions:AspectPositionBefore usingBlock:^(id<AspectInfo> info) {
        NSLog(@"ViewController is about to be deallocated: %@", [info instance]);
    } error:&error];
#endif
    
    [UIViewController aspect_hookSelector:@selector(viewWillAppear:) withOptions:AspectPositionBefore usingBlock:^(id<AspectInfo> info, BOOL animated) {
        UIViewController *vc = info.instance;
        
        //默认恢复右滑返回
        vc.navigationController.interactivePopGestureRecognizer.enabled = YES;
        

        if ([vc.navigationController respondsToSelector:@selector(tintColor)]) {
            UIColor *tintColor = [vc.navigationController performSelector:@selector(tintColor)];
            [vc.navigationController.navigationBar setTintColor:tintColor];
        }
        
        
        [vc.navigationController.navigationBar setBackgroundColor:[UIColor colorWithHexString:@"3ad3aa"]];
        [vc.navigationController.navigationBar setBarTintColor:[UIColor colorWithHexString:@"3ad3aa"]];
        [vc.navigationController.navigationBar setTitleTextAttributes:@{ NSForegroundColorAttributeName: [UIColor whiteColor],NSFontAttributeName:[UIFont boldSystemFontOfSize:17] }];
        
        [vc.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
        [vc.navigationController.navigationBar setShadowImage:[UIImage new]];

        
        [vc.navigationController.navigationBar setHidden:NO];
        @weakify(vc);
        if (vc.navigationController.viewControllers.count > 1) {
            UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
            
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            
            [button setImage:[[UIImage imageNamed:@"button_back"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            
            if ([vc respondsToSelector:@selector(backTitle)]) {
                NSString *backTitle = [vc performSelector:@selector(backTitle)];
                [button setTitle:backTitle forState:UIControlStateNormal];
                button.titleLabel.font = [UIFont systemFontOfSize:18.f];
                [button setTitleEdgeInsets:UIEdgeInsetsMake(0, 17, 0, 0)];
                button.frame = CGRectMake(0, 0, 200, 60);
            } else {
                [button setTitle:@"" forState:UIControlStateNormal];
                button.frame = CGRectMake(0, 0, 32, 44);
            }
            
            if ([vc respondsToSelector:@selector(backColor)]) {
                UIColor *backColor = [vc performSelector:@selector(backColor)];
                [button setTitleColor:backColor forState:UIControlStateNormal];
                [button setTintColor:backColor];
            }
            
            button.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
                @strongify(vc);
                BOOL shouldPop = YES;;
                if([vc respondsToSelector:@selector(navigationShouldPopOnBackButton)]) {
                    shouldPop = [vc performSelector:@selector(navigationShouldPopOnBackButton)];
                }
                
                if (shouldPop) {
                    [vc.navigationController popViewControllerAnimated:YES];
                }
                return [RACSignal empty];
            }];
            
//            [button setContentEdgeInsets:UIEdgeInsetsMake(0, -7, 0, 0)];
            
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            backItem.customView = button;
            backItem.tintColor = [UIColor whiteColor];
            
            vc.navigationItem.leftBarButtonItem = backItem;
            
        } else {
            vc.navigationItem.leftBarButtonItem = nil;
        }
        
        
        
    } error:&error];
    
    [UIViewController aspect_hookSelector:@selector(viewWillAppear:) withOptions:AspectPositionAfter usingBlock:^(id<AspectInfo> info, BOOL animated) {
        
        UIViewController *vc = info.instance;
        if ([vc.navigationItem.rightBarButtonItem.customView isKindOfClass:[UIButton class]]) {
            UIButton *button = vc.navigationItem.rightBarButtonItem.customView;
            button.titleLabel.font = [UIFont systemFontOfSize:16.f];
            button.frame = CGRectMake(0, 0, 100, 44);
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        }
    } error:&error];
    
    NSAssert(!error, @"hook error");
}

/// 触感
- (void)impactFeedback{
    
    // 触感 中度点击
    UIImpactFeedbackGenerator *impactFeedback = [[UIImpactFeedbackGenerator alloc]initWithStyle:UIImpactFeedbackStyleMedium];
    [impactFeedback impactOccurred];
}

@end
