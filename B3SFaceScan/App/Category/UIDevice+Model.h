//
//  UIDevice+Model.h
//  zm-mall
//
//  Created on 25/12/2017.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (Model)
    
+ (NSString *)deviceModelName;

// 判断是否可用FaceID
+ (BOOL)checkFaceIDAvailable;

// 判断是否是iPhone X 那种四周是圆角的屏幕
+ (BOOL)isIPhoneXScreen;

/// 判断设备是否是iPad
+ (BOOL)isIpad;

@end
