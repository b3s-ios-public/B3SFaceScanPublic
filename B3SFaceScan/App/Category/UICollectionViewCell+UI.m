//
//  UICollectionViewCell+UI.m
//  body3dscale
//
//  Created on 31/03/2018.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "UICollectionViewCell+UI.h"

@implementation UICollectionViewCell (UI)

+ (NSString *)identifier
{
    return NSStringFromClass(self);
}

@end
