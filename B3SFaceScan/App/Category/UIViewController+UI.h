//
//  UIViewController+UI.h
//  body3dscale
//
//  Created on 02/04/2018.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (UI)

/// 触感: 中度
- (void)impactFeedback;

@end
