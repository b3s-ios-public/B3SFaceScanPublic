//
//  MBProgressHUD+B3S.h
//  body3dscale
//
//  Created on 26/02/2018.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <MBProgressHUD/MBProgressHUD.h>

@interface MBProgressHUD (B3S)

+ (void)show;

+ (void)hidden;

+ (MBProgressHUD *)showToast:(NSString *)toast view:(UIView *)view;

+ (MBProgressHUD *)showToast:(NSString *)toast;

+ (MBProgressHUD *)showLoadingWithView:(UIView *)view;

@end
