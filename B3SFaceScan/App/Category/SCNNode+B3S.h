//
//  SCNNode+B3S.h
//  body3dscale
//
//  Created on 2019/11/29.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <SceneKit/SceneKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SCNNode (B3S)

/// 删除子节点
/// @param name 节点名称
-(void)removeCustomNode:(NSString *)name;

/**
 删除所有子节点
 */
- (void)removeChildsNode;

@end

NS_ASSUME_NONNULL_END
