//
//  SCNNode+Line.h
//  body3dscale
//
//  Created on 2018/4/26.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <SceneKit/SceneKit.h>

@interface SCNNode (Line)

+ (SCNNode *)buildLineInTwoPointsWithRotation:(SCNVector3)startPoint
                                     endPoint:(SCNVector3)endPoint
                                       radius:(CGFloat)radius
                                        color:(UIColor *)color
                                   lengthRate:(float)lr;

@end
