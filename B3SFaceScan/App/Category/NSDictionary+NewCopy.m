//
//  NSDictionary+DeepCopy.m
//  body3dscale
//
//  Created on 2018/5/23.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "NSDictionary+NewCopy.h"

@implementation NSDictionary (DeepCopy)

- (NSDictionary *)newCopy
{
    NSMutableDictionary * returnDict = [[NSMutableDictionary alloc] initWithCapacity:self.count];
    NSArray * keys = [self allKeys];
    
    for(id key in keys) {
        id oneValue = [self objectForKey:key];
        id oneCopy = nil;
        
        if([oneValue respondsToSelector:@selector(newCopy)]) {
            oneCopy = [oneValue newCopy];
        }
        else if([oneValue respondsToSelector:@selector(mutableCopy)]) {
            oneCopy = [oneValue mutableCopy];
        }
        else {
            oneCopy = [oneValue copy];
        }
        [returnDict setValue:oneCopy forKey:key];
    }
    return [returnDict copy];
}

@end
