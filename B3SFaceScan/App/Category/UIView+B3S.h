//
//  UIView+B3S.h
//  body3dscale
//
//  Created on 2018/5/28.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (B3S)

- (UIImage *)snapshotImageWithScale:(CGFloat)scale;

@end
