//
//  UIView+B3S.m
//  body3dscale
//
//  Created on 2018/5/28.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "UIView+B3S.h"
#import <Aspects.h>

@implementation UIView (B3S)

+ (void)load
{
    NSError *error = nil;
//    [UIView aspect_hookSelector:@selector(setCenter:) withOptions:AspectPositionInstead usingBlock:^(id<AspectInfo> aspects ,CGPoint center){
//        
//        if (center.x != NAN && center.y != NAN) {
//            [aspects.originalInvocation invoke];
//        } else {
//            NSLog(@"%@ setCenter with NAN",NSStringFromClass(aspects.class));
//        }
//    } error:&error];
    
    NSAssert(!error, @"hook error");
}

- (UIImage *)snapshotImageWithScale:(CGFloat)scale
{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.opaque, scale);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *snap = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return snap;
}

@end
