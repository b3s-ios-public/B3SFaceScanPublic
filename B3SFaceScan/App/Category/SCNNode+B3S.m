//
//  SCNNode+B3S.m
//  body3dscale
//
//  Created on 2019/11/29.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "SCNNode+B3S.h"

@implementation SCNNode (B3S)

-(void)removeCustomNode:(NSString *)name{
    
    SCNNode *childNode = [self childNodeWithName:name recursively:YES];
    if (childNode) {
        [childNode removeFromParentNode];
    }
}

- (void)removeChildsNode{
    
    for (SCNNode *childNode in self.childNodes) {
        [childNode removeFromParentNode];
    }
}

@end
