//
//  UIImageView+B3S.h
//  body3dscale
//
//  Created on 2018/5/2.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (B3S)

- (void)b3s_setImageURL:(NSURL *)imageURL;

- (void)b3s_setImageStr:(NSString *)imageURLstr;

@end
