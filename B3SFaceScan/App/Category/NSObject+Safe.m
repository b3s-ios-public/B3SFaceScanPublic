//
//  NSObject+Safe.m
//  body3dscale
//
//  Created on 2018/6/7.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "NSObject+Safe.h"
#import <Aspects.h>
#import <objc/runtime.h>

@implementation NSObject (Safe)

+ (void)load
{
    NSError *error = nil;
    [NSClassFromString(@"__NSDictionaryM") aspect_hookSelector:@selector(setObject:forKey:) withOptions:AspectPositionInstead usingBlock:^(id<AspectInfo> info,id object,id key) {
        if (object) {
            [info.originalInvocation invoke];
        } else {
            NSLog(@"%@ setObject nil , forKey : %@ ",info.instance,key);
        }
    } error:&error];
    
    [object_getClass(NSClassFromString(@"NSDictionary")) aspect_hookSelector:@selector(dictionaryWithObjects:forKeys:count:) withOptions:AspectPositionInstead usingBlock:^(id<AspectInfo> info,const id *objs,const id *keys,NSUInteger count) {
        
        BOOL isHasNil = NO;
        for (int i = 0; i < count; i++) {
            NSObject *obj = objs[i];
            NSObject *key = keys[i];
            if (!obj) {
                NSLog(@"[Safe]Object isEqule nil , forKey : %@ ",keys[i]);
                isHasNil = YES;
            }
            
            if (!key) {
                NSLog(@"[Safe]Key isEqule nil , forObject : %@ ",objs[i]);
                isHasNil = YES;
            }
        }
        if (!isHasNil) {
            [info.originalInvocation invoke];
        } else {
            
        }
        
    } error:&error];
    
    NSAssert(!error, @"hook error");
    
    [NSClassFromString(@"__NSArrayM") aspect_hookSelector:@selector(addObject:) withOptions:AspectPositionInstead usingBlock:^(id<AspectInfo> info,id object) {
        if (object) {
            [info.originalInvocation invoke];
        } else {
            NSLog(@"%@ add nil ",info.instance);
        }
    } error:&error];
    
    NSAssert(!error, @"hook error");
    
//    [NSClassFromString(@"__NSArray0") aspect_hookSelector:@selector(objectAtIndex:) withOptions:AspectPositionInstead usingBlock:^(id<AspectInfo> info,NSUInteger index) {
//        NSArray *array = info.instance;
//        if (index < array.count) {
//            [info.originalInvocation invoke];
//        } else {
//            NSLog(@"%@ %lu out of %lu ",info.instance,(unsigned long)index,(unsigned long)array.count);
//        }
//    } error:&error];
//    
//    NSAssert(!error, @"hook error");
    
//    [NSClassFromString(@"__NSArrayM") aspect_hookSelector:@selector(objectAtIndex:) withOptions:AspectPositionInstead usingBlock:^(id<AspectInfo> info,NSUInteger index) {
//        NSArray *array = info.instance;
//        if (index < array.count) {
//            [info.originalInvocation invoke];
//        } else {
//            NSLog(@"%@  %lu out of %lu ",info.instance,(unsigned long)index,(unsigned long)array.count);
//        }
//    } error:&error];
    
    [NSMutableArray aspect_hookSelector:@selector(objectAtIndex:) withOptions:AspectPositionInstead usingBlock:^(id<AspectInfo> info,NSUInteger index) {
        NSArray *array = info.instance;
        if (index < array.count) {
            [info.originalInvocation invoke];
        } else {
            NSLog(@"%@  %lu out of %lu ",info.instance,(unsigned long)index,(unsigned long)array.count);
        }
    } error:&error];
    
    
    NSAssert(!error, @"hook error");
}

@end
