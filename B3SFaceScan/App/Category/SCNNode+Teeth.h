//
//  SCNNode+Teeth.h
//  body3dscale
//
//  Created on 2019/4/17.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <SceneKit/SceneKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SCNNode (Teeth)

/**
 通过MDLObject模型直接创建SCNNode节点
 
 @param mesh 模型名
 @param diffuse 纹理图片名
 @return 节点
 */
+ (SCNNode *)nodeWithMesh:(NSString *)mesh diffuse:(NSString *)diffuse;

/**
 通过模型目录创建SCNNode节点

 @param url 模型目录URL
 @return 节点
 */
+ (SCNNode *)nodeWithDir:(NSURL *)url;
    
@end

NS_ASSUME_NONNULL_END
