
//
//  B3SSceneKitHelper.m
//  body3dscale
//
//  Created on 02/04/2018.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SSceneKitHelper.h"

@implementation B3SSceneKitHelper

+ (void)printSCNMatrix4:(SCNMatrix4)m
{
    NSLog(@"%f %f %f %f %f %f %f %f %f %f %f %f",m.m11,m.m12,m.m13,m.m14,m.m21,m.m22,m.m23,m.m24,m.m31,m.m32,m.m33,m.m34);
}

+ (void)printSCNVector3:(SCNVector3)v
{
    NSLog(@"%f %f %f",v.x,v.y,v.z);
}
@end
