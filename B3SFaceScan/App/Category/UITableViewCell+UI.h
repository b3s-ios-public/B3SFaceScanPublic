//
//  UITableViewCell+UI.h
//  body3dscale
//
//  Created on 02/04/2018.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (UI)

+ (NSString *)identifier;

@end
