//
//  B3SUser.h
//  B3SFaceScan
//
//  Created on 2020/8/4.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <Foundation/Foundation.h>


@interface B3SUser : NSObject
@property (nonatomic, copy) NSString * userId;
@property (nonatomic, copy) NSString * email;

@end
