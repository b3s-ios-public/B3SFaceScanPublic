//
//  B3SUserManager.h
//  B3SFaceScan
//
//  Created on 2020/8/4.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <Foundation/Foundation.h>
@class B3SUser;

@interface B3SUserManager : NSObject

//授权，也就是登陆过程. 在这个版本中已经换成了离线license授权.
@property (nonatomic, assign) BOOL isAuthorization;


@property (nonatomic, strong) B3SUser * currentUser;

+ (instancetype)sharedInstance;

- (NSString *)getLastUserCdKey;

- (BOOL)saveUserCdkey:(NSString *)cdKey;

- (void)removeLastUserCdKey;

@end

