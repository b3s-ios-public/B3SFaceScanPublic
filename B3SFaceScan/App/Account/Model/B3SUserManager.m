//
//  B3SUserManager.m
//  B3SFaceScan
//
//  Created on 2020/8/4.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SUserManager.h"

@implementation B3SUserManager

+ (instancetype)sharedInstance
{
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (NSString *)getLastUserCdKey{
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"cdKey"];
}

- (BOOL)saveUserCdkey:(NSString *)cdKey{
    
    if (!cdKey) {
        return NO;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
         [[NSUserDefaults standardUserDefaults] setObject:cdKey forKey:@"cdKey"];
    });
   
    return YES;
}

- (void)removeLastUserCdKey{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"cdKey"];
    });
  
}

@end
