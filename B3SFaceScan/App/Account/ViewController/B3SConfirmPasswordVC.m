//
//  B3SConfirmPasswordVC.m
//  B3SFaceScan
//
//  Created on 2020/8/3.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SConfirmPasswordVC.h"
#import "B3SScanHomeViewController.h"
#import <WTAuthorizationTool/WTAuthorizationTool.h>
#import <AVFoundation/AVFoundation.h>
#import "B3SPreviewViewController.h"

@interface B3SConfirmPasswordVC ()
@property (nonatomic, weak) IBOutlet UITextField * passwordTextField;
@property (nonatomic, weak) IBOutlet UILabel * accountLabel;
@end

@implementation B3SConfirmPasswordVC


+ (instancetype)new
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Account" bundle:nil];
    return [sb instantiateViewControllerWithIdentifier:NSStringFromClass(self)];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.gk_navTitle = NSLocalizedString(@"输入密码", nil);
    [self showBackgroundView];
    [self setupNavItems];
    self.accountLabel.text = [NSString stringWithFormat:@"Account: %@",self.account];
    
}



- (IBAction)nextButtonPress:(id)sender{
    
    NSString * password = [self.passwordTextField text];
    
    if (!password) {
        [self.view makeToast:@"Password can not nil" duration:0.3 position:CSToastPositionCenter];
        return;
    }
    
    //调授权接口
    if ([self.account isEqualToString:@"17398509358"] &&[password isEqualToString:@"7890"]) {
        
        NSArray * vcArray = [self.navigationController viewControllers];
        
        if (vcArray.count>2) {
            
                 UIViewController * vc = [vcArray objectAtIndex:vcArray.count - 3];
                 [self.navigationController popToViewController:vc animated:YES];
                 if (self.canScan) {
                     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                       [[NSNotificationCenter defaultCenter] postNotificationName:goToScanNoticifation object:nil];
                     });
                    
                 }
        }

    }else{
        
           [self.view makeToast:@"Password Error" duration:0.3 position:CSToastPositionCenter];
    }
    
}

@end
