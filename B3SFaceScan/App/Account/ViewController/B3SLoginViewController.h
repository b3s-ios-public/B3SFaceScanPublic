//
//  B3SLoginViewController.h
//  B3SFaceScan
//
//  Created on 2020/8/3.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SBaseViewController.h"

typedef void(^AuthorizationSuccessBlock)(void);

@interface B3SLoginViewController : B3SBaseViewController
@property (nonatomic, assign) BOOL canScan;

@property (nonatomic, copy) AuthorizationSuccessBlock authorizationSuccessBlock;

@end
