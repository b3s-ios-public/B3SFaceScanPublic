//
//  B3SSetModelPrecisionUnitViewController.h
//  body3dscale
//
//  Created on 2019/2/14.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SAlertController.h"

#import "B3SModelService.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^B3SSetModelPrecisionUnitSelectBlock)(B3SModelServiceModelUnit unit);

@interface B3SSetModelPrecisionUnitViewController : B3SAlertController

@property (weak, nonatomic) IBOutlet UIButton *meterButton;  // 米
@property (weak, nonatomic) IBOutlet UIButton *decimeterButton; // 分米
@property (weak, nonatomic) IBOutlet UIButton *centimeterButton; // 厘米
@property (weak, nonatomic) IBOutlet UIButton *millimeterButton; // 毫米

@property (copy, nonatomic) B3SSetModelPrecisionUnitSelectBlock selectBlock;

@end

NS_ASSUME_NONNULL_END
