//
//  B3SSetModelPrecisionUnitViewController.m
//  body3dscale
//
//  Created on 2019/2/14.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SSetModelPrecisionUnitViewController.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

#import "MBProgressHUD+B3S.h"
#import "B3SLocalizeService.h"


@interface B3SSetModelPrecisionUnitViewController ()

@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;

@end

@implementation B3SSetModelPrecisionUnitViewController

+ (instancetype)new
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Account" bundle:nil];
    return [sb instantiateViewControllerWithIdentifier:NSStringFromClass(self)];
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    
    [self showBackgroundView];
    [self setupNavItems];
    
    self.gk_navTitle = NSLocalizedString(@"导出模型单位", nil);

    NSString * conetnt = NSLocalizedString(@"请选择如下其中一个精度单位,当前是 : ",nil);
    
    NSMutableAttributedString *str1 = [[NSMutableAttributedString alloc] initWithString:conetnt];
    NSMutableAttributedString *str2 = nil;
    
    B3SModelService *modelService = [B3SModelService new];
    
    switch (modelService.modelUnitForExporting) {
        case B3SModelServicePrecisionUnitMeter:
            str2 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"米",nil)];
            break;
        case B3SModelServicePrecisionUnitDecimeter:
            str2 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"分米",nil)];
            break;
        case B3SModelServicePrecisionUnitCentimeter:
            str2 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"厘米",nil)];
            break;
        case B3SModelServicePrecisionUnitMillimeter:
            str2 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"毫米",nil)];
            break;
        default:
            str2 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"米",nil)];
            break;
    }
    
    [str1 addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, str1.length)];
    [str2 addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0, str2.length)];
    
    [str1 appendAttributedString:str2];
    
    self.subtitleLabel.text = str1.mutableString;
    
    @weakify(self);
    self.meterButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(UIButton *input) {
        @strongify(self);
        
        if (self.selectBlock) {
            self.selectBlock(B3SModelServicePrecisionUnitMeter);
        }
        
        [self dismissWithComplete:^{
            [MBProgressHUD showToast:[NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"你选择的是", nil), NSLocalizedString([input titleForState:UIControlStateNormal], nil)]];
        }];
        

        return [RACSignal empty];
    }];
    
    self.decimeterButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(UIButton *input) {
        @strongify(self);
        
        
        if (self.selectBlock) {
            self.selectBlock(B3SModelServicePrecisionUnitDecimeter);
        }
        
        [self dismissWithComplete:^{
            [MBProgressHUD showToast:[NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"你选择的是", nil), NSLocalizedString([input titleForState:UIControlStateNormal], nil)]];
        }];
        

        return [RACSignal empty];
    }];
    
    self.centimeterButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(UIButton *input) {
        @strongify(self);
        
        if (self.selectBlock) {
            self.selectBlock(B3SModelServicePrecisionUnitCentimeter);
        }
        [self dismissWithComplete:^{
            [MBProgressHUD showToast:[NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"你选择的是", nil), NSLocalizedString([input titleForState:UIControlStateNormal], nil)]];
        }];

        return [RACSignal empty];
    }];
    
    self.millimeterButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(UIButton *input) {
        @strongify(self);
        if (self.selectBlock) {
            self.selectBlock(B3SModelServicePrecisionUnitMillimeter);
        }
        
        [self dismissWithComplete:^{
            [MBProgressHUD showToast:[NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"你选择的是", nil), NSLocalizedString([input titleForState:UIControlStateNormal], nil)]];
        }];
        return [RACSignal empty];
    }];
}


- (void)dismiss
{
    [self dismissWithComplete:NULL];
}

- (void)dismissWithComplete:(void(^)(void))complete
{

    if (complete) {
        complete();
    }
    [self.navigationController popViewControllerAnimated:YES];
}



@end
