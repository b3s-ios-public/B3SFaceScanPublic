//
//  B3SSettingViewController.m
//  B3SFaceScan
//
//  Created on 2020/7/30.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SSettingViewController.h"
#import "B3SPrivacyViewController.h"
#import "B3SUserManager.h"
#import "B3SLoginViewController.h"
#import "B3SSetModelPrecisionUnitViewController.h"
#import "B3SAboutViewController.h"
#import "B3SUserAgreementViewController.h"

@interface DataItem : NSObject
@property (nonatomic, copy) NSString * title;
@property (nonatomic, copy) NSString * subTitle;
@property (nonatomic, strong) UIImage * image;

@end

@implementation DataItem

@end


@interface B3SSettingViewController ()
@property (nonatomic, strong) NSArray <DataItem *>*dataArray;
@property (nonatomic,strong)UITableView * tableView;
@end

@implementation B3SSettingViewController

+ (instancetype)new
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Account" bundle:nil];
    return [sb instantiateViewControllerWithIdentifier:NSStringFromClass(self)];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.gk_navTitle = NSLocalizedString(@"设置", nil);
    [self setupNavItems];
    [self showBackgroundView];

    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(self.view.mas_right);
        make.bottom.mas_equalTo(self.view.mas_bottom);
    }];
    self.tableView.rowHeight = 58;
    [self.tableView setDelegate:(id<UITableViewDelegate> _Nullable)self];
    [self.tableView setDataSource:(id<UITableViewDataSource> _Nullable)self];
    
    self.tableView.tableFooterView = [UIView new];
    
    [self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    self.tableView.tableHeaderView = [self tableViewHeaderView];
}
 
- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
}

- (NSArray<DataItem *> *)dataArray{
    
    if (!_dataArray) {
        
         DataItem * item0 = [[DataItem alloc] init];
        item0.title = @"导出模型单位";
        
       B3SModelServiceModelUnit unit =  [[B3SModelService new] modelUnitForExporting];
        if (unit == B3SModelServicePrecisionUnitMeter) {
             item0.subTitle = @"米"; //从磁盘读取
        }else if (unit == B3SModelServicePrecisionUnitDecimeter){
              item0.subTitle = @"分米"; //从磁盘读取
        }else if (unit == B3SModelServicePrecisionUnitCentimeter){
              item0.subTitle = @"厘米"; //从磁盘读取
        }else{
            item0.subTitle = @"毫米"; //从磁盘读取
        }

        
         DataItem * item1 = [[DataItem alloc] init];
        item1.title = @"关于我们";
        item1.subTitle = @"";
        
         DataItem * item2 = [[DataItem alloc] init];
        item2.title = @"隐私政策";
        item2.subTitle = @"";
        
        
         DataItem * item3 = [[DataItem alloc] init];
        item3.title = @"用户协议";
        item3.subTitle = @"";
        
        
        _dataArray = [[NSArray alloc] initWithObjects:item0,item1,item2,item3, nil];
    }
    
    return _dataArray;
}

- (UITableView *)tableView{
    
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorColor = [[UIColor whiteColor] colorWithAlphaComponent:0.2];
    }
    return _tableView;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];

    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cell"];
    }
    cell.contentView.backgroundColor = HexColor(0x35363D);
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.detailTextLabel.textColor = [UIColor whiteColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
   // cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    DataItem * dataItem = [self.dataArray objectAtIndex:indexPath.row];
    cell.textLabel.text = dataItem.title;
    cell.detailTextLabel.text = dataItem.subTitle;
   
    return cell;
}

- (UIView *)tableViewHeaderView{
    
    UIView * headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 177+18)];
    headerView.backgroundColor = HexColor(0x35363D);
    
    UIImageView * imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"app_logo"]];
    
    [headerView addSubview:imageView];
    
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(21);
        make.bottom.equalTo(headerView.mas_bottom).with.offset(-23-18);
        make.width.mas_equalTo(49);
        make.height.mas_equalTo(42);
    }];
    
    NSString * title = [B3SUserManager sharedInstance].getLastUserCdKey?[B3SUserManager sharedInstance].getLastUserCdKey: @"请登陆";
    UILabel * signLabel = [[UILabel alloc] init];
    signLabel.text = title;
    signLabel.textColor = [UIColor whiteColor];
    signLabel.userInteractionEnabled = YES;
    signLabel.font = [UIFont boldSystemFontOfSize:18.0];
    [signLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(signInLabelTapped:)]];
    [headerView addSubview:signLabel];
    
    [signLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imageView.mas_right).with.offset(16);
        make.centerY.equalTo(imageView.mas_centerY);
        make.width.mas_equalTo(280);
        make.height.mas_equalTo(24);
    }];
    
    UIView * lineView = [[UIView alloc] init];
    lineView.backgroundColor = HexColor(0x484952);
    [headerView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.bottom.equalTo(headerView.mas_bottom);
        make.right.equalTo(headerView.mas_right);
        make.height.mas_equalTo(18);
        
    }];
    
    
    return headerView;
}

- (void)signInLabelTapped:(UIGestureRecognizer *)gesture{
    
    B3SLoginViewController * loginVc =  [B3SLoginViewController new];
    [self.navigationController pushViewController:loginVc animated:YES];
  
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
   
    if (indexPath.row == 0) {
        
       
        B3SSetModelPrecisionUnitViewController *vc = [B3SSetModelPrecisionUnitViewController new];
        vc.selectBlock = ^(B3SModelServiceModelUnit unit) {
            //存磁盘
            [[B3SModelService new] setModelUnitForExporting:unit];
            
            NSString * title = NSLocalizedString(@"毫米", nil);
            if (unit == B3SModelServicePrecisionUnitMeter) {
                title = NSLocalizedString(@"米", nil);
            }else if (unit == B3SModelServicePrecisionUnitDecimeter){
                title = NSLocalizedString(@"分米", nil);
            }else if (unit == B3SModelServicePrecisionUnitCentimeter){
                title = NSLocalizedString(@"厘米", nil);
            }else if (unit == B3SModelServicePrecisionUnitMillimeter){
                title = NSLocalizedString(@"毫米", nil);
            }
            
           DataItem *item = [self.dataArray objectAtIndex:indexPath.row];
           item.subTitle = title;
            [tableView beginUpdates];
            [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            [tableView endUpdates];
        };
        [self.navigationController pushViewController:vc animated:YES];
        
    }else if (indexPath.row == 1){
        
        B3SAboutViewController * vc = [B3SAboutViewController new];
        [self.navigationController pushViewController:vc animated:YES];
        
    }else if (indexPath.row == 2){
        
        B3SPrivacyViewController * vc = [B3SPrivacyViewController new];
        [self.navigationController pushViewController:vc animated:YES];
        
    }else if (indexPath.row == 3){
        
        B3SUserAgreementViewController * vc = [B3SUserAgreementViewController new];
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}

@end
