//
//  B3SConfirmPasswordVC.h
//  B3SFaceScan
//
//  Created on 2020/8/3.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface B3SConfirmPasswordVC : B3SBaseViewController
@property (nonatomic, copy) NSString * account;
@property (nonatomic, assign)BOOL canScan;

@end

NS_ASSUME_NONNULL_END
