//
//  B3SAboutViewController.m
//  body3dscale
//
//  Created on 2018/4/18.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SAboutViewController.h"
#import "BlocksKit+UIKit.h"

@interface B3SAboutViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@end

@implementation B3SAboutViewController

+ (instancetype)new
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Account" bundle:nil];
    return [sb instantiateViewControllerWithIdentifier:NSStringFromClass(self)];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self showBackgroundView];
    [self setupNavItems];

    
    self.gk_navTitle = NSLocalizedString(@"关于我们", nil);
    
    self.logoImageView.layer.cornerRadius = 20.f;
    self.logoImageView.layer.masksToBounds = YES;
    
    NSDictionary *infoDic = [[NSBundle mainBundle] infoDictionary];
    
    // 获取App的版本号
    NSString *appVersion = [infoDic objectForKey:@"CFBundleShortVersionString"];
    
    // 获取App的build版本
    NSString *appBuildVersion = [infoDic objectForKey:@"CFBundleVersion"];
    
    self.versionLabel.text = [NSString stringWithFormat:@"V %@.%@",appVersion?:@"0",appBuildVersion?:@"0"];
    
    //设置字间距
    
    NSDictionary *dic = @{NSKernAttributeName:@1.f
                          };
    NSString *text = self.textLabel.text;
    NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithString:text attributes:dic];
    
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5];//行间距
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [text length])];
    
    [self.textLabel setAttributedText:attributedString];
    self.textLabel.textAlignment = NSTextAlignmentLeft;
    [self.textLabel sizeToFit];
    
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
