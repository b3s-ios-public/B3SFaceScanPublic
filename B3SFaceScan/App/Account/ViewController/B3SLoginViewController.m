//
//  B3SLoginViewController.m
//  B3SFaceScan
//
//  Created on 2020/8/3.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SLoginViewController.h"
#import "B3SConfirmPasswordVC.h"
#import "B3SUserManager.h"
#import <B3SScanSDK/B3SScanSDK.h>
#import <MBProgressHUD/MBProgressHUD.h>

@interface B3SLoginViewController ()
@property (nonatomic, weak) IBOutlet UITextField * accountTextField;
@property (nonatomic, weak) IBOutlet UIView * containerView;
@end

@implementation B3SLoginViewController


+ (instancetype)new
{
  UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Account" bundle:nil];
  return [sb instantiateViewControllerWithIdentifier:NSStringFromClass(self)];
}


- (void)viewDidLoad {
  [super viewDidLoad];
  self.containerView.layer.masksToBounds = YES;
  self.gk_navTitle = NSLocalizedString(@"账号", nil);
  [self showBackgroundView];
  [self setupNavItems];
  
}

- (void)viewDidAppear:(BOOL)animated{
  [super viewDidAppear:animated];
  
  [self.accountTextField becomeFirstResponder];
}

- (IBAction)nextButtonPress:(id)sender
{
  NSString * text = [self.accountTextField text];
  if (!text || [text isEqualToString:@""]) {
    [self.view makeToast:NSLocalizedString(@"账号不能为空", nil) 
              duration:0.3 
              position:CSToastPositionCenter];
    return;
  }
  [self.accountTextField resignFirstResponder];
  
  //离线授权
  MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
  B3SSdkManager *platMgr = [B3SSdkManager sharedInstance];
  B3SUserManager * manager = [B3SUserManager sharedInstance];
  // request to get authorization. Please make sure the app have access to network beforehand.
  NSDate *start=[NSDate date];
  __block NSError *sdkErr=nil;
  [platMgr verifySDKIntegrity:^(NSError *error){
      NSLog(@"verificate time used:%.3lf seconds",[[NSDate date] timeIntervalSinceDate:start]);
      if(error){
        sdkErr = [error copy];
        NSLog(@"sdk init failed, %@", error);
      }else{
        NSLog(@"sdk init succ");
      }
  }];
  if (sdkErr) {//授权失败
    NSLog(@"Authorization Failed：%@", sdkErr);
    [self.view makeToast:sdkErr.localizedDescription
                duration:0.3 
                position:CSToastPositionCenter];
  } else {//授权成功
    [manager setIsAuthorization:YES];
    [manager saveUserCdkey:@"dummy-key"];
    
    if ([text isEqualToString:@"17398509358"]) {
      //苹果审核人员, 让他去输入密码的界面
      dispatch_async(dispatch_get_main_queue(), ^{
        B3SConfirmPasswordVC * passwod =  [B3SConfirmPasswordVC new];
        passwod.account = text;
        passwod.canScan = self.canScan;
        [self.navigationController pushViewController:passwod animated:YES];
      });
      return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
      [self.view makeToast:NSLocalizedString(@"登陆成功", nil) 
                  duration:0.3 
                  position:CSToastPositionCenter];
      [self.navigationController popViewControllerAnimated:YES];
    });
    
    if (self.canScan) {
      dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:goToScanNoticifation object:nil];
      });
      
    }
  }
    
    
  /*在线远程授权接口
  [platMgr authorizationWithCdKey:text complet:^(NSError *error) {
    dispatch_async(dispatch_get_main_queue(), ^{
      [hud hideAnimated:YES];
    });
    if (error) {
      NSLog(@"Authorization Failed：%@", error.localizedDescription);
      if (error.code == SDKAuthorizationModelNoTimesError||
          error.code == SDKAuthorizationExpireError||
          error.code == SDKAuthorizationDeviceNumbersLimitError||
          error.code == SDKAuthorizationCdKeyExpectionError) {
        manager.isAuthorization = NO;
        [manager removeLastUserCdKey]; //业务出错了，就授权失败
      }else{
        [self.view makeToast:error.localizedDescription duration:0.3 position:CSToastPositionCenter]; //一般出差，弹totast
      }
      
    } else {
      
      [manager setIsAuthorization:YES];
      [manager saveUserCdkey:text];
      
      if ([text isEqualToString:@"17398509358"]) {
        //审核账号让他去输入密码
        dispatch_async(dispatch_get_main_queue(), ^{
          B3SConfirmPasswordVC * passwod =  [B3SConfirmPasswordVC new];
          passwod.account = text;
          passwod.canScan = self.canScan;
          [self.navigationController pushViewController:passwod animated:YES];
          
        });
        return;
      }
      
      dispatch_async(dispatch_get_main_queue(), ^{
        [self.view makeToast:NSLocalizedString(@"登陆成功", nil) duration:0.3 position:CSToastPositionCenter];
        [self.navigationController popViewControllerAnimated:YES];
        
      });
      
      if (self.canScan) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
          [[NSNotificationCenter defaultCenter] postNotificationName:goToScanNoticifation object:nil];
        });
        
      }
      
    }
  }]; */
  
}

@end
