//
//  B3SSettingViewController.h
//  B3SFaceScan
//
//  Created on 2020/7/30.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface B3SSettingViewController : B3SBaseViewController

@end

NS_ASSUME_NONNULL_END
