//
//  B3SUserAgreementViewController.m
//  B3SFaceScan
//
//  Created on 2020/8/6.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import "B3SUserAgreementViewController.h"

@interface B3SUserAgreementViewController ()
@property (nonatomic, weak) IBOutlet UIScrollView * scrollView;
@property (nonatomic, weak) IBOutlet UILabel * contentLabel;
@end

@implementation B3SUserAgreementViewController


+ (instancetype)new
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Account" bundle:nil];
    return [sb instantiateViewControllerWithIdentifier:NSStringFromClass(self)];
}


- (void)viewDidLoad {
    [super viewDidLoad];
     
      self.gk_navTitle = NSLocalizedString(@"用户协议", nil);
      [self showBackgroundView];
      [self setupNavItems];
    
    
    NSDictionary *dic = @{NSKernAttributeName:@1.f
                          };
    NSString *text = self.contentLabel.text;
    NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithString:text attributes:dic];
    
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5];//行间距
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [text length])];
    
    [self.contentLabel setAttributedText:attributedString];
    self.contentLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentLabel sizeToFit];
}



@end
