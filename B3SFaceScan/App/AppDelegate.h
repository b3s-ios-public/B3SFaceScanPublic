//
//  AppDelegate.h
//  B3SFaceScan
//
//  Created on 2020/7/24.
//  Copyright © 2021 body3dscale.com All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


+ (instancetype)sharedInstance;

/// 控制应用入口逻辑
- (void)handlerMainLogic;

@end

